.. _section-gettingstarted:

Getting Started
=====================================

This guide will help you setup Python to control memetis ECU-P hardware.

Install VCP driver
-------------------------------------

The ECU-P hardware uses chip from SiliconLabs for the communication over USB. Unfortunatly this requires installation of a driver package from the chip vendor.

.. tabs::
    .. group-tab:: macOS

        * Visit `SiLabs Download page <https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers>`_ and switch to `Downloads` tab. Download the `current version for macOS <https://www.silabs.com/documents/public/software/Mac_OSX_VCP_Driver.zip>`_.
        * Unzip the downloaded file, mount `SiLabsUSBDriverDisk.dmg` by double-clicking it and run `Install CP210x VCP Driver.app`.
        * Follow the installation instructions. You will be prompted to enter your password twice. After a short while you will be informed that a system extension was blocked.
        * Open the `Security & Privacy` pane in macOS Settings.
        * Allow making changes by clicking the small lock symbol in the bottom right-hand corner. Then allow the `CP210xVCPDriver.app`.
        * The installation will finish automatically.

        .. image:: images/gettingstarted/macOS_systemextension_blocked.png
            :width: 33 %

        .. image:: images/gettingstarted/macOS_systemextension_allow.png
            :width: 63 %

    .. group-tab:: Windows

        * Visit `SiLabs Download page <https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers>`_ and switch to `Downloads` tab. Download the `current version for Windows <https://www.silabs.com/documents/public/software/CP210x_Universal_Windows_Driver.zip>`_.
        * Unzip the downloaded file and open the folder.
        * Right-click on `silabser.inf`, select `Show more options` and hit `Install`.
        * You will be asked to allow to make changes to your device. Select `Yes`.
        * The installation will complete without any further notifications.

        .. image:: images/gettingstarted/windows_driverfiles.png
            :width: 30 %

        .. image:: images/gettingstarted/windows_driverinstall.png
            :width: 30 %

        .. image:: images/gettingstarted/windows_allowchanges.png
            :width: 30 %

    .. group-tab:: Linux

        The VCP driver has been integrated in the Linux kernel since version 3.

        .. note::

            The user running Python needs access to serial devices. On most Linux distributions this is achieved by adding the user to the `dialout` group.

            Use this command to add your username to this group:

            .. code-block::

                sudo usermod -a -G dialout your_username

Check USB connection
-------------------------------------

Before continuing you can use the steps below to check if the driver was successfully installed and the ECU-P hardware is found by your computer.

.. tabs::
    .. group-tab:: macOS

        * Connect the ECU-P hardware with a USB micro cable to your computer.
        * Open the `Terminal` application.
        * Run ``ls -l /dev/cu.*`` in the terminal.
        * You should see at least two devices. `/dev/cu.SLAB_USBtoUART` & `/dev/cu.usbserial-110` in addition to devices already present on your system.

        .. image:: images/gettingstarted/macOS_ecup_connected.png

        .. note::

            If you cannot see the ECU-P hardware `/dev/cu.SLAB_USBtoUART` try updating the VCP driver from the previous step. Make sure you allowed the **system extension** under `Security & Privacy`.
            
            You can also try with a different USB cable.

    .. group-tab:: Windows

        * Connect the ECU-P hardware with a USB micro cable to your computer.
        * Open `Device Manager` to see all connected hardware.
        * Expand the `Ports (COM & LPT)` section.
        * You should see a device called `Silicon Labs CP210x USB to UART Bridge` and the serial interface name in brackets.

        .. image:: images/gettingstarted/windows_ecup_connected.png

        .. note::

            If you cannot see the ECU-P hardware try updating the VCP driver from the previous step.

            You can also try with a different USB cable.

    .. group-tab:: Linux

        * Connect the ECU-P hardware with a USB micro cable to your computer.
        * Open a terminal. In Ubuntu it's called `Terminal`.
        * Run ``ls -l /dev/ttyUSB*`` in the terminal.
        * You should see at least one device named `/dev/ttyUSB0`.

        .. image:: images/gettingstarted/linux_ecup_connected.png

        .. note::

            Depending on your distrubution serial devices might be named differently. You can check with ``ls -l /dev/tty*`` to see all available serial port.
            
            You can also try with a different USB cable.

Install and check Python
-------------------------------------

The ECU-P library requires Python 3.8 or higher.

.. tabs::
    .. group-tab:: macOS

        macOS comes with Python 3 pre-installed. Its available with the ``python3`` and ``pip3`` commands.

        Check that the version is at least 3.8 and that `pip` is working. Run the following two commands in `Terminal`:

        .. code-block::

            python3 --version
            pip3 --version

        These commands should not generate any error messages and display the version number of Python 3 and the package manager `pip`.

        .. image:: images/gettingstarted/macOS_python_working.png

    .. group-tab:: Windows

        Windows does not come with Python pre-installed. You need to install it using the following steps:

        * Download Python 3 from `python.org <https://www.python.org/downloads/windows/>`_. Make sure to select **Python 3** and not version 2. Get the Windows installer for you Windows variant (32bit/64bit).
        * Execute the downloaded Python installer.
        * Make sure to select `Add Python 3.x to PATH` and press `Install Now`.
        * When asked, allow the installer to make changes to the system.
        * The installation should complete

        .. image:: images/gettingstarted/windows_installpython.png

        Once you have installed Python 3 open the `Command Prompt` app and enter the following two commands:

        .. code-block::

            python --version
            pip --version

       These commands should not generate any error messages and display the version number of Python 3 and the package manager `pip`.

        .. image:: images/gettingstarted/windows_python_working.png

    .. group-tab:: Linux

        Many Linux distributions come with Python pre-installed. If it is not installed you can install it by running ``sudo apt install python3 python3-pip`` in your terminal.

        Check that the version is at least 3.8 and that `pip` is working. Run the following two commands in the terminal:

        .. code-block::

            python3 --version
            pip3 --version

        These commands should not generate any error messages and display the version number of Python 3 and the package manager `pip`.

        .. image:: images/gettingstarted/linux_python_working.png

Install ECU-P Python library
-------------------------------------

Install the ECU-P library by running

.. code-block::

    pip3 install https://gitlab.com/memetis/ecu-p/python/-/archive/master/python-master.zip

in the terminal / Command Prompt. This will automatically download and install the latest version of the library. You can use the same command to update any old version to the latest one.

Connect to ECU-P hardware with Python
-------------------------------------

Create a new Python program with the following content:

.. code-block::

    import logging
    from ecu import ECUManager

    logging.getLogger().setLevel(logging.WARNING)
    logging.warning('Connection example started!')

    manager = ECUManager()
    for ecu in manager.get_all():
        print(ecu)

Run this Python program with ECU-P hardware conencted to your computer with a USB micro cable.

It will test all available serial ports on your computer for ECU-P hardware automatically. It then prints out information (product name, firmware version, serial port and UUID) of all found ECU-P devices.

.. image:: images/gettingstarted/ecu_found.png

.. note::

    If no device was found make sure you followd all the above steps. Make espacially sure that your computer recognises the ECU-P hardware as a serial port.

Enable output for 5 seconds
-------------------------------------

Create a second Python program with the following content:

.. code-block::

    import logging
    import time
    from ecu import ECUManager

    logging.getLogger().setLevel(logging.INFO)
    logging.info('Connection example started!')

    manager = ECUManager()
    ecu = manager.get_all()[0]
    print(ecu)

    ecu.enable(1)
    time.sleep(5)
    ecu.disable(1)

Run this Python program with ECU-P hardware conencted to your computer with a USB micro cable.

It will search all serial ports for ECU-P hardware. It then prints out information about the first found device. Then it enables `CH1` output. After waiting 5 seconds it disables the output again.

.. image:: images/gettingstarted/ecu_outputenabled.png

You should see the LED on the front panel turn on. Depending on the configuration of you device, this LED will turn off automatically after 300 milliseconds (for bistable valves, this can be overwritten by enabling `manual_mode`) or 5 seconds (for NC valves).
