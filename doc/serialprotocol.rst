Serial Protocol
=====================================

A custom serial communication protocol is used to send commands over USB and I2C to the ECU-P hardware and reading back a response.

.. contents::
    :local:

.. _section-general-format-and-checksum:

General Format and Checksum
-------------------------------------

Every communications starts with a :ref:`Command <section-command-structure>` being sent to the ECU-P hardware. The ECU-P then will try to execute the received command and always send a single :ref:`Respone <section-response-structure>` message . The ECU-P hardware never initialize communication.

.. _subsection-message-structure:

Message Structure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The structure of :ref:`Command <section-command-structure>` and :ref:`Respone <section-response-structure>` messages follow this general format:

.. _table-general-message-structure:

.. table:: General Message Structure

    +-----------------+--------------------+---------+---------+---------+------------------+-------------------+
    | Byte Number     | 0                  | 1       | 2       | ...     | N - 2            | N - 1             |
    +=================+====================+=========+=========+=========+==================+===================+ 
    | **Value**       | N                  | D0      | D1      | ...     | CRC16 & 0xFF     | CRC16 >> 8        |
    +-----------------+--------------------+---------+---------+---------+------------------+-------------------+
    | **Description** | total length in    | message data                | CRC16 lower byte | CRC 16 upper byte |
    +                 +                    +                             +------------------+-------------------+
    |                 | bytes. Max. **32** |                             | checksum                             |
    +-----------------+--------------------+---------+---------+---------+------------------+-------------------+

The length of the entire message (including the length byte and checksum) is transmitted first. The **maximum allowed message length is 32**. Given that, the maximum data length in a single message is 29.

Any 2-byte values encoded in the message data and the checksum follow **little-endian byte order**.

.. _subsection-checksum:

Checksum
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A 2-byte checksum is transmitted. The lower byte is transmitted first, then the upper byte completes the message.
The checksum is calculated over the whole previous data, including the first total length byte.

The 16 bit checksum used is **CRC16 (CCITT-16)**, inital value 0 and polynomial 0x1021 (x\ :sup:`16` + x\ :sup:`12` + x\ :sup:`5` + 1).
This checksum is implemented in Python for example in `binascii.crc_hqx(data, 0)`.

For example the byte sequence of `0x05, 0x01, 0x3F` results in a checksum value of `0x1F7D`, so the first byte to append to the message is `0x7D` and the second byte is `0x1F`.

.. _table-example-message-with-checksum:

.. table:: Example Message with Checksum

    +-----------------+--------+------+-------+------+------+
    | Byte Number     | 0      | 1    | 2     | 3    | 4    |
    +=================+========+======+=======+======+======+
    | **Value**       | 0x05   | 0x01 | 0x3F  | 0x7D | 0x1F |
    +-----------------+--------+------+-------+------+------+
    | **Description** | length | message data | checksum    |
    +-----------------+--------+------+-------+------+------+

USB Communication
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Connecting to the ECU-P hardware uses a USB-to-Serial convert chip: `CP2102N` from Silicon Laboratories. This chip requires a USB driver being installed on the USB host side (Desktop PC, Laptop etc.). The driver is offered by SiLabs on their `download page <https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers>`_. Short instructions on how to install the driver can be found in the Getting Started guide.

The serial port needs to be opened with these parameters:

:Baudrate: 1000000 (1 million symbols / second)
:Data bits: 8
:Parity: None
:Stop bits: 1
:Flow Control: None

or in short **1000000 baudrate 8N1**.

A transaction start with the USB host sending the command message. As soon as the last byte is received, the ECU-P hardware processes the commands and tries to execute it. If a command message is not fully transmitted, the ECU-P hardware waits for completion for 50ms. Bytes received after this timeout are assumed to be a new command message.

The ECU-P hardware then transmits the response message. During transmission of the response no new commands are allowd to be send to the ECU-P hardware.

Once the response message has been fully transmitted, the ECU-P hardware is ready to receive the next command.

.. I2C Communication
.. ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _section-command-structure:

Command Structure
-------------------------------------

Commands sent to the ECU-P hardware extend the :ref:`General Message <table-general-message-structure>` format and define further bytes in the message. The structure follow the following format:

.. _table-command-structure:

.. table:: Command Structure

    +-----------------+--------+------------+------------------------+----+----+-----+-------+-------+
    | Byte Number     | 0      | 1          | 2                      | 3  | 4  | ... | N - 2 | N - 1 |
    +=================+========+============+========================+====+====+=====+=======+=======+
    | **Value**       | N      | ID         | MODE                   | D0 | D1 | ... | CRC16         |
    +-----------------+--------+------------+------------------------+----+----+-----+-------+-------+
    | **Description** | length | command    | `0x21` write mode,     | command data  | checksum      |
    +                 +        +            +                        +               +               +
    |                 |        | identifier | `0x3F` read mode       | if any        |               |
    +-----------------+--------+------------+------------------------+----+----+-----+-------+-------+

The first byte transmitted is the total length of the message (including this byte and the checksum).

Which command is to be executed is transmitted as a **unique command identifier (ID)** in the second byte of the message. Not all commands are supported by each ECU-P hardware. Some commands are only available on some hardware.

A command can either be a **write or a read command**. This is specified by the third byte: `0x21` is transmitted for write mode, `0x3F` for read mode. Some commands only support write mode, some only read mode and some commands support both.

Then follows **data for the specific command**. The length of this data depends on the command identifier and mode. Some commands do not allow any data at all, some only require data for read mode or vice versa. The data length in write mode is not the same when in read mode.

At the end the :ref:`checksum <subsection-checksum>` is transmitted.

.. _section-response-structure:

Response Structure
-------------------------------------

Responses from the ECU-P hardware (after receiving a :ref:`Command message <section-command-structure>`) extend the :ref:`General Message <table-general-message-structure>` format and define further bytes in the message. The structure follow the following format:

.. _table-response-structure:

.. table:: Response Structure

    +-----------------+--------+------------+------------------------+----+----+---------+-------+-------+
    | Byte Number     | 0      | 1          | 2                      | 3  | 4  | ...     | N - 2 | N - 1 |
    +=================+========+============+========================+====+====+=========+=======+=======+
    | **Value**       | N      | ID         | STATUS                 | D0 | D1 | ...     | CRC16         |
    +-----------------+--------+------------+------------------------+----+----+---------+-------+-------+
    | **Description** | length | command    | `0x2B` success,        | response data if  | checksum      |
    +                 +        +            +                        +                   +               +
    |                 |        | identifier | `0x2D` error           | any or error code |               |
    +-----------------+--------+------------+------------------------+----+----+---------+-------+-------+

The first byte transmitted is the total length of the message (including this byte and the checksum).

The second byte transmitted is the **command identifier (ID)** of the received command this response is for.

The third by returns the **status (STATUS)** of the received command: `0x2B` is sent if the received command was executed successfully, `0x2D` is transmitted if an error occured during command execution.

Then follows the response data if any in case the command was executed successfully (STATUS=0x2B). Some commands do not return any response data. The length of the response data depends on the command identifier and the mode. In case an error occured during command execution (STATUS=0x2D) the response data will contain exactly **one byte**, the :ref:`Error Code <section-error-codes>`.

At the end the :ref:`checksum <subsection-checksum>` is transmitted.

.. _section-error-codes:

Error Codes
-------------------------------------

In case the command could not be executed successfully by the ECU-P hardware an error response will be send by the harwdare. This error response extends the :ref:`Response Structure <table-response-structure>` and has the following format:

.. _table-error-response-structure:

.. table:: Error Response Structure

    +-----------------+--------+------------+--------+------------+-----+----+
    | Byte Number     | 0      | 1          | 2      | 3          | 4   | 5  |
    +=================+========+============+========+============+=====+====+
    | **Value**       | 0x06   | ID         | 0x2D   | ERROR CODE | CRC16    |
    +-----------------+--------+------------+--------+------------+-----+----+
    | **Description** | length | command    | error  | error      | checksum |
    +                 +        +            +        +            +          +
    |                 |        | identifier | status | identifier |          |
    +-----------------+--------+------------+--------+------------+-----+----+

The following **error identifiers (ERROR CODE)** values are defined:

0x01 CHECKSUM
    The checksum in the transmitted command did not match the data in the command.

0x02 UNKNOWN_COMMAND
    An unkown command identifier was transmitted. Be aware that support for some commands depend on the hardware.

0x03 WRONG_MODE
    The mode selection byte in the command was not valid.

0x04 READ_ONLY
    The transmitted command is read-only and cannot be in write-mode.

0x05 WRITE_ONLY
    The transmitted command is write-ony and cannot be in read-mode.

0x06 WRONG_DATA_LENGTH
    The command data did not match the expected length for this command and selected mode.

0x07 WRONG_CHANNEL
    The specified channel in the command data is not valid.

0x08 CALIBRATION_LOCKED
    Writing calibration failed because the the calibration is locked. Sending the unlock command first is required.

0x09 AUTOMATIC_MODE
    The hardware is in automatic mode. Some commands changing the output are not allowed. They require switching to manual mode first.

0x0A STATEMACHINE_WRONG
    The transmitted statemachine byte stream was not valid.

0x0B OUT_OF_RANGE
    The range specified in the command data was not valid.

0x0C I2C_TRANSFER_FAILED
    The I2C transaction failed.

.. _section-command-overview:

Command Overview
-------------------------------------

.. _table-command-overview:

.. table:: Overview of all commands

    +----------+-----------------------------------------------------------------------------------+------+-------+
    | ID       | Name                                                                              | Read | Write |
    +==========+===================================================================================+======+=======+
    | **0x01** | :ref:`DEVICEID <subsection-command-deviceid>`                                     | yes  |  no   |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x02** | :ref:`FIRMWARENAME <subsection-command-firmwarename>`                             | yes  |  no   |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x03** | :ref:`FIRMWAREVERSION <subsection-command-firmwareversion>`                       | yes  |  no   |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x04** | :ref:`DEVICEUUID <subsection-command-deviceuuid>`                                 | yes  |  no   |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x05** | :ref:`ENTERBOOTLOADER <subsection-command-enterbootloader>`                       | no   |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x06** | :ref:`RESET <subsection-command-reset>`                                           | no   |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x07** | :ref:`ENABLE <subsection-command-enable>`                                         | yes  |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x08** | :ref:`SETPOINT <subsection-command-setpoint>`                                     | yes  |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x09** | :ref:`PROCESSVALUE <subsection-command-processvalue>`                             | yes  |  no   |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x0A** | :ref:`VOLTAGE <subsection-command-voltage>`                                       | yes  |  no   |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x0B** | :ref:`RESISTANCE <subsection-command-resistance>`                                 | yes  |  no   |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x0C** | :ref:`INPUTCURRENT <subsection-command-inputcurrent>`                             | yes  |  no   |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x0D** | :ref:`INPUTCURRENTMAX <subsection-command-inputcurrentmax>`                       | yes  |  no   |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x0E** | :ref:`MODE <subsection-command-mode>`                                             | yes  |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x0F** | :ref:`MODECONFIGURATION <subsection-command-modeconfiguration>`                   | yes  |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x10** | :ref:`STATEMACHINECONFIGURATION <subsection-command-statemachineconfiguration>`   | yes  |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x11** | :ref:`MONITORINGCONFIGURATION <subsection-command-monitoringconfiguration>`       | yes  |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x12** | :ref:`CCSOURCECONFIGURATION <subsection-command-ccsourceconfiguration>`           | yes  |  yes  |
    +          +-----------------------------------------------------------------------------------+      +       +
    |          | :ref:`CCSOURCECONFIGURATION V2 <subsection-command-ccsourceconfiguration-v2>`     |      |       |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x13** | :ref:`DACCALIBRATION <subsection-command-daccalibration>`                         | yes  |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x14** | :ref:`ADCCONFIGURATION <subsection-command-adcconfiguration>`                     | yes  |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x15** | :ref:`ADCCURRENTCALIBRATION <subsection-command-adccurrentcalibration>`           | yes  |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x16** | :ref:`ADCINPUTCURRENTCALIBRATION <subsection-command-adcinputcurrentcalibration>` | yes  |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x17** | :ref:`ADCVOLTAGECALIBRATION <subsection-command-adcvoltagecalibration>`           | yes  |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x18** | :ref:`PUSHBUTTONCONFIGURATION <subsection-command-pushbuttonconfiguration>`       | yes  |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x19** | :ref:`I2CCONFIGURATION <subsection-command-i2cconfiguration>`                     | yes  |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x1A** | :ref:`UNLOCK <subsection-command-unlock>`                                         | no   |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x1B** | :ref:`SAVETOEEPROM <subsection-command-savetoeeprom>`                             | no   |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x1C** | :ref:`MEASURERESISTANCE <subsection-command-measureresistance>`                   | yes  |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x1D** | :ref:`CHANNELINFO <subsection-command-channelinfo>`                               | yes  |  no   |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x1E** | :ref:`DIGITALOUTPUT <subsection-command-digitaloutput>`                           | yes  |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x1F** | :ref:`VOLTAGESOURCE <subsection-command-voltagesource>`                           | yes  |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x20** | :ref:`ANALOGINPUT <subsection-command-analoginput>`                               | yes  |  no   |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x21** | :ref:`I2CCONTROLLER <subsection-command-i2ccontroller>`                           | no   |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+
    | **0x22** | :ref:`I2CCONTROLLERSPEED <subsection-command-i2ccontroller-speed>`                | no   |  yes  |
    +----------+-----------------------------------------------------------------------------------+------+-------+

.. _section-identify-commands:

Identify Commands
-------------------------------------

These commands can be used to identify specific products and their revision. Reading also the unique identifier to differentiate units of the same type.

.. _subsection-command-deviceid:

DEVICEID
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x01
:Read supported: True
:Write supported: False

Get several identifiers of ECU-P hardware. The values returned depend on the specific memetis product and revision. There is a list of :ref:`hardware <section-supported-commands-per-hardware>` and their supported commands.

- **Read Mode**
    :Command data length: 0

    .. table:: Command DEVICEID

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x01 | 0x3F | 0x7D | 0x1F |
        +-----------------+------+------+------+------+------+

    :Response data length: 4
    :DEVICEID: identifier of mircocontroller family
    :DERIVID: identifier for specific microcontroller within family
    :REVID: identifier for microcontroller harwdare revision
    :HARDWAREID: memetis product identifier

    .. table:: Response DEVICEID

        +-----------------+------+------+------+----------+---------+-------+------------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3        | 4       | 5     | 6          | 7 | 8 |
        +=================+======+======+======+=====+====+=========+=======+============+===+===+
        | **Value**       | 0x09 | 0x01 | 0x2B | DEVICEID | DERIVID | REVID | HARDWAREID | CRC16 |
        +-----------------+------+------+------+----------+---------+-------+------------+---+---+

.. _subsection-command-firmwarename:

FIRMWARENAME
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x02
:Read supported: True
:Write supported: False

Get the name of the firmware running on this ECU-P. The respone has no fixed length because the firmware name has a variable length across differnt hardware.

- **Read Mode**
    :Command data length: 0

    .. table:: Command FIRMWARENAME

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x02 | 0x3F | 0x2E | 0x4A |
        +-----------------+------+------+------+------+------+

    :Response data length: variable, depends on the firmware name to be transmitted
    :FIRMWARENAME: name of the firmware running on this ECU-P, encoded in ASCII

    .. table:: Response FIRMWARENAME

        +-----------------+------+------+------+---+---+---+---+-----+-------+-------+
        | Byte Number     | 0    | 1    | 2    | 3 | 4 | 5 | 6 | ... | N - 2 | N - 1 |
        +=================+======+======+======+===+===+===+===+=====+=======+=======+
        | **Value**       | N    | 0x02 | 0x2B | FIRMWARENAME        | CRC16         |
        +-----------------+------+------+------+---+---+---+---+-----+-------+-------+

.. _subsection-command-firmwareversion:

FIRMWAREVERSION
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x03
:Read supported: True
:Write supported: False

Get the version of the firmware running on this ECU-P. The respone has no fixed length because the firmware version can have a variable length across differnt hardware.

- **Read Mode**
    :Command data length: 0

    .. table:: Command FIRMWAREVERSION

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x03 | 0x3F | 0x1F | 0x79 |
        +-----------------+------+------+------+------+------+

    :Response data length: variable, depends on the firmware version to be transmitted
    :FIRMWAREVERSION: version of the firmware running on this ECU-P, encoded in ASCII

    .. table:: Response FIRMWAREVERSION

        +-----------------+------+------+------+---+---+---+---+-----+-------+-------+
        | Byte Number     | 0    | 1    | 2    | 3 | 4 | 5 | 6 | ... | N - 2 | N - 1 |
        +=================+======+======+======+===+===+===+===+=====+=======+=======+
        | **Value**       | N    | 0x03 | 0x2B | FIRMWAREVERSION     | CRC16         |
        +-----------------+------+------+------+---+---+---+---+-----+-------+-------+

.. _subsection-command-deviceuuid:

DEVICEUUID
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x04
:Read supported: True
:Write supported: False

Get the universally unique identifier (UUID) of this ECU-P unit. The UUID identifies specific units of the same memetis product.

- **Read Mode**
    :Command data length: 0

    .. table:: Command DEVICEUUID

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x04 | 0x3F | 0x88 | 0xE0 |
        +-----------------+------+------+------+------+------+

    :Response data length: 16
    :UUID: 128 bit universally unique identifier

    .. table:: Response FIRMWAREVERSION

        +-----------------+------+------+------+---+---+---+-----+----+----+----+
        | Byte Number     | 0    | 1    | 2    | 3 | 4 | 5 | ... | 18 | 19 | 20 |
        +=================+======+======+======+===+===+===+=====+====+====+====+
        | **Value**       | 0x15 | 0x04 | 0x2B | UUID                 | CRC16   |
        +-----------------+------+------+------+---+---+---+-----+----+----+----+

.. _section-general-commands:

General Commands
-------------------------------------

These commands affect all channels at the same time and are not related to changing factory settings.

.. _subsection-command-reset:

RESET
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x06
:Read supported: False
:Write supported: True

Reset the unit to it's power up state. This is equivalent to turning the power off and back on again.

- **Write Mode**
    :Command data length: 0

    .. table:: Command RESET

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x06 | 0x21 | 0x15 | 0x75 |
        +-----------------+------+------+------+------+------+

    :Response data length: 0

    .. table:: Response RESET

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x06 | 0x2B | 0x5F | 0xD4 |
        +-----------------+------+------+------+------+------+

.. _subsection-command-mode:

MODE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x0E
:Read supported: True
:Write supported: True

Read the current mode or set a new mode. Currently only two modes are available:

automatic mode
    Outputs can only be enabled/disabled, the output current profile etc. cannot be changed. The behaviour is defined by the statemachine configuration.

manual mode
    Full control over outputs, like changing output current etc. The statemachine configuration is not used.

- **Read Mode**
    :Command data length: 0

    .. table:: Command MODE (read)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x0E | 0x3F | 0x43 | 0x0F |
        +-----------------+------+------+------+------+------+

    :Response data length: 1
    :MODE: 0x00 if in automatic mode, 0x01 if in manual mode

    .. table:: Response MODE (read)

        +-----------------+------+------+------+------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3    | 4 | 5 |
        +=================+======+======+======+======+===+===+
        | **Value**       | 0x06 | 0x0E | 0x2B | MODE | CRC16 |
        +-----------------+------+------+------+------+---+---+

- **Write Mode**
    :Command data length: 1
    :MODE: 0x00 to set automatic mode, 0x01 to set manual mode

    .. table:: Command MODE (write)

        +-----------------+------+------+------+------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3    | 4 | 5 |
        +=================+======+======+======+======+===+===+
        | **Value**       | 0x06 | 0x0E | 0x21 | MODE | CRC16 |
        +-----------------+------+------+------+------+---+---+

    :Response data length: 0

    .. table:: Response MODE (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x0E | 0x2B | 0xF6 | 0x5D |
        +-----------------+------+------+------+------+------+

.. _subsection-command-inputcurrent:

INPUTCURRENT
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x0C
:Read supported: True
:Write supported: False

Get the input current of the ECU-P. The hardware measures it's own power consumption and can limit it by automaticall turning off the outputs.

- **Read Mode**
    :Command data length: 0

    .. table:: Command INPUTCURRENT

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x0C | 0x3F | 0x21 | 0x69 |
        +-----------------+------+------+------+------+------+

    :Response data length: 2
    :CURRENT: in 0.1mA, so 1000 equals 100mA, lower byte transmitted first

    .. table:: Response INPUTCURRENT

        +-----------------+------+------+------+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4  | 5 | 6 |
        +=================+======+======+======+====+====+===+===+
        | **Value**       | 0x07 | 0x0C | 0x2B | CURRENT | CRC16 |
        +-----------------+------+------+------+----+----+---+---+

.. _subsection-command-inputcurrentmax:

INPUTCURRENTMAX
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x0D
:Read supported: True
:Write supported: False

Get the maximum allowed input current. On some hardware this depends on the USB power supply used.

- **Read Mode**
    :Command data length: 0

    .. table:: Command INPUTCURRENTMAX

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x0D | 0x3F | 0x10 | 0x5A |
        +-----------------+------+------+------+------+------+

    :Response data length: 2
    :CURRENT: in 0.1mA, so 1000 equals 100mA, lower byte transmitted first

    .. table:: Response INPUTCURRENTMAX

        +-----------------+------+------+------+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4  | 5 | 6 |
        +=================+======+======+======+====+====+===+===+
        | **Value**       | 0x07 | 0x0D | 0x2B | CURRENT | CRC16 |
        +-----------------+------+------+------+----+----+---+---+

.. _subsection-command-measureresistance:

MEASURERESISTANCE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x1C
:Read supported: True
:Write supported: True

Get or set resistance measurement mode. If set to 0x00, the resistance will only be measured when the output is enabled. When set to 0x01, the resistance will always be measured. For this the output will be turned on shortly for the measurement. This results in a small output current that is always present.

- **Read Mode**
    :Command data length: 0

    .. table:: Command MEASURERESISTANCE (read)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x1C | 0x3F | 0x52 | 0x6A |
        +-----------------+------+------+------+------+------+

    :Response data length: 1
    :MEAS: 0x00 if resistance is only measured when output is enabled, 0x01 if resistance is always measured

    .. table:: Response MEASURERESISTANCE (read)

        +-----------------+------+------+------+------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3    | 4 | 5 |
        +=================+======+======+======+======+===+===+
        | **Value**       | 0x06 | 0x1C | 0x2B | MEAS | CRC16 |
        +-----------------+------+------+------+------+---+---+

- **Write Mode**
    :Command data length: 1
    :MEAS: 0x00 to only measure resistance when output is enabled, 0x01 to always measure resistance

    .. table:: Command MEASURERESISTANCE (write)

        +-----------------+------+------+------+------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3    | 4 | 5 |
        +=================+======+======+======+======+===+===+
        | **Value**       | 0x06 | 0x1C | 0x21 | MEAS | CRC16 |
        +-----------------+------+------+------+------+---+---+

    :Response data length: 0

    .. table:: Response MEASURERESISTANCE (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x1C | 0x2B | 0xE7 | 0x38 |
        +-----------------+------+------+------+------+------+

.. _subsection-command-voltagesource:

VOLTAGESOURCE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x1F
:Read supported: True
:Write supported: True

Get or set the output voltage of the voltage source. If the value is 0, the voltage source is disabled. The values are in 1mV, so 1000 equals 1V.

- **Read Mode**
    :Command data length: 0

    .. table:: Command VOLTAGESOURCE (read)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x1F | 0x3F | 0x01 | 0x3F |
        +-----------------+------+------+------+------+------+

    :Response data length: 2
    :VOLTAGE: 0 means the voltage source is diabled, in 1mV, so 1000 equals 1V, lower byte transmitted first

    .. table:: Response VOLTAGESOURCE (read)

        +-----------------+------+------+------+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4  | 5 | 6 |
        +=================+======+======+======+====+====+===+===+
        | **Value**       | 0x07 | 0x1F | 0x2B | VOLTAGE | CRC16 |
        +-----------------+------+------+------+----+----+---+---+

- **Write Mode**
    :Command data length: 2
    :VOLTAGE: 0 to disable voltage source, in 1mV, so 1000 equals 1V, lower byte transmitted first

    .. table:: Command VOLTAGESOURCE (write)

        +-----------------+------+------+------+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4  | 5 | 6 |
        +=================+======+======+======+====+====+===+===+
        | **Value**       | 0x07 | 0x1F | 0x21 | VOLTAGE | CRC16 |
        +-----------------+------+------+------+----+----+---+---+

    :Response data length: 0

    .. table:: Response VOLTAGESOURCE (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x1F | 0x2B | 0xB4 | 0x6D |
        +-----------------+------+------+------+------+------+

.. _subsection-command-i2ccontroller:

I2CCONTROLLER
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x21
:Read supported: False
:Write supported: True

Write and read bytes on the I2C bus as controller.

This is a combined command to first write bytes (if any) to a peripheral, then generating a repeated start condition and then reading back bytes (if any) from the peripheral. If no bytes are to be written the peripheral is only addressed once to read data and vice versa.

- **Write Mode**
    :Command data length: variable, depends on the number of bytes to be transmitted on the I2C bus
    :ADDRESS: I2C 7-bit address of the peripheral on the I2C bus
    :WRITE_LENGTH: number of bytes to be written to the peripheral, 0 if data should only be read from the peripheral
    :READ_LENGTH: number of bytes to be read from the peripheral, 0 if data should only be written to the peripheral
    :WRITE_DATA: data bytes to be written to the peripheral

    .. table:: Command I2CCONTROLLER

        +-----------------+------+------+------+---------+--------------+-------------+---+---+---+-----+-------+-------+
        | Byte Number     | 0    | 1    | 2    | 3       | 4            | 5           | 6 | 7 | 8 | ... | N - 2 | N - 1 |
        +=================+======+======+======+=========+==============+=============+===+===+===+=====+=======+=======+
        | **Value**       | N    | 0x21 | 0x21 | ADDRESS | WRITE_LENGTH | READ_LENGTH | WRITE_DATA      | CRC16         |
        +-----------------+------+------+------+---------+--------------+-------------+---+---+---+-----+-------+-------+

    :Response data length: variable, depends on the number of bytes to be read from the I2C bus
    :ADDRESS: same as command
    :WRITE_LENGTH: same as command
    :READ_LENGTH: same as command
    :READ_DATA: data bytes read from the peripheral

    .. table:: Response I2CCONTROLLER

        +-----------------+------+------+------+---------+--------------+-------------+---+---+---+-----+-------+-------+
        | Byte Number     | 0    | 1    | 2    | 3       | 4            | 5           | 6 | 7 | 8 | ... | N - 2 | N - 1 |
        +=================+======+======+======+=========+==============+=============+===+===+===+=====+=======+=======+
        | **Value**       | N    | 0x21 | 0x2B | ADDRESS | WRITE_LENGTH | READ_LENGTH | READ_DATA       | CRC16         |
        +-----------------+------+------+------+---------+--------------+-------------+---+---+---+-----+-------+-------+

.. _subsection-command-i2ccontroller-speed:

I2CCONTROLLERSPEED
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x22
:Read supported: True
:Write supported: True

Get or set the I2C controller clock speed.

- **Read Mode**
    :Command data length: 0

    .. table:: Command I2CCONTROLLERSPEED (read)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x22 | 0x3F | 0xC8 | 0x4C |
        +-----------------+------+------+------+------+------+

    :Response data length: 2
    :SPEED: clock speed in kbit/s, lower byte transmitted first

    .. table:: Response I2CCONTROLLERSPEED (read)

        +-----------------+------+------+------+---+---+---+---+
        | Byte Number     | 0    | 1    | 2    | 3 | 4 | 5 | 6 |
        +=================+======+======+======+===+===+===+===+
        | **Value**       | 0x07 | 0x22 | 0x2B | SPEED | CRC16 |
        +-----------------+------+------+------+---+---+---+---+

- **Write Mode**
    :Command data length: 2
    :SPEED: clock speed in kbit/s, lower byte transmitted first

    .. table:: Command I2CCONTROLLERSPEED (write)

        +-----------------+------+------+------+---+---+---+---+
        | Byte Number     | 0    | 1    | 2    | 3 |   | 5 | 6 |
        +=================+======+======+======+===+===+===+===+
        | **Value**       | 0x07 | 0x22 | 0x21 | SPEED | CRC16 |
        +-----------------+------+------+------+---+---+---+---+

    :Response data length: 0

    .. table:: Response I2CCONTROLLERSPEED (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x22 | 0x2B | 0x7D | 0x1E |
        +-----------------+------+------+------+------+------+


.. _section-channel-specific-commands:

Channel-specific Commands
-------------------------------------

.. _subsection-command-enable:

ENABLE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x07
:Read supported: True
:Write supported: True

Read if a channel is currently enabled or enable/disable a channel.

- **Read Mode**
    :Command data length: 1
    :CH: channel number to read, starting with 1

    .. table:: Command ENABLE (read)

        +-----------------+------+------+------+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4 | 5 |
        +=================+======+======+======+====+===+===+
        | **Value**       | 0x06 | 0x07 | 0x3F | CH | CRC16 |
        +-----------------+------+------+------+----+---+---+

    :Response data length: 1
    :STATUS: 0x00 if this channel is disabled, 0x01 if it's enabled

    .. table:: Response ENABLE (read)

        +-----------------+------+------+------+--------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3      | 4 | 5 |
        +=================+======+======+======+========+===+===+
        | **Value**       | 0x06 | 0x07 | 0x2B | STATUS | CRC16 |
        +-----------------+------+------+------+--------+---+---+

- **Write Mode**
    :Command data length: 2
    :CH: channel number to set, starting with 1
    :STATUS: 0x00 to disable, 0x01 to enable this channel

    .. table:: Command ENABLE (write)

        +-----------------+------+------+------+----+--------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4      | 5 | 6 |
        +=================+======+======+======+====+========+===+===+
        | **Value**       | 0x07 | 0x07 | 0x21 | CH | STATUS | CRC16 |
        +-----------------+------+------+------+----+--------+---+---+

    :Response data length: 0

    .. table:: Command ENABLE (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x07 | 0x2B | 0x6E | 0xE7 |
        +-----------------+------+------+------+------+------+

.. _subsection-command-setpoint:

SETPOINT
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x08
:Read supported: True
:Write supported: True

Read the current setpoint of a channel or set a new one. All current values are in 0.1mA so 1000 equals 100mA.

- **Read Mode**
    :Command data length: 1
    :CH: channel number to read, starting with 1

    .. table:: Command SETPOINT (read)

        +-----------------+------+------+------+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4 | 5 |
        +=================+======+======+======+====+===+===+
        | **Value**       | 0x06 | 0x08 | 0x3F | CH | CRC16 |
        +-----------------+------+------+------+----+---+---+

    :Response data length: 2
    :CURRENT: setpoint of channel in 0.1mA, so 1000 equals 100mA, lower byte transmitted first

    .. table:: Response SETPOINT (read)

        +-----------------+------+------+------+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4  | 5 | 6 |
        +=================+======+======+======+====+====+===+===+
        | **Value**       | 0x07 | 0x08 | 0x2B | CURRENT | CRC16 |
        +-----------------+------+------+------+----+----+---+---+

- **Write Mode**
    :Command data length: 3
    :CH: channel number to read, starting with 1
    :CURRENT: new setpoint of channel in 0.1mA, so 1000 equals 100mA, lower byte transmitted first

    .. table:: Command SETPOINT (write)

        +-----------------+------+------+------+----+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4  | 5  | 6 | 7 |
        +=================+======+======+======+====+====+====+===+===+
        | **Value**       | 0x08 | 0x08 | 0x21 | CH | CURRENT | CRC16 |
        +-----------------+------+------+------+----+----+----+---+---+

    :Response data length: 0

    .. table:: Command SETPOINT (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x08 | 0x2B | 0x50 | 0xF7 |
        +-----------------+------+------+------+------+------+

.. _subsection-command-processvalue:

PROCESSVALUE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x09
:Read supported: True
:Write supported: False

Read the current process value of a channel (actual output current). All current values are in 0.1mA so 1000 equals 100mA.

- **Read Mode**
    :Command data length: 1
    :CH: channel number to read, starting with 1

    .. table:: Command PROCESSVALUE

        +-----------------+------+------+------+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4 | 5 |
        +=================+======+======+======+====+===+===+
        | **Value**       | 0x06 | 0x09 | 0x3F | CH | CRC16 |
        +-----------------+------+------+------+----+---+---+

    :Response data length: 2
    :CURRENT: process value of channel in 0.1mA, so 1000 equals 100mA, lower byte transmitted first

    .. table:: Response PROCESSVALUE

        +-----------------+------+------+------+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4  | 5 | 6 |
        +=================+======+======+======+====+====+===+===+
        | **Value**       | 0x07 | 0x09 | 0x2B | CURRENT | CRC16 |
        +-----------------+------+------+------+----+----+---+---+

.. _subsection-command-voltage:

VOLTAGE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x0A
:Read supported: True
:Write supported: False

Read the voltages at both output pins of a channel towards circuit ground. All voltages are in 1mV so 1000 equals 1V. The actual voltage across the output is the difference of the two voltages.

- **Read Mode**
    :Command data length: 1
    :CH: channel number to read, starting with 1

    .. table:: Command VOLTAGE

        +-----------------+------+------+------+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4 | 5 |
        +=================+======+======+======+====+===+===+
        | **Value**       | 0x06 | 0x0A | 0x3F | CH | CRC16 |
        +-----------------+------+------+------+----+---+---+

    :Response data length: 4
    :VOLTAGE_P: high side voltage in 1mv, so 1000 equals 1V, lower byte transmitted first
    :VOLTAGE_N: low side voltage in 1mv, so 1000 equals 1V, lower byte transmitted first

    .. table:: Response VOLTAGE

        +-----------------+------+------+------+-----+-----+-----+-----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3   | 4   | 5   | 6   | 7 | 8 |
        +=================+======+======+======+=====+=====+=====+=====+===+===+
        | **Value**       | 0x09 | 0x0A | 0x2B | VOLTAGE_P | VOLTAGE_N | CRC16 |
        +-----------------+------+------+------+-----+-----+-----+-----+---+---+

.. _subsection-command-resistance:

RESISTANCE
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x0B
:Read supported: True
:Write supported: False

Read the resistance connected to this channel. The value is in 1mΩ, so 1000 equals 1Ω. :ref:`MEASURERESISTANCE <subsection-command-measureresistance>` configures if the resistance is also measured when the output is disabled.

- **Read Mode**
    :Command data length: 1
    :CH: channel number to read, starting with 1

    .. table:: Command RESISTANCE

        +-----------------+------+------+------+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4 | 5 |
        +=================+======+======+======+====+===+===+
        | **Value**       | 0x06 | 0x0B | 0x3F | CH | CRC16 |
        +-----------------+------+------+------+----+---+---+

    :Response data length: 2
    :RESISTANCE: resistance connected to this channel, in 1mΩ, so 1000 equals 1Ω, 0 if the resistance is currently not being measured

    .. table:: Response RESISTANCE

        +-----------------+------+------+------+------+------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    | 5 | 6 |
        +=================+======+======+======+======+======+===+===+
        | **Value**       | 0x07 | 0x0B | 0x2B | RESISTANCE  | CRC16 |
        +-----------------+------+------+------+------+------+---+---+

.. _subsection-command-channelinfo:

CHANNELINFO
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x1D
:Read supported: True
:Write supported: False

Read basic values of a specific channels. This combines these commands: :ref:`subsection-command-enable`, :ref:`subsection-command-setpoint`, :ref:`subsection-command-processvalue`, :ref:`subsection-command-voltage` and :ref:`subsection-command-resistance`.

- **Read Mode**
    :Command data length: 1
    :CH: channel number to read, starting with 1

    .. table:: Command CHANNELINFO

        +-----------------+------+------+------+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4 | 5 |
        +=================+======+======+======+====+===+===+
        | **Value**       | 0x06 | 0x1D | 0x3F | CH | CRC16 |
        +-----------------+------+------+------+----+---+---+

    :Response data length: 11
    :STATUS: 0x00 if this channel is disabled, 0x01 if it's enabled
    :SETPOINT: setpoint of channel in 0.1mA, so 1000 equals 100mA, lower byte transmitted first
    :PROCESS: process value of channel in 0.1mA, so 1000 equals 100mA, lower byte transmitted first
    :VOLTAGE_P: high side voltage in 1mv, so 1000 equals 1V, lower byte transmitted first
    :VOLTAGE_N: low side voltage in 1mv, so 1000 equals 1V, lower byte transmitted first
    :RESISTANCE: resistance connected to this channel, in 1mΩ, so 1000 equals 1Ω, 0 if the resistance is currently not being measured

    .. table:: Response CHANNELINFO

        +-----------------+------+------+------+--------+-----+-----+----+----+-----+-----+-----+-----+------+------+----+----+
        | Byte Number     | 0    | 1    | 2    | 3      | 4   | 5   | 6  | 7  | 8   | 9   | 10  | 11  | 12   | 13   | 14 | 15 | 
        +=================+======+======+======+========+=====+=====+====+====+=====+=====+=====+=====+======+======+====+====+
        | **Value**       | 0x10 | 0x1D | 0x2B | STATUS | SETPOINT  | PROCESS | VOLTAGE_P | VOLTAGE_N | RESISTANCE  | CRC16   |
        +-----------------+------+------+------+--------+-----+-----+----+----+-----+-----+-----+-----+------+------+----+----+

.. _subsection-command-digitaloutput:

DIGITALOUTPUT
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x1E
:Read supported: True
:Write supported: True

Read the current digital output value of a channel or set a new one.

- **Read Mode**
    :Command data length: 1
    :CH: channel number to read, starting with 1

    .. table:: Command DIGITALOUTPUT (read)

        +-----------------+------+------+------+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4 | 5 |
        +=================+======+======+======+====+===+===+
        | **Value**       | 0x06 | 0x1E | 0x3F | CH | CRC16 |
        +-----------------+------+------+------+----+---+---+

    :Response data length: 2
    :VALUE: current digital output value, lower byte transmitted first

    .. table:: Response DIGITALOUTPUT (read)

        +-----------------+------+------+------+---+---+---+---+
        | Byte Number     | 0    | 1    | 2    | 3 | 4 | 5 | 6 |
        +=================+======+======+======+===+===+===+===+
        | **Value**       | 0x07 | 0x1E | 0x2B | VALUE | CRC16 |
        +-----------------+------+------+------+---+---+---+---+

- **Write Mode**
    :Command data length: 3
    :CH: channel number to read, starting with 1
    :VALUE: new digital output value, lower byte transmitted first

    .. table:: Command DIGITALOUTPUT (write)

        +-----------------+------+------+------+----+---+---+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4 | 5 | 6 | 7 |
        +=================+======+======+======+====+===+===+===+===+
        | **Value**       | 0x08 | 0x1E | 0x21 | CH | VALUE | CRC16 |
        +-----------------+------+------+------+----+---+---+---+---+

    :Response data length: 0

    .. table:: Command DIGITALOUTPUT (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x1E | 0x2B | 0x85 | 0x5E |
        +-----------------+------+------+------+------+------+

.. _subsection-command-analoginput:

ANALOGINPUT
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x20
:Read supported: True
:Write supported: False

Read the analog input value of this channel. The value is in 1mΩ, so 1000 equals 1V.

- **Read Mode**
    :Command data length: 1
    :CH: channel number to read, starting with 1

    .. table:: Command ANALOGINPUT

        +-----------------+------+------+------+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4 | 5 |
        +=================+======+======+======+====+===+===+
        | **Value**       | 0x06 | 0x20 | 0x3F | CH | CRC16 |
        +-----------------+------+------+------+----+---+---+

    :Response data length: 2
    :VOLTAGE: voltage on analog input of this channel, in 1mV, so 1000 equals 1V

    .. table:: Response ANALOGINPUT

        +-----------------+------+------+------+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4  | 5 | 6 |
        +=================+======+======+======+====+====+===+===+
        | **Value**       | 0x07 | 0x20 | 0x2B | VOLTAGE | CRC16 |
        +-----------------+------+------+------+----+----+---+---+

.. _subsection-command-digitalinput:

DIGITALINPUT
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x23
:Read supported: True
:Write supported: False

Read the digial input values of this channel.

- **Read Mode**
    :Command data length: 1
    :CH: channel number to read, starting with 1

    .. table:: Command DIGITALINPUT

        +-----------------+------+------+------+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4 | 5 |
        +=================+======+======+======+====+===+===+
        | **Value**       | 0x06 | 0x23 | 0x3F | CH | CRC16 |
        +-----------------+------+------+------+----+---+---+

    :Response data length: 2
    :VALUES: status of the associated digital inputs of the request channel

    .. table:: Response DIGITALINPUT

        +-----------------+------+------+------+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4  | 5 | 6 |
        +=================+======+======+======+====+====+===+===+
        | **Value**       | 0x07 | 0x23 | 0x2B | VALUES  | CRC16 |
        +-----------------+------+------+------+----+----+---+---+

.. _section-configuration-commands:

Configuration Commands
-------------------------------------

These commands allow changing the behaviour of the ECU-P hardware. The changes will be lost after resetting or unpowering the ECU-P. To make them permanent save them to the internal memeory with :ref:`subsection-command-savetoeeprom`.

.. _subsection-command-enterbootloader:

ENTERBOOTLOADER
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x05
:Read supported: False
:Write supported: True

Enter bootloader mode. This allows firmware updates to take place.

- **Write Mode**
    :Command data length: 0

    .. table:: Command ENTERBOOTLOADER

        +-----------------+--------+------+------+------+------+
        | Byte Number     | 0      | 1    | 2    | 3    | 4    |
        +=================+========+======+======+======+======+
        | **Value**       | 0x05   | 0x05 | 0x21 | 0x46 | 0x20 |
        +-----------------+--------+------+------+------+------+

    :Response data length: 0

    .. table:: Response ENTERBOOTLOADER

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x05 | 0x2B | 0x0C | 0x81 |
        +-----------------+------+------+------+------+------+

.. _subsection-command-savetoeeprom:

SAVETOEEPROM
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x1B
:Read supported: False
:Write supported: True

Save any configuration and calibration changes to the internal memory. Without this, all changes will be lost after the next reset/power cycle.

- **Write Mode**
    :Command data length: 0

    .. table:: Command SAVETOEEPROM

        +-----------------+--------+------+------+------+------+
        | Byte Number     | 0      | 1    | 2    | 3    | 4    |
        +=================+========+======+======+======+======+
        | **Value**       | 0x05   | 0x1B | 0x21 | 0x3A | 0x00 |
        +-----------------+--------+------+------+------+------+

    :Response data length: 0

    .. table:: Response SAVETOEEPROM

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x1B | 0x2B | 0x70 | 0xA1 |
        +-----------------+------+------+------+------+------+

.. _subsection-command-modeconfiguration:

MODECONFIGURATION
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x0F
:Read supported: True
:Write supported: True

Read or set the configuration for default mode after reset and the default current when switching to manual mode.

- **Read Mode**
    :Command data length: 0

    .. table:: Command MODECONFIGURATION (read)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x0F | 0x3F | 0x72 | 0x3C |
        +-----------------+------+------+------+------+------+

    :Response data length: 3
    :MODE: 0x00 if in automatic mode, 0x01 if in manual mode after reset
    :CURRENT: default output current of all channels in manual mode after reset, in 0.1mA, so 1000 equals 100mA, lower byte transmitted first

    .. table:: Response MODECONFIGURATION (read)

        +-----------------+------+------+------+------+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3    | 4  | 5  | 6 | 7 |
        +=================+======+======+======+======+====+====+===+===+
        | **Value**       | 0x08 | 0x0F | 0x2B | MODE | CURRENT | CRC16 |
        +-----------------+------+------+------+------+----+----+---+---+

- **Write Mode**
    :Command data length: 3
    :MODE: 0x00 if in automatic mode, 0x01 if in manual mode after reset
    :CURRENT: default output current of all channels in manual mode after reset, in 0.1mA, so 1000 equals 100mA, lower byte transmitted first

    .. table:: Command MODECONFIGURATION (write)

        +-----------------+------+------+------+------+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3    | 4  | 5  | 6 | 7 |
        +=================+======+======+======+======+====+====+===+===+
        | **Value**       | 0x08 | 0x0F | 0x21 | MODE | CURRENT | CRC16 |
        +-----------------+------+------+------+------+----+----+---+---+

    :Response data length: 0

    .. table:: Response MODECONFIGURATION (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x0F | 0x2B | 0xC7 | 0x6E |
        +-----------------+------+------+------+------+------+

.. _subsection-command-statemachineconfiguration:

STATEMACHINECONFIGURATION
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x10
:Read supported: True
:Write supported: True

Get or set the configuration of the state machine.

Multiple state machines are supported and each state can have many actions (enabling/disabling channels, etc.) when entered. The transition to a configurable next state occures if a configurable trigger event occurs (time out, push button press, serial command, etc.). This allows for automated sequeneces of output current. The state machine functionality is disabled when the ECU is in :ref:`manual mode <subsection-command-mode>`.

While writing a new configuration, no actions will be executed by the state machines. Only after writing the complete configuration the state machines are reinizialized.

Multiple reads or writes with increasing start address might be required to transmit the full binary byte stream.

- **Read Mode**
    :Command data length: 2
    :START_ADDRESS: address of first byte to read

    .. table:: Command STATEMACHINECONFIGURATION (read)

        +-----------------+------+------+------+-------+-------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3     | 4     | 5 | 6 |
        +=================+======+======+======+=======+=======+===+===+
        | **Value**       | 0x07 | 0x10 | 0x3F | START_ADDRESS | CRC16 |
        +-----------------+------+------+------+-------+-------+---+---+

    :Response data length: variable, depends on the byte stream to be transmitted
    :BYTE_STREAM: binary state machine configuration, max. 27 bytes (or less if remaing byte stream at the start address is shorter) will be returned

    .. table:: Response STATEMACHINECONFIGURATION (read)

        +-----------------+------+------+------+---+---+---+---+-----+-------+-------+
        | Byte Number     | 0    | 1    | 2    | 3 | 4 | 5 | 6 | ... | N - 2 | N - 1 |
        +=================+======+======+======+===+===+===+===+=====+=======+=======+
        | **Value**       | N    | 0x10 | 0x2B | BYTE_STREAM         | CRC16         |
        +-----------------+------+------+------+---+---+---+---+-----+-------+-------+

- **Write Mode**
    :Command data length: variable, depends on the byte stream to be transmitted
    :START_ADDRESS: address of first byte to write
    :BYTE_STREAM: binary state machine configuration, max. 27 bytes (or less if remaing byte stream at the start address is shorter) will be returned

    .. table:: Command STATEMACHINECONFIGURATION (write)

        +-----------------+------+------+------+-------+-------+---+---+---+---+-----+-------+-------+
        | Byte Number     | 0    | 1    | 2    | 3     | 4     | 5 | 6 | 7 | 8 | ... | N - 2 | N - 1 |
        +=================+======+======+======+=======+=======+===+===+===+===+=====+=======+=======+
        | **Value**       | N    | 0x10 | 0x21 | START_ADDRESS | BYTE_STREAM         | CRC16         |
        +-----------------+------+------+------+-------+-------+---+---+---+---+-----+-------+-------+

    :Response data length: 0

    .. table:: Response STATEMACHINECONFIGURATION (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x10 | 0x2B | 0x8A | 0x7D |
        +-----------------+------+------+------+------+------+

.. _subsection-command-monitoringconfiguration:

MONITORINGCONFIGURATION
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x11
:Read supported: True
:Write supported: True

Get or set the configuration for input and output current monitoring.

- **Read Mode**
    :Command data length: 0

    .. table:: Command MONITORINGCONFIGURATION (read)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x11 | 0x3F | 0x0E | 0x1C |
        +-----------------+------+------+------+------+------+

    :Response data length: 6
    :INPUT: 0x01 to enable input current monitoring, 0x00 to disable it.
    :INPUT_TIMEOUT: timout after input current was too high during which the outputs can't be enabled again, in miliseconds, lower byte transmitted first
    :OUTPUT: 0x01 to enable monitoring of output current, 0x00 to disable it
    :OUTPUT_ERROR: relative error threshold in 0.01%, so 1000 equals 10%, lower byte transmitted first

    .. table:: Response MONITORINGCONFIGURATION (read)

        +-----------------+------+------+------+-------+-------+-------+--------+-------+-------+----+----+
        | Byte Number     | 0    | 1    | 2    | 3     | 4     | 5     | 6      | 7     | 8     | 9  | 10 |
        +=================+======+======+======+=======+=======+=======+========+=======+=======+====+====+
        | **Value**       | 0x0B | 0x11 | 0x2B | INPUT | INPUT_TIMEOUT | OUTPUT | OUTPUT_ERROR  | CRC16   |
        +-----------------+------+------+------+-------+-------+-------+--------+-------+-------+----+----+

- **Write Mode**
    :Command data length: 6
    :INPUT: 0x01 to enable input current monitoring, 0x00 to disable it.
    :INPUT_TIMEOUT: timout after input current was too high during which the outputs can't be enabled again, in miliseconds, lower byte transmitted first
    :OUTPUT: 0x01 to enable monitoring of output current, 0x00 to disable it
    :OUTPUT_ERROR: relative error threshold in 0.01%, so 1000 equals 10%, lower byte transmitted first

    .. table:: Command MONITORINGCONFIGURATION (write)

        +-----------------+------+------+------+-------+-------+-------+--------+-------+-------+----+----+
        | Byte Number     | 0    | 1    | 2    | 3     | 4     | 5     | 6      | 7     | 8     | 9  | 10 |
        +=================+======+======+======+=======+=======+=======+========+=======+=======+====+====+
        | **Value**       | 0x0B | 0x11 | 0x21 | INPUT | INPUT_TIMEOUT | OUTPUT | OUTPUT_ERROR  | CRC16   |
        +-----------------+------+------+------+-------+-------+-------+--------+-------+-------+----+----+

    :Response data length: 0

    .. table:: Response MONITORINGCONFIGURATION (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x11 | 0x2B | 0xBB | 0x4E |
        +-----------------+------+------+------+------+------+

.. _subsection-command-ccsourceconfiguration:

CCSOURCECONFIGURATION
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x12
:Read supported: True
:Write supported: True

Get or set the configuration for constant current sources.

- **Read Mode**
    :Command data length: 0

    .. table:: Command CCSOURCECONFIGURATION (read)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x12 | 0x3F | 0x5D | 0x49 |
        +-----------------+------+------+------+------+------+

    :Response data length: 3
    :CLOSED_LOOP: 0x01 to enable close loop control of output current on all channels. This compares the measured output current with the setpoint and adjusts the DAC value accordingly. 0x00 to disable closed loop control
    :MULITPLIER: mutlitplier for output current error, result is added to DAC value, lower byte transmitted first

    .. table:: Response CCSOURCECONFIGURATION (read)

        +-----------------+------+------+------+-------------+------+------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3           | 4    | 5    | 6 | 7 |
        +=================+======+======+======+=============+======+======+===+===+
        | **Value**       | 0x08 | 0x12 | 0x2B | CLOSED_LOOP | MULITPLIER  | CRC16 |
        +-----------------+------+------+------+-------------+------+------+---+---+

- **Write Mode**
    :Command data length: 3
    :CLOSED_LOOP: 0x01 to enable close loop control of output current on all channels. This compares the measured output current with the setpoint and adjusts the DAC value accordingly. 0x00 to disable closed loop control
    :MULITPLIER: mutlitplier for output current error, result is added to DAC value, lower byte transmitted first

    .. table:: Command CCSOURCECONFIGURATION (write)

        +-----------------+------+------+------+-------------+------+------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3           | 4    | 5    | 6 | 7 |
        +=================+======+======+======+=============+======+======+===+===+
        | **Value**       | 0x08 | 0x12 | 0x21 | CLOSED_LOOP | MULITPLIER  | CRC16 |
        +-----------------+------+------+------+-------------+------+------+---+---+

    :Response data length: 0

    .. table:: Response CCSOURCECONFIGURATION (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x12 | 0x2B | 0x23 | 0xF4 |
        +-----------------+------+------+------+------+------+

.. _subsection-command-ccsourceconfiguration-v2:

CCSOURCECONFIGURATION V2
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x12
:Read supported: True
:Write supported: True

Get or set the configuration for constant current sources.

- **Read Mode**
    :Command data length: 0

    .. table:: Command CCSOURCECONFIGURATION (read)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x12 | 0x3F | 0x5D | 0x49 |
        +-----------------+------+------+------+------+------+

    :Response data length: 11
    :CLOSED_LOOP: 0x01 to enable close loop control of output current on all channels. This compares the measured output current with the setpoint and adjusts the DAC value accordingly. 0x00 to disable closed loop control
    :MULITPLIER: mutlitplier for output current error, result is added to DAC value, lower byte transmitted first
    :DELAY: clock cycles between starts of ADC conversions. This clock is running at 6 MHz, so a value of 12000 results in a sampling rate of 500 Hz. Lower byte transmitted first
    :DEALY_ADC: Clock cycles between turning on output channels and starting the ADC conversion. This gives the analog voltages time to settle. This clock is running at 6 MHz, so a value of 850 results in a delay of 142 us, lower byte transmitted first
    :PWM: 0x01 to nable switching to PWM mode for output current if setpoint is lower than threshold. The PWM frequency is determind by the DELAY and the minimal achievable on time is determind by DEALY_ADC and the conversion time of the ADC. 0x00 to disable PWM switch over
    :PWM_CURRENT: threshold current in 0.1mA below which the output switches to PWM mode, so 1000 equals 100mA, lower byte transmitted first
    :MEAS_RES: 0x01 to enable always measuring resistance of disabled channels by default after reset, 0x00 to disable default measurement after reset.

    .. table:: Response CCSOURCECONFIGURATION (read)

        +-----------------+------+------+------+-------------+------+------+---+---+-----+-----+-----+------+------+----------+----+----+
        | Byte Number     | 0    | 1    | 2    | 3           | 4    | 5    | 6 | 7 | 8   | 9   | 10  | 11   | 12   | 13       | 14 | 15 |
        +=================+======+======+======+=============+======+======+===+===+=====+=====+=====+======+======+==========+====+====+
        | **Value**       | 0x10 | 0x12 | 0x2B | CLOSED_LOOP | MULITPLIER  | DELAY | DEALY_ADC | PWM | PWM_CURRENT | MEAS_RES | CRC16   |
        +-----------------+------+------+------+-------------+------+------+---+---+-----+-----+-----+------+------+----------+----+----+

- **Write Mode**
    :Command data length: 11
    :CLOSED_LOOP: 0x01 to enable close loop control of output current on all channels. This compares the measured output current with the setpoint and adjusts the DAC value accordingly. 0x00 to disable closed loop control
    :MULITPLIER: mutlitplier for output current error, result is added to DAC value, lower byte transmitted first
    :DELAY: clock cycles between starts of ADC conversions. This clock is running at 6 MHz, so a value of 12000 results in a sampling rate of 500 Hz. Lower byte transmitted first
    :DEALY_ADC: Clock cycles between turning on output channels and starting the ADC conversion. This gives the analog voltages time to settle. This clock is running at 6 MHz, so a value of 850 results in a delay of 142 us, lower byte transmitted first
    :PWM: 0x01 to nable switching to PWM mode for output current if setpoint is lower than threshold. The PWM frequency is determind by the DELAY and the minimal achievable on time is determind by DEALY_ADC and the conversion time of the ADC. 0x00 to disable PWM switch over
    :PWM_CURRENT: threshold current in 0.1mA below which the output switches to PWM mode, so 1000 equals 100mA, lower byte transmitted first
    :MEAS_RES: 0x01 to enable always measuring resistance of disabled channels by default after reset, 0x00 to disable default measurement after reset.

    .. table:: Command CCSOURCECONFIGURATION (write)

        +-----------------+------+------+------+-------------+------+------+---+---+-----+-----+-----+------+------+----------+----+----+
        | Byte Number     | 0    | 1    | 2    | 3           | 4    | 5    | 6 | 7 | 8   | 9   | 10  | 11   | 12   | 13       | 14 | 15 |
        +=================+======+======+======+=============+======+======+===+===+=====+=====+=====+======+======+==========+====+====+
        | **Value**       | 0x10 | 0x12 | 0x21 | CLOSED_LOOP | MULITPLIER  | DELAY | DEALY_ADC | PWM | PWM_CURRENT | MEAS_RES | CRC16   |
        +-----------------+------+------+------+-------------+------+------+---+---+-----+-----+-----+------+------+----------+----+----+

    :Response data length: 0

    .. table:: Response CCSOURCECONFIGURATION (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x12 | 0x2B | 0x23 | 0xF4 |
        +-----------------+------+------+------+------+------+

.. _subsection-command-adcconfiguration:

ADCCONFIGURATION
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x14
:Read supported: True
:Write supported: True

Get or set the configuration for the analog-to-digital converter.

- **Read Mode**
    :Command data length: 0

    .. table:: Command ADCCONFIGURATION (read)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x14 | 0x3F | 0xFB | 0xE3 |
        +-----------------+------+------+------+------+------+

    :Response data length: 4
    :CURRENT_TRACK: time in ADC clock cycles for sampling the current channel inputs. This time is needed for the input capacitor to reach the input voltage. Maximum value is 63 clock cycles
    :CUR_ACCU: number of samples to average current channel inputs. Possible values are 1, 4, 8, 16, 32
    :VOL_TRACK: time in ADC clock cycles for sampling the voltage channel inputs. This time is needed for the input capacitor to reach the input voltage. Maximum value is 63 clock cycles
    :VOL_ACCU: number of samples to average voltage channel inputs. Possible values are 1, 4, 8, 16, 36

    .. table:: Response ADCCONFIGURATION (read)

        +-----------------+------+------+------+---------------+----------+-----------+----------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3             | 4        | 5         | 6        | 7 | 8 |
        +=================+======+======+======+===============+==========+===========+==========+===+===+
        | **Value**       | 0x09 | 0x14 | 0x2B | CURRENT_TRACK | CUR_ACCU | VOL_TRACK | VOL_ACCU | CRC16 |
        +-----------------+------+------+------+---------------+----------+-----------+----------+---+---+

- **Write Mode**
    :Command data length: 4
    :CURRENT_TRACK: time in ADC clock cycles for sampling the current channel inputs. This time is needed for the input capacitor to reach the input voltage. Maximum value is 63 clock cycles
    :CUR_ACCU: number of samples to average current channel inputs. Possible values are 1, 4, 8, 16, 32
    :VOL_TRACK: time in ADC clock cycles for sampling the voltage channel inputs. This time is needed for the input capacitor to reach the input voltage. Maximum value is 63 clock cycles
    :VOL_ACCU: number of samples to average voltage channel inputs. Possible values are 1, 4, 8, 16, 36

    .. table:: Command ADCCONFIGURATION (write)

        +-----------------+------+------+------+---------------+----------+-----------+----------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3             | 4        | 5         | 6        | 7 | 8 |
        +=================+======+======+======+===============+==========+===========+==========+===+===+
        | **Value**       | 0x09 | 0x14 | 0x21 | CURRENT_TRACK | CUR_ACCU | VOL_TRACK | VOL_ACCU | CRC16 |
        +-----------------+------+------+------+---------------+----------+-----------+----------+---+---+

    :Response data length: 0

    .. table:: Response ADCCONFIGURATION (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x14 | 0x2B | 0x4E | 0xB1 |
        +-----------------+------+------+------+------+------+

.. _subsection-command-pushbuttonconfiguration:

PUSHBUTTONCONFIGURATION
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x18
:Read supported: True
:Write supported: True

Get or set the configuration for the push button mode.

- **Read Mode**
    :Command data length: 0

    .. table:: Command PUSHBUTTONCONFIGURATION (read)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x18 | 0x3F | 0x96 | 0xA6 |
        +-----------------+------+------+------+------+------+

    :Response data length: 1
    :TOGGLE: 0x01 for push buttons to act in toggle mode, 0x00 for non-toggle mode

    .. table:: Response PUSHBUTTONCONFIGURATION (read)

        +-----------------+------+------+------+--------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3      | 4 | 5 |
        +=================+======+======+======+========+===+===+
        | **Value**       | 0x06 | 0x18 | 0x2B | TOGGLE | CRC16 |
        +-----------------+------+------+------+--------+---+---+

- **Write Mode**
    :Command data length: 1

    .. table:: Command PUSHBUTTONCONFIGURATION (write)

        +-----------------+------+------+------+--------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3      | 4 | 5 |
        +=================+======+======+======+========+===+===+
        | **Value**       | 0x06 | 0x18 | 0x21 | TOGGLE | CRC16 |
        +-----------------+------+------+------+--------+---+---+

    :Response data length: 0

    .. table:: Response PUSHBUTTONCONFIGURATION (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x18 | 0x2B | 0x23 | 0xF4 |
        +-----------------+------+------+------+------+------+

.. _subsection-command-i2cconfiguration:

I2CCONFIGURATION
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x19
:Read supported: True
:Write supported: True

Read current I2C address or set a new one. The address is the 7 bit I2C address right-shifted (without the R/W bit).

- **Read Mode**
    :Command data length: 0

    .. table:: Command I2CCONFIGURATION (read)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x19 | 0x3F | 0xA7 | 0x95 |
        +-----------------+------+------+------+------+------+

    :Response data length: 1
    :ADDR: current 7 bit I2C address

    .. table:: Response I2CCONFIGURATION (read)

        +-----------------+------+------+------+------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3    | 4 | 5 |
        +=================+======+======+======+======+===+===+
        | **Value**       | 0x06 | 0x19 | 0x2B | ADDR | CRC16 |
        +-----------------+------+------+------+------+---+---+

- **Write Mode**
    :Command data length: 1
    :ADDR: new 7 bit I2C address

    .. table:: Command I2CCONFIGURATION (write)

        +-----------------+------+------+------+------+---+---+
        | Byte Number     | 0    | 1    | 2    | 3    | 4 | 5 |
        +=================+======+======+======+======+===+===+
        | **Value**       | 0x06 | 0x19 | 0x21 | ADDR | CRC16 |
        +-----------------+------+------+------+------+---+---+

    :Response data length: 0

    .. table:: Response I2CCONFIGURATION (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x19 | 0x2B | 0x12 | 0xC7 |
        +-----------------+------+------+------+------+------+

.. _section-calibration-commands:

Calibration Commands
-------------------------------------

These commands allow changing the calibration values of the ECU-P hardware. The changes will be lost after resetting or unpowering the ECU-P. To make them permanent save them to the internal memeory with :ref:`subsection-command-savetoeeprom`.

Before changing any values the ECU-P needs to be unlocked to prevent changing the calibration by accident. Use the :ref:`subsection-command-unlock` command with the unlock keys for the specific hardware.

.. _subsection-command-unlock:

UNLOCK
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x1A
:Read supported: False
:Write supported: True

Unlock the calibration values. This needs to be sent before any values can be changed. The unlock keys that are required depend on the :ref:`hardware <section-supported-commands-per-hardware>`.

- **Write Mode**
    :Command data length: 2
    :KEY1: unlock key first byte, specific to the ECU-P hardware
    :KEY2: unlock key second byte, specific to the ECU-P hardware

    .. table:: Command UNLOCK

        +-----------------+--------+------+------+------+------+---+---+
        | Byte Number     | 0      | 1    | 2    | 3    | 4    | 5 | 6 |
        +=================+========+======+======+======+======+===+===+
        | **Value**       | 0x07   | 0x1A | 0x21 | KEY1 | KEY2 | CRC16 |
        +-----------------+--------+------+------+------+------+---+---+

    :Response data length: 0

    .. table:: Response UNLOCK

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x1A | 0x2B | 0x41 | 0x92 |
        +-----------------+------+------+------+------+------+

.. _subsection-command-daccalibration:

DACCALIBRATION
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x13
:Read supported: True
:Write supported: True

Read the current DAC calibration values or set new ones for this channel. The calibration needs to be :ref:`unlocked <subsection-command-unlock>` first.

This is used to calculate the DAC output voltage from the set output current. The following formula is used (output_current in 0.1mA, so 1000 equals 100mA):

DAC_VALUE = (output_current * MULTIPLIER / 2^10 + OFFSET) / 2^6

- **Read Mode**
    :Command data length: 1
    :CH: channel number to read, starting with 1

    .. table:: Command DACCALIBRATION (read)

        +-----------------+------+------+------+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4 | 5 |
        +=================+======+======+======+====+===+===+
        | **Value**       | 0x06 | 0x13 | 0x3F | CH | CRC16 |
        +-----------------+------+------+------+----+---+---+

    :Response data length: 4
    :MULTIPLIER: lower byte transmitted first
    :OFFSET: lower byte transmitted first

    .. table:: Response DACCALIBRATION (read)

        +-----------------+------+------+------+------+------+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    | 5  | 6  | 7 | 8 |
        +=================+======+======+======+======+======+====+====+===+===+
        | **Value**       | 0x09 | 0x13 | 0x2B | MULTIPLIER  | OFFSET  | CRC16 |
        +-----------------+------+------+------+------+------+----+----+---+---+

- **Write Mode**
    :Command data length: 5
    :CH: channel number to read, starting with 1
    :MULTIPLIER: lower byte transmitted first
    :OFFSET: lower byte transmitted first

    .. table:: Command DACCALIBRATION (write)

        +-----------------+------+------+------+----+------+------+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4    | 5    | 6  | 7  | 8 | 9 |
        +=================+======+======+======+====+======+======+====+====+===+===+
        | **Value**       | 0x0A | 0x13 | 0x21 | CH | MULTIPLIER  | OFFSET  | CRC16 |
        +-----------------+------+------+------+----+------+------+----+----+---+---+

    :Response data length: 0

    .. table:: Response DACCALIBRATION (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x13 | 0x2B | 0xD9 | 0x28 |
        +-----------------+------+------+------+------+------+

.. _subsection-command-adccurrentcalibration:

ADCCURRENTCALIBRATION
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x15
:Read supported: True
:Write supported: True

Read the current ADC current calibration values or set new ones for this channel. The calibration needs to be :ref:`unlocked <subsection-command-unlock>` first.

This is used to calculate the output current from the measured ADC value. The following formula is used (output_current in 0.1mA, so 1000 equals 100mA):

output_current = (ADC_VALUE * MULTIPLIER / 2^9 + OFFSET - 32768) / 2^5

- **Read Mode**
    :Command data length: 1
    :CH: channel number to read, starting with 1

    .. table:: Command ADCCURRENTCALIBRATION (read)

        +-----------------+------+------+------+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4 | 5 |
        +=================+======+======+======+====+===+===+
        | **Value**       | 0x06 | 0x15 | 0x3F | CH | CRC16 |
        +-----------------+------+------+------+----+---+---+

    :Response data length: 4
    :MULTIPLIER: lower byte transmitted first
    :OFFSET: lower byte transmitted first

    .. table:: Response ADCCURRENTCALIBRATION (read)

        +-----------------+------+------+------+------+------+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    | 5  | 6  | 7 | 8 |
        +=================+======+======+======+======+======+====+====+===+===+
        | **Value**       | 0x09 | 0x15 | 0x2B | MULTIPLIER  | OFFSET  | CRC16 |
        +-----------------+------+------+------+------+------+----+----+---+---+

- **Write Mode**
    :Command data length: 5
    :CH: channel number to read, starting with 1
    :MULTIPLIER: lower byte transmitted first
    :OFFSET: lower byte transmitted first

    .. table:: Command ADCCURRENTCALIBRATION (write)

        +-----------------+------+------+------+----+------+------+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4    | 5    | 6  | 7  | 8 | 9 |
        +=================+======+======+======+====+======+======+====+====+===+===+
        | **Value**       | 0x0A | 0x15 | 0x21 | CH | MULTIPLIER  | OFFSET  | CRC16 |
        +-----------------+------+------+------+----+------+------+----+----+---+---+

    :Response data length: 0

    .. table:: Response ADCCURRENTCALIBRATION (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x15 | 0x2B | 0x7F | 0x82 |
        +-----------------+------+------+------+------+------+

.. _subsection-command-adcinputcurrentcalibration:

ADCINPUTCURRENTCALIBRATION
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x16
:Read supported: True
:Write supported: True

Read the current ADC current calibration values or set new ones for this channel. The calibration needs to be :ref:`unlocked <subsection-command-unlock>` first.

This is used to calculate the USB current from the measured ADC value. The following formula is used (input_current in 0.1mA, so 1000 equals 100mA):

input_current = (ADC_VALUE * MULTIPLIER / 2^9 + OFFSET - 32768) / 2^5

- **Read Mode**
    :Command data length: 0
    :CH: channel number to read, starting with 1

    .. table:: Command ADCINPUTCURRENTCALIBRATION (read)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x16 | 0x3F | 0x99 | 0x85 |
        +-----------------+------+------+------+------+------+

    :Response data length: 4
    :MULTIPLIER: lower byte transmitted first
    :OFFSET: lower byte transmitted first

    .. table:: Response ADCINPUTCURRENTCALIBRATION (read)

        +-----------------+------+------+------+------+------+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    | 5  | 6  | 7 | 8 |
        +=================+======+======+======+======+======+====+====+===+===+
        | **Value**       | 0x09 | 0x16 | 0x2B | MULTIPLIER  | OFFSET  | CRC16 |
        +-----------------+------+------+------+------+------+----+----+---+---+

- **Write Mode**
    :Command data length: 4
    :CH: channel number to read, starting with 1
    :MULTIPLIER: lower byte transmitted first
    :OFFSET: lower byte transmitted first

    .. table:: Command ADCINPUTCURRENTCALIBRATION (write)

        +-----------------+------+------+------+------+------+----+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    | 5  | 6  | 7 | 8 |
        +=================+======+======+======+======+======+====+====+===+===+
        | **Value**       | 0x09 | 0x16 | 0x21 | MULTIPLIER  | OFFSET  | CRC16 |
        +-----------------+------+------+------+------+------+----+----+---+---+

    :Response data length: 0

    .. table:: Response ADCINPUTCURRENTCALIBRATION (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x16 | 0x2B | 0x2C | 0xD7 |
        +-----------------+------+------+------+------+------+

.. _subsection-command-adcvoltagecalibration:

ADCVOLTAGECALIBRATION
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:ID: 0x17
:Read supported: True
:Write supported: True

Read the current ADC voltage calibration values or set new ones for this channel. The calibration needs to be :ref:`unlocked <subsection-command-unlock>` first.

This is used to calculate the output voltage from the measured ADC value. Both, the high and the low side of the output channel are measured. The voltage across the output is the difference of the high side and the low side voltage. The following formulas are used (output_voltage in 1mV, so 1000 equals 1V):

output_voltage_high_side = (ADC_VALUE * MULTIPLIER_P / 2^9 + OFFSET_P - 32768) / 2^5

output_voltage_low_side = (ADC_VALUE * MULTIPLIER_N / 2^9 + OFFSET_N - 32768) / 2^5

- **Read Mode**
    :Command data length: 1
    :CH: channel number to read, starting with 1

    .. table:: Command ADCVOLTAGECALIBRATION (read)

        +-----------------+------+------+------+----+---+---+
        | Byte Number     | 0    | 1    | 2    | 3  | 4 | 5 |
        +=================+======+======+======+====+===+===+
        | **Value**       | 0x06 | 0x17 | 0x3F | CH | CRC16 |
        +-----------------+------+------+------+----+---+---+

    :Response data length: 8
    :MULTIPLIER_P: lower byte transmitted first
    :OFFSET_P: lower byte transmitted first
    :MULTIPLIER_N: lower byte transmitted first
    :OFFSET_N: lower byte transmitted first

    .. table:: Response ADCVOLTAGECALIBRATION (read)

        +-----------------+------+------+------+-------+-------+-----+-----+-------+-------+-----+-----+----+----+
        | Byte Number     | 0    | 1    | 2    | 3     | 4     | 5   | 6   | 7     | 8     | 9   | 10  | 11 | 12 |
        +=================+======+======+======+=======+=======+=====+=====+=======+=======+=====+=====+====+====+
        | **Value**       | 0x0D | 0x17 | 0x2B | MULTIPLIER_P  | OFFSET_P  | MULTIPLIER_N  | OFFSET_N  | CRC16   |
        +-----------------+------+------+------+-------+-------+-----+-----+-------+-------+-----+-----+----+----+

- **Write Mode**
    :Command data length: 9
    :CH: channel number to read, starting with 1
    :MULTIPLIER_P: lower byte transmitted first
    :OFFSET_P: lower byte transmitted first
    :MULTIPLIER_N: lower byte transmitted first
    :OFFSET_N: lower byte transmitted first

    .. table:: Command ADCVOLTAGECALIBRATION (write)

        +-----------------+------+------+------+----+-------+-------+-----+-----+-------+-------+-----+-----+----+----+
        | Byte Number     | 0    | 1    | 2    | 3  | 4     | 5     | 6   | 7   | 8     | 9     | 10  | 11  | 12 | 13 |
        +=================+======+======+======+====+=======+=======+=====+=====+=======+=======+=====+=====+====+====+
        | **Value**       | 0x0E | 0x17 | 0x21 | CH | MULTIPLIER_P  | OFFSET_P  | MULTIPLIER_N  | OFFSET_N  | CRC16   |
        +-----------------+------+------+------+----+-------+-------+-----+-----+-------+-------+-----+-----+----+----+

    :Response data length: 0

    .. table:: Response ADCVOLTAGECALIBRATION (write)

        +-----------------+------+------+------+------+------+
        | Byte Number     | 0    | 1    | 2    | 3    | 4    |
        +=================+======+======+======+======+======+
        | **Value**       | 0x05 | 0x17 | 0x2B | 0x1D | 0xE4 |
        +-----------------+------+------+------+------+------+

.. _section-supported-commands-per-hardware:

Supported Commands per Hardware
-------------------------------------

ECU-2I15-10
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:DEVICEID: 0x34
:DERIVID: 0x45
:HARDWAREID: 0xE7
:Firmware Version: <= 1.2
:UNLOCK KEY1: 0x34
:UNLOCK KEY2: 0xBE

.. tabs::
    .. group-tab:: Identify

        * :ref:`subsection-command-deviceid`
        * :ref:`subsection-command-firmwarename`
        * :ref:`subsection-command-firmwareversion`
        * :ref:`subsection-command-deviceuuid`

    .. group-tab:: General

        * :ref:`subsection-command-reset`
        * :ref:`subsection-command-mode`
        * :ref:`subsection-command-inputcurrent`
        * :ref:`subsection-command-inputcurrentmax`

    .. group-tab:: Channel-specific

        * :ref:`subsection-command-enable`
        * :ref:`subsection-command-setpoint`
        * :ref:`subsection-command-processvalue`
        * :ref:`subsection-command-voltage`
        * :ref:`subsection-command-resistance`

    .. group-tab:: Configuration

        * :ref:`subsection-command-enterbootloader`
        * :ref:`subsection-command-savetoeeprom`
        * :ref:`subsection-command-modeconfiguration`
        * :ref:`subsection-command-statemachineconfiguration`
        * :ref:`subsection-command-monitoringconfiguration`
        * :ref:`subsection-command-ccsourceconfiguration`
        * :ref:`subsection-command-adcconfiguration`
        * :ref:`subsection-command-pushbuttonconfiguration`
        * :ref:`subsection-command-i2cconfiguration`

    .. group-tab:: Calibration

        * :ref:`subsection-command-unlock`
        * :ref:`subsection-command-daccalibration`
        * :ref:`subsection-command-adccurrentcalibration`
        * :ref:`subsection-command-adcinputcurrentcalibration`
        * :ref:`subsection-command-adcvoltagecalibration`

ECU-2I15-11
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:DEVICEID: 0x34
:DERIVID: 0x42 or 0x45
:HARDWAREID: 0xE7
:Firmware Version: >= 1.3
:UNLOCK KEY1: 0x34
:UNLOCK KEY2: 0xBE

.. tabs::
    .. group-tab:: Identify

        * :ref:`subsection-command-deviceid`
        * :ref:`subsection-command-firmwarename`
        * :ref:`subsection-command-firmwareversion`
        * :ref:`subsection-command-deviceuuid`

    .. group-tab:: General

        * :ref:`subsection-command-reset`
        * :ref:`subsection-command-mode`
        * :ref:`subsection-command-inputcurrent`
        * :ref:`subsection-command-inputcurrentmax`
        * :ref:`subsection-command-measureresistance`

    .. group-tab:: Channel-specific

        * :ref:`subsection-command-enable`
        * :ref:`subsection-command-setpoint`
        * :ref:`subsection-command-processvalue`
        * :ref:`subsection-command-voltage`
        * :ref:`subsection-command-resistance`
        * :ref:`subsection-command-channelinfo`

    .. group-tab:: Configuration

        * :ref:`subsection-command-enterbootloader`
        * :ref:`subsection-command-savetoeeprom`
        * :ref:`subsection-command-modeconfiguration`
        * :ref:`subsection-command-statemachineconfiguration`
        * :ref:`subsection-command-monitoringconfiguration`
        * :ref:`subsection-command-ccsourceconfiguration-v2`
        * :ref:`subsection-command-adcconfiguration`
        * :ref:`subsection-command-pushbuttonconfiguration`
        * :ref:`subsection-command-i2cconfiguration`

    .. group-tab:: Calibration

        * :ref:`subsection-command-unlock`
        * :ref:`subsection-command-daccalibration`
        * :ref:`subsection-command-adccurrentcalibration`
        * :ref:`subsection-command-adcinputcurrentcalibration`
        * :ref:`subsection-command-adcvoltagecalibration`

ECU-P2
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:DEVICEID: 0x34
:DERIVID: 0x42 or 0x45
:HARDWAREID: 0xE8
:UNLOCK KEY1: 0x34
:UNLOCK KEY2: 0xBE

.. tabs::
    .. group-tab:: Identify

        * :ref:`subsection-command-deviceid`
        * :ref:`subsection-command-firmwarename`
        * :ref:`subsection-command-firmwareversion`
        * :ref:`subsection-command-deviceuuid`

    .. group-tab:: General

        * :ref:`subsection-command-reset`
        * :ref:`subsection-command-mode`
        * :ref:`subsection-command-inputcurrent`
        * :ref:`subsection-command-inputcurrentmax`
        * :ref:`subsection-command-measureresistance`

    .. group-tab:: Channel-specific

        * :ref:`subsection-command-enable`
        * :ref:`subsection-command-setpoint`
        * :ref:`subsection-command-processvalue`
        * :ref:`subsection-command-voltage`
        * :ref:`subsection-command-resistance`
        * :ref:`subsection-command-channelinfo`

    .. group-tab:: Configuration

        * :ref:`subsection-command-enterbootloader`
        * :ref:`subsection-command-savetoeeprom`
        * :ref:`subsection-command-modeconfiguration`
        * :ref:`subsection-command-statemachineconfiguration`
        * :ref:`subsection-command-monitoringconfiguration`
        * :ref:`subsection-command-ccsourceconfiguration-v2`
        * :ref:`subsection-command-adcconfiguration`
        * :ref:`subsection-command-pushbuttonconfiguration`
        * :ref:`subsection-command-i2cconfiguration`

    .. group-tab:: Calibration

        * :ref:`subsection-command-unlock`
        * :ref:`subsection-command-daccalibration`
        * :ref:`subsection-command-adccurrentcalibration`
        * :ref:`subsection-command-adcinputcurrentcalibration`
        * :ref:`subsection-command-adcvoltagecalibration`

ECU-PCON-mp6quad
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:DEVICEID: 0x30
:DERIVID: 0x02 or 0x18
:HARDWAREID: 0xA1

.. tabs::
    .. group-tab:: Identify

        * :ref:`subsection-command-deviceid`
        * :ref:`subsection-command-firmwarename`
        * :ref:`subsection-command-firmwareversion`
        * :ref:`subsection-command-deviceuuid`

    .. group-tab:: General

        * :ref:`subsection-command-reset`
        * :ref:`subsection-command-i2ccontroller`
        * :ref:`subsection-command-i2ccontroller-speed`

    .. group-tab:: Channel-specific

    .. group-tab:: Configuration

        * :ref:`subsection-command-enterbootloader`

    .. group-tab:: Calibration

ECU-PCON-mp6single
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:DEVICEID: 0x30
:DERIVID: 0x02 or 0x18
:HARDWAREID: 0xA9

.. tabs::
    .. group-tab:: Identify

        * :ref:`subsection-command-deviceid`
        * :ref:`subsection-command-firmwarename`
        * :ref:`subsection-command-firmwareversion`
        * :ref:`subsection-command-deviceuuid`

    .. group-tab:: General

        * :ref:`subsection-command-reset`
        * :ref:`subsection-command-i2ccontroller`
        * :ref:`subsection-command-i2ccontroller-speed`

    .. group-tab:: Channel-specific

    .. group-tab:: Configuration

        * :ref:`subsection-command-enterbootloader`

    .. group-tab:: Calibration

ECU-PCON-ABP2LAN
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:DEVICEID: 0x30
:DERIVID: 0x02 or 0x18
:HARDWAREID: 0xB1

.. tabs::
    .. group-tab:: Identify

        * :ref:`subsection-command-deviceid`
        * :ref:`subsection-command-firmwarename`
        * :ref:`subsection-command-firmwareversion`
        * :ref:`subsection-command-deviceuuid`

    .. group-tab:: General

        * :ref:`subsection-command-reset`
        * :ref:`subsection-command-i2ccontroller`
        * :ref:`subsection-command-i2ccontroller-speed`

    .. group-tab:: Channel-specific

    .. group-tab:: Configuration

        * :ref:`subsection-command-enterbootloader`

    .. group-tab:: Calibration

ECU-PCON-SLF3
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:DEVICEID: 0x30
:DERIVID: 0x02 or 0x18
:HARDWAREID: 0xB9

.. tabs::
    .. group-tab:: Identify

        * :ref:`subsection-command-deviceid`
        * :ref:`subsection-command-firmwarename`
        * :ref:`subsection-command-firmwareversion`
        * :ref:`subsection-command-deviceuuid`

    .. group-tab:: General

        * :ref:`subsection-command-reset`
        * :ref:`subsection-command-i2ccontroller`
        * :ref:`subsection-command-i2ccontroller-speed`

    .. group-tab:: Channel-specific

    .. group-tab:: Configuration

        * :ref:`subsection-command-enterbootloader`

    .. group-tab:: Calibration
