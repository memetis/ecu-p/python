ECU-P I2C Address Change
=====================================

This guide will help you change the I2C address of ECU-P hardware.

Preparations
-------------------------------------

Make sure Python, the VCP driver and the ECU-P Python library are all setup. You can follow the :ref:`Getting Started <section-gettingstarted>` guide for instructions.


I2C Address
-------------------------------------

By default ECU-P hardware listens to the 7 bit I2C address `20` (0x14). This address can be changed over USB and I2C with the :ref:`I2CCONFIGURATION <subsection-command-i2cconfiguration>` command. The new address need to be in the range of 0 - 127.

With the :ref:`SAVETOEEPROM <subsection-command-savetoeeprom>` command the new address can be stored permanently in the EEPROM, so that the ECU-P firmware listens to the new address after a reset (by disconnecting the power supply for example) by default.
Without the save command the I2C address is reverted back to the default address after a reset.

The two commands used to permanently change the address are implemented in the :ref:`ECUFactorySettings Python class <section-api-ecufactorysettings>`.


Use Python to change the address
-------------------------------------

.. code-block::

    import logging

    from ecu import ECUFactorySettings, ECUManager

    new_i2c_address = 61  # set your new address here

    # Configure logging
    log = logging.getLogger()
    log.setLevel(logging.INFO)
    logHandler = logging.StreamHandler()
    logFormat = logging.Formatter('%(asctime)s %(levelname)s - '
                                  '%(name)s: %(message)s')
    logHandler.setFormatter(logFormat)
    log.addHandler(logHandler)

    manager = ECUManager()
    print('List of all connected ECUs:')
    print('-----------------------------------')
    for ecu in manager.get_all():
        print(ecu)
    print('-----------------------------------')

    if len(manager.get_all()) == 0:
        log.error('no ECU-P connected')
        raise ConnectionError('no ECU-P connected')
    elif len(manager.get_all()) > 1:
        log.error('more than one ECU-P connected, '
                  'you should change the address one by one')
        raise ConnectionError('more than one ECU-P connected')
    ecu_factory_settings = ECUFactorySettings.from_ecu(manager.get_all()[0])

    # read current I2C address
    log.info('current I2C address %d',
             ecu_factory_settings.get_configuration_i2c()['address'])

    # set new I2C address
    ecu_factory_settings.set_configuration_i2c(address=new_i2c_address)
    log.info('set I2C address to %d', new_i2c_address)

    # read I2C address again
    log.info('now I2C address is %d',
             ecu_factory_settings.get_configuration_i2c()['address'])

    # save to EEPROM to make change persist across resets
    ecu_factory_settings.save_eeprom()

Change `new_i2c_address` to the new address you want to set, then run the Python program.
