Supported Devices
=====================================

This library supports controlling the following devices:

.. table:: Supported Devices

    +------------------------+-----------------------------------------------------------------------------------------------------------------+------------+----------+
    | **Name**               | Description                                                                                                     | Product ID | Channels |
    +========================+=================================================================================================================+============+==========+
    | **ECU-P8 v1.0**        | 8 channel ECU-P with additional 16 digital outputs per channel, analog input and a configurable voltage source. | 0x35       | 8        |
    +------------------------+                                                                                                                 +------------+----------+
    | **ECU-P8 v1.1**        | It can measure the actuator current, voltage and resistance, allowing for self-sensing closed-loop control.     | 0x36       | 8        |
    +------------------------+-----------------------------------------------------------------------------------------------------------------+------------+----------+
    | **ECU-P1 v1.0**        | 1 channel ECU-P as an PCB-mountable module. It only offers I2C communication, no USB.                           | 0x86       | 1        |
    +------------------------+                                                                                                                 +------------+----------+
    | **ECU-P1 v1.1**        | It can measure the actuator current, voltage and resistance, allowing for self-sensing closed-loop control.     | 0x87       | 1        |
    +------------------------+-----------------------------------------------------------------------------------------------------------------+------------+----------+
    | **ECU-PCON-mp6quad**   | Interface to Bartels Mikortechnik Highdriver4 piezo pump driver. It gives full control of up to 4 mp6 pumps.    | 0xA1       | 4        |
    +------------------------+-----------------------------------------------------------------------------------------------------------------+------------+----------+
    | **ECU-PCON-mp6single** | Interface to Bartels Mikortechnik Highdriver4 piezo pump driver. It gives full control of up to 4 mp6 pumps.    | 0xA9       | 1        |
    +------------------------+-----------------------------------------------------------------------------------------------------------------+------------+----------+
    | **ECU-PCON-ABP2LAN**   | Single pressure sensor.                                                                                         | 0xB1       | 0        |
    +------------------------+-----------------------------------------------------------------------------------------------------------------+------------+----------+
    | **ECU-PCON-SLF3**      | Interface to Sensiron liquid flow sensors.                                                                      | 0xB9       | 0        |
    +------------------------+-----------------------------------------------------------------------------------------------------------------+------------+----------+
    | **ECU-2I15-10**        | 2 channel ECU-P with USB and I2C communication.                                                                 | 0xE7       | 2        |
    +------------------------+                                                                                                                 +------------+----------+
    | **ECU-2I15-11**        | It can measure the actuator current, voltage and resistance, allowing for self-sensing closed-loop control.     | 0xE7       | 2        |
    +------------------------+                                                                                                                 +------------+----------+
    | **ECU-P2**             |                                                                                                                 | 0xE8       | 2        |
    +------------------------+-----------------------------------------------------------------------------------------------------------------+------------+----------+
