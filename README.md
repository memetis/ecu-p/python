# Python Library for ECU-P hardware

## Getting Started
Visist [https://memetis.gitlab.io/ecu-p/python/gettingstarted.html](https://memetis.gitlab.io/ecu-p/python/gettingstarted.html) for a short guide to get you started using the ECU-P hardware with Python.

## Installation

### SiLabs VCP Driver
Communicating with the ECU-P over USB requieres a hardware driver to be installed. Please visit [SiLabs VCP Driver Download Page](https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers) to get the latest version.

### Install using pip
Install/update to the latest version of this library with
```
pip3 install https://gitlab.com/memetis/ecu-p/python/-/archive/master/python-master.zip
```

## Documentation
Visit  [https://memetis.gitlab.io/ecu-p/python/](https://memetis.gitlab.io/ecu-p/python/) for a documentation of this library.

## Something not working
Please report anything not working as expected or that could be improved.
Feel free to open an issue here in Gitlab or contact us at [contact@memetis.com](mailto:contact@memetis.com).

## License
This library is published under the MIT license. See [LICENSE](LICENSE) for details.