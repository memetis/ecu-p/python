# The MIT License (MIT)
# Copyright (c) 2020 Karl-Petter Lindegaard
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from ctypes import (POINTER, Structure, c_char, c_uint16, create_string_buffer,
                    string_at)
from typing import (TYPE_CHECKING, Any, Iterable, List, Optional, Sequence,
                    SupportsBytes, Union)

if TYPE_CHECKING:
    from ecu.ecu import ECU

# SMBus transfer read or write markers from uapi/linux/i2c.h
I2C_SMBUS_WRITE = 0
I2C_SMBUS_READ = 1


class i2c_msg(Structure):
    """
    As defined in ``i2c.h``.
    """
    _fields_ = [
        ('addr', c_uint16),
        ('flags', c_uint16),
        ('len', c_uint16),
        ('buf', POINTER(c_char))]

    def __iter__(self):
        """ Iterator / Generator
        :return: iterates over :py:attr:`buf`
        :rtype: :py:class:`generator` which returns int values
        """
        idx = 0
        while idx < self.len:
            yield ord(self.buf[idx])
            idx += 1

    def __len__(self) -> int:
        return self.len

    def __bytes__(self) -> bytes:
        return string_at(self.buf, self.len)

    def __repr__(self) -> str:
        return 'i2c_msg(%d,%d,%r)' % (self.addr, self.flags, self.__bytes__())

    def __str__(self) -> str:
        s = self.__bytes__()
        # Throw away non-decodable bytes
        return s.decode(errors="ignore")

    @staticmethod
    def read(address: int, length: int) -> "i2c_msg":
        """
        Prepares an i2c read transaction.
        :param address: Slave address.
        :type: address: int
        :param length: Number of bytes to read.
        :type: length: int
        :return: New :py:class:`i2c_msg` instance for read operation.
        :rtype: :py:class:`i2c_msg`
        """
        arr = create_string_buffer(length)
        return i2c_msg(addr=address, flags=I2C_SMBUS_READ, len=length, buf=arr)

    @staticmethod
    def write(address: int,
              buf: Union[str, Iterable[int], SupportsBytes]) -> "i2c_msg":
        """
        Prepares an i2c write transaction.
        :param address: Slave address.
        :type address: int
        :param buf: Bytes to write. Either list of values or str.
        :type buf: list
        :return: New :py:class:`i2c_msg` instance for write operation.
        :rtype: :py:class:`i2c_msg`
        """
        if isinstance(buf, str):
            buf = bytes(map(ord, buf))
        else:
            buf = bytes(buf)
        arr = create_string_buffer(buf, len(buf))
        return i2c_msg(addr=address, flags=I2C_SMBUS_WRITE, len=len(arr), buf=arr)


class SMBus():
    def __init__(self, ecu: 'ECU'):
        self._ecu = ecu

    def _write(self, address: int,
               data: Union[Iterable[int], SupportsBytes]) -> None:
        self._ecu.i2c_transfer(address, write_data=bytes(data))

    def _read(self, address: int, length: int) -> bytes:
        return self._ecu.i2c_transfer(address, read_length=length)

    def _write_read(self, address: int,
                    data: Union[Iterable[int], SupportsBytes],
                    length: int) -> bytes:
        return self._ecu.i2c_transfer(address, write_data=bytes(data),
                                      read_length=length)

    def open(self, bus: Any) -> None:
        pass

    def close(self) -> None:
        pass

    def write_quick(self, i2c_address: int,
                    force: Optional[bool] = None) -> None:
        """
        Perform quick transaction. Sends I2C start condition and transmits
        addres and immediatly sends stop signal. No actual data will be
        transmitted to the addressed device.

        :param i2c_address: I2C address
        :type i2c_address: int
        :param force: will be ignored
        :type force: Boolean
        :rtype: None
        """
        self._write(i2c_address, bytes())

    def read_byte(self, i2c_address: int, force: Optional[bool] = None) -> int:
        """
        Read a single byte from a device.

        :param i2c_address: I2C address
        :type i2c_address: int
        :param force: will be ignored
        :type force: Boolean
        :return: Read byte value
        :rtype: int
        """
        return int(self._read(i2c_address, 1)[0])

    def write_byte(self, i2c_address: int, value: int,
                   force: Optional[bool] = None) -> None:
        """
        Write a single byte to a device.

        :param i2c_address: I2C address
        :type i2c_address: int
        :param value: value to write
        :type value: int
        :param force: will be ignored
        :type force: Boolean
        :rtype: None
        """
        self._write(i2c_address, [value])

    def read_byte_data(self, i2c_address: int, register: int,
                       force: Optional[bool] = None) -> int:
        """
        Read a single byte from a designated register.
        :param i2c_address: i2c address
        :type i2c_address: int
        :param register: Register to read
        :type register: int
        :param force: will be ignored
        :type force: Boolean
        :return: Read byte value
        :rtype: int
        """
        result = self._write_read(i2c_address, [register], 1)
        if len(result) != 1:
            raise ValueError()
        return int(result[0])

    def write_byte_data(self, i2c_address: int, register: int, value: int,
                        force: Optional[bool] = None) -> None:
        """
        Write a byte to a given register.
        :param i2c_address: i2c address
        :type i2c_address: int
        :param register: Register to write to
        :type register: int
        :param value: Byte value to transmit
        :type value: int
        :param force: will be ignored
        :type force: Boolean
        :rtype: None
        """
        self._write(i2c_address, bytes([register, value]))

    def read_word_data(self, i2c_address: int, register: int,
                       force: Optional[bool] = None) -> int:
        """
        Read a single word (2 bytes) from a given register.
        :param i2c_address: i2c address
        :type i2c_address: int
        :param register: Register to read
        :type register: int
        :param force: will be ignored
        :type force: Boolean
        :return: 2-byte word
        :rtype: int
        """
        result = self._write_read(i2c_address, [register], 2)
        if len(result) != 2:
            raise ValueError()
        return int((result[1] << 8) | result[0])

    def write_word_data(self, i2c_address: int, register: int, value: int,
                        force: Optional[bool] = None) -> None:
        """
        Write a single word (2 bytes) to a given register.
        :param i2c_address: i2c address
        :type i2c_address: int
        :param register: Register to write to
        :type register: int
        :param value: Word value to transmit
        :type value: int
        :param force: will be ignored
        :type force: Boolean
        :rtype: None
        """
        data = bytes([register, value & 0xFF, (value >> 8) & 0xFF])
        self._write(i2c_address, data)

    def process_call(self, i2c_address: int, register: int, value: int,
                     force: Optional[bool] = None):
        """
        Executes a SMBus Process Call, sending a 16-bit value and receiving a
        16-bit response

        :param i2c_address: i2c address
        :type i2c_address: int
        :param register: Register to read/write to
        :type register: int
        :param value: Word value to transmit
        :type value: int
        :param force:
        :type force: Boolean
        :rtype: int
        """
        # TODO
        raise NotImplementedError

    def read_block_data(self, i2c_address: int, register: int,
                        force: Optional[bool] = None) -> List[int]:
        """
        Read a block of up to 32-bytes from a given register.
        :param i2c_address: i2c address
        :type i2c_address: int
        :param register: Start register
        :type register: int
        :param force:
        :type force: Boolean
        :return: List of bytes
        :rtype: list
        """
        # TODO
        raise NotImplementedError

    def write_block_data(self, i2c_address: int, register: int,
                         data: Sequence[int],
                         force: Optional[bool] = None) -> None:
        """
        Write a block of byte data to a given register.
        :param i2c_address: i2c address
        :type i2c_address: int
        :param register: Start register
        :type register: int
        :param data: List of bytes
        :type data: list
        :param force:
        :type force: Boolean
        :rtype: None
        """
        # TODO
        raise NotImplementedError

    def block_process_call(self, i2c_address: int, register: int,
                           data: Sequence[int],
                           force: Optional[bool] = None) -> List[int]:
        """
        Executes a SMBus Block Process Call, sending a variable-size data
        block and receiving another variable-size response
        :param i2c_address: i2c address
        :type i2c_address: int
        :param register: Register to read/write to
        :type register: int
        :param data: List of bytes
        :type data: list
        :param force:
        :type force: Boolean
        :return: List of bytes
        :rtype: list
        """
        # TODO
        raise NotImplementedError

    def read_i2c_block_data(self, i2c_address: int, register: int, length: int,
                            force: Optional[bool] = None) -> List[int]:
        """
        Read a block of byte data from a given register.

        :param i2c_address: I2C address
        :type i2c_address: int
        :param register: Start register
        :type register: int
        :param length: Desired block length
        :type length: int
        :param force: will be ignored
        :type force: Boolean
        :return: List of bytes
        :rtype: list
        """
        return list(self._write_read(i2c_address, [register], length))

    def write_i2c_block_data(self, i2c_address: int, register: int,
                             data: Sequence[int],
                             force: Optional[bool] = None) -> None:
        """
        Write a block of byte data to a given register.

        :param i2c_address: I2C address
        :type i2c_address: int
        :param register: Start register
        :type register: int
        :param data: List of bytes
        :type data: list
        :param force: will be ignored
        :type force: Boolean
        :rtype: None
        """
        if isinstance(data, str):
            data = data.encode('ascii')
        if isinstance(data, int):
            data = bytes([data])
        if isinstance(data, list):
            data = bytes(data)
        self._write(i2c_address, bytes([register]) + bytes(data))

    def i2c_rdwr(self, *i2c_msgs: i2c_msg) -> None:
        """
        Combine a series of i2c read and write operations in a single
        transaction (with repeated start bits but no stop bits in between).
        This method takes i2c_msg instances as input, which must be created
        first with :py:meth:`i2c_msg.read` or :py:meth:`i2c_msg.write`.
        :param i2c_msgs: One or more i2c_msg class instances.
        :type i2c_msgs: i2c_msg
        :rtype: None
        """
        if len(i2c_msgs) == 0:
            raise ValueError
        if (len(i2c_msgs) == 2
                and (i2c_msgs[0].flags != I2C_SMBUS_WRITE
                     or i2c_msgs[1].flags == I2C_SMBUS_WRITE)):
            raise NotImplementedError
        if (len(i2c_msgs) == 2 and i2c_msgs[0].addr != i2c_msgs[1].addr):
            raise NotImplementedError
        if len(i2c_msgs) > 2:
            raise NotImplementedError

        # Write transaction
        if len(i2c_msgs) == 1 and i2c_msgs[0].flags == 0:
            self._write(i2c_msgs[0].addr, bytes(i2c_msgs[0]))
        # Read transaction
        elif len(i2c_msgs) == 1 and i2c_msgs[0].flags != 0:
            result = self._read(i2c_msgs[0].addr, i2c_msgs[0].len)
            i2c_msgs[0].buf = create_string_buffer(bytes(result), len(result))
        # Write then read transactions
        elif len(i2c_msgs) == 2:
            result = self._write_read(
                i2c_msgs[0].addr,
                bytes(i2c_msgs[0]),
                i2c_msgs[1].len
            )
            i2c_msgs[1].buf = create_string_buffer(bytes(result), len(result))
