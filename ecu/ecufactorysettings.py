# The MIT License (MIT)
# Copyright 2021-2022 memetis GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import json
import logging
import os
import pathlib
import time
from typing import Any, Dict, List, Optional, Union

from intelhex import IntelHex

from ecu.communication import (Command, CommunicationBootloader,
                               CommunicationI2C)
from ecu.ecu import ECU

log = logging.getLogger(__name__)


class ECUFactorySettings(ECU):
    """
    Connect to an ECU in factory mode. This allows making changes to the
    configuration and calibration of the ECU.

    :param interface: the communication interface to use. If interface is
                      of type `Serial` or str, communication via USB is
                      used. If interface is of type 'SMBus', communication
                      via I2C is used.
    :type interface: `Serial`, str or `SMBus`
    :param retry: how many times a command should be resent if a
                  transmission fails.
    :type retry: int
    :param timeout: how long to wait for a response to commands in seconds
    :type timeout: float
    :param i2c_address: the address to use for I2C communication.
    :type i2c_address: int
    :raises ConnectionError: if communication with ECU fails
    """
    @classmethod
    def from_ecu(cls, ecu: ECU) -> 'ECUFactorySettings':
        ecu.__class__ = ECUFactorySettings
        return ecu  # type: ignore[return-value]

    def enter_bootloader(self) -> None:
        """
        Enter serial-only bootloader on ECU.
        This is part of the firmware update process and can only be done over
        USB.

        :return: True if successfully entered bootloader
        :rtype: bool
        :raises TypeError: if connected over I2C
        :raises ConnectionError: if communication with ECU fails
        """
        if isinstance(self._interface, CommunicationI2C):
            raise TypeError("bootloader mode is only supported on USB "
                            "connection, not over I2C")

        # Enter bootloader
        if 'enter_bootloader' not in self._hardware['commands']:
            raise ConnectionError('ENTERBOOTLOADER is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['enter_bootloader']()
        command.send(self._interface)

        # Give ECU time to reset to bootloader
        time.sleep(0.2)

        # Update hardware information
        try:
            self._get_hardware_type()
        except ConnectionError as exc:
            raise ConnectionError('failed to enter bootloader') from exc

        log.info('entered bootloader on ECU %s', self.uuid_short)

    def firmware_write(self, filename: pathlib.Path,
                       overwrite_eeprom: bool = False) -> None:
        self._check_file(filename, read=True)

        # Load hex data from file
        hexdata = IntelHex()
        hexdata.loadhex(filename)

        # Remove any bytes over maximum allowed firmware size
        # to prevent flash-emulated EEPROM data being overwritten.
        # If this is skipped the stored configuration and calibration
        # is lost.
        end_address = 0x77FF
        if 'bytestream_start' in self._hardware['firmware']:
            end_address = (self._hardware['firmware']['bytestream_start'])
        if not overwrite_eeprom:
            hexdata = hexdata[0:end_address]

        # Enter bootloader if it's not running
        if not self.bootloader_active:
            self.enter_bootloader()

        # Write firmware
        bootloader = CommunicationBootloader(self._interface.serial_port)
        bootloader.write_firmware(hexdata)

        # Reset
        bootloader.reset()

    def unlock(self) -> None:
        """
        Unlock calibration data. This must be done before attempting to write
        configuration data. This prevents accidentally overwriting the
        calibration data.

        :return: None
        :raises ConnectionError: if communication with ECU fails
        """
        if 'unlock' not in self._hardware['commands']:
            raise ConnectionError('UNLOCK is not supported by this hardware')
        command = self._hardware['commands']['unlock']()
        command.key1 = self._hardware['firmware']['unlock'][0]
        command.key2 = self._hardware['firmware']['unlock'][1]
        command.send(self._interface)
        log.info('unlocked configuration and calibration on ECU %s',
                 self.uuid_short)

    def save_eeprom(self) -> None:
        """
        Save changed configuration and calibration to internal EEPROM. If this
        is not called after changing the configuration or calibration, these
        changes will be reverted after the ECU resets or is powered off.

        :return: None
        :raises ConnectionError: if communication with ECU fails
        """
        if 'save_to_eeprom' not in self._hardware['commands']:
            raise ConnectionError('SAVETOEEPROM is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['save_to_eeprom']()
        command.send(self._interface)
        log.info('saved configuration and calibration to EEPROM on ECU %s',
                 self.uuid_short)

    def _check_file(self, filename: pathlib.Path, read: bool = True) -> None:
        # TODO: update to use pathlib
        # Check if file is available
        if read and not os.access(filename, os.R_OK):
            raise ValueError("readinng file '{}' failed".format(filename))

        # Check if file exists and can be written
        # or parent folder can be written into
        if not read:
            if os.path.exists(filename):
                if not os.path.isfile(filename):
                    raise ValueError("'{}' is a directory".format(filename))
                if not os.access(filename, os.W_OK):
                    raise ValueError("writing file '{}' failed"
                                     .format(filename))
            else:
                parent_dir = os.path.dirname(filename)
                if not parent_dir:
                    parent_dir = '.'
                if not os.access(parent_dir, os.W_OK):
                    raise ValueError("writing file '{}' failed"
                                     .format(filename))

    def get_configuration_mode(self) -> Dict[str, Union[bool, float]]:
        """
        Get the configuration for default mode after reset and the default
        current when switching to manual mode.

        :return: Dictionary with the following keys:

                 * ``manual_mode`` (*bool*): Is manual mode enabled after
                   reset?
                 * ``default_current`` (*float*): Default current that is set
                   when switchung to manual mode.
        :rtype: dict
        :raises ConnectionError: if communication with ECU fails
        """
        if 'configuration_mode' not in self._hardware['commands']:
            raise ConnectionError('MODECONFIG is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['configuration_mode']()
        command.write_mode = False
        command.send(self._interface)

        ret = {}
        ret['manual_mode'] = command.manual_mode
        ret['default_current'] = command.current
        return ret

    def set_configuration_mode(self, manual_mode: Optional[bool] = None,
                               default_current: Optional[float] = None) -> None:
        """
        Set the configuration for default mode after reset and the default
        current when switching to manual mode.

        :param manual_mode: If true, ECU will be in manual mode after reset.
                            This deactivates all state machines. So the
                            outputs will not turn off after the configured
                            amount of time and the output current can be set
                            with ``ECU.set_setpoint()``.
        :type manual_mode: bool
        :param default_current: The default current in mA that all outputs are
                                set to, when switching to manual mode.
        :type default_current: float
        :return: None
        :raises TypeError: if the types don't match
        :raises ValueError: if default_current is out of range
        :raises ConnectionError: if communication with ECU fails

        Use ``save_eeprom()`` to save the change to EEPROM. If not saved, the
        change will be reverted during the next reset/power off.
        """
        # Read current configuration
        if 'configuration_mode' not in self._hardware['commands']:
            raise ConnectionError('MODECONFIG is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['configuration_mode']()
        command.write_mode = False
        command.send(self._interface)

        # Set new values
        if manual_mode is not None:
            if not isinstance(manual_mode, bool):
                raise TypeError("'manual_mode' must either be True or False")
            command.manual_mode = manual_mode
        if default_current is not None:
            self._check_current(default_current)
            command.current = default_current
        command.write_mode = True
        command.send(self._interface)

    def get_configuration_statemachine(self) -> Dict[str, bytes]:
        """
        Get the configuration of the state machine.

        :return: Dictionary with the following keys:

                 * ``byte_stream`` (*bytearray*): State machine byte-stream
                    configuration.
        :rtype: dict
        :raises ValueError: if byte-stream doesn't contain overall length in
                            first 2 bytes
        :raises TimeoutError: if the byte-stream is not fully transmitted
                              within 2 seconds
        :raises ConnectionError: if communication with ECU fails
        """
        if 'configuration_state_machine' not in self._hardware['commands']:
            raise ConnectionError('STATEMACHINECONFIG is not supported by '
                                  'this hardware')
        command = self._hardware['commands']['configuration_state_machine']()
        command.write_mode = False

        # Data is transmitted in chunks of max. 27 bytes. The overall length
        # of the state machine byte-stream configuration is encoded in the
        # first 2 bytes. To prevent an infinite loop, timeout after 2 seconds.
        byte_stream = bytearray()
        expected_length = 0
        start_time = time.perf_counter()

        # Read the first chunk
        command.address = 0
        command.send(self._interface)
        byte_stream += command.byte_data

        # Get the expected length from first 2 bytes
        if len(byte_stream) < 2:
            raise ValueError('received byte_stream is too short')
        expected_length = (byte_stream[1] << 8) | byte_stream[0]

        # Get the rest of the data or timeout
        while len(byte_stream) < expected_length \
                and time.perf_counter() - start_time < 2:
            command.address = len(byte_stream)
            command.send(self._interface)
            byte_stream += command.byte_data

        # Check if a timeout occured
        if len(byte_stream) < expected_length:
            raise TimeoutError('byte_stream was not fully read, operation '
                               'timed out')

        ret = {}
        ret['byte_stream'] = bytes(byte_stream)
        return ret

    def set_configuration_statemachine(self, byte_stream: Optional[bytes] = None) -> None:
        """
        Set the configuration of the state machine.

        Multiple state machines are supported and each
        state can have many actions (enabling/disabling
        channels, etc.) when entered. The transition to a
        configurable next state occures if a configurable
        trigger event occurs (time out, push button press,
        serial command, etc.). This allows for automated
        sequeneces of output current. The state machine
        functionality is disabled when the ECU is in
        **manual mode**.

        While writing a new configuration, no actions will be executed
        by the state machines. Only after writing the complete configuration
        the state machines are reinizialized.

        :param byte_stream: State machine configuration in byte-stream form.
        :type byte_stream: bytearray or bytes
        :return: None
        :raises TypeError: if the types don't match
        :raises ValueError: if byte_stream is too long
        :raises ConnectionError: if communication with ECU fails

        Use ``save_eeprom()`` to save the change to EEPROM. If not saved, the
        change will be reverted during the next reset/power off.
        """
        if byte_stream is None:
            return

        if not isinstance(byte_stream, (bytes, bytearray)):
            raise TypeError("'byte_stream' must either be of"
                            "bytes or bytearray")
        if len(byte_stream) > (self._hardware['firmware']
                               ['bytestream_length']):
            raise ValueError("'byte_stream' is too long")
        # Data is transmitted in chunks of max. 27 bytes. The overall length
        # of the state machine byte-stream configuration is limited by
        # hardware.
        if 'configuration_state_machine' not in self._hardware['commands']:
            raise ConnectionError('STATEMACHINECONFIG is not supported by '
                                  'this hardware')
        command = self._hardware['commands']['configuration_state_machine']()
        command.write_mode = True

        for chunk_number in range(0, len(byte_stream) // 25 + 1):
            start_address = chunk_number * 25
            end_address = start_address + 25
            if end_address > len(byte_stream):
                end_address = len(byte_stream)

            command.address = start_address
            command.byte_data = byte_stream[start_address:end_address]
            command.send(self._interface)

    def get_configuration_monitoring(self) -> Dict[str, bool]:
        """
        Get the configuration for USB and output current monitoring.

        :return: Dictionary with the following keys:

                 * ``usb`` (*bool*): Is USB current monitoring enabled?
                 * ``usb_timeout`` (*float*): Timeout in seconds after too
                   much current was drawn
                 * ``current`` (*bool*): Is output current monitoring enabled?
                 * ``current_error`` (*float*): Maximum allowed output current
                   error in percent.
        :rtype: dict
        :raises ConnectionError: if communication with ECU fails
        """
        if 'configuration_monitoring' not in self._hardware['commands']:
            raise ConnectionError('MONITORINGCONFIG is not supported by '
                                  'this hardware')
        command = self._hardware['commands']['configuration_monitoring']()
        command.write_mode = False
        command.send(self._interface)

        ret = {}
        ret['usb'] = command.usb_monitoring
        ret['usb_timeout'] = command.usb_monitoring_timeout / 1000
        ret['current'] = command.current_monitoring
        ret['current_error'] = command.current_monitoring_error / 100
        return ret

    def set_configuration_monitoring(self, usb: Optional[bool] = None,
                                     usb_timeout: Optional[float] = None,
                                     current: Optional[bool] = None,
                                     current_error: Optional[float] = None) -> None:
        """
        Set the configuration for USB and output current monitoring.

        :param usb: Enable USB current monitoring. Checks current drawn from
                    USB port and compares it with the maximum allowed current
                    set by the USB host. If too much current is drawn,
                    all outpus will be disabled, can't be reenabled and
                    the power LED flashes.
                    After the configured timeout has passed,
                    the LED stops flashing and outputs can be enabled again.
        :type usb: bool
        :param usb_timeout: Time period in seconds,
                            during which outputs can't be enabled after
                            too much current was drawn from USB input.
        :type usb_timeout: float
        :param current: Enable output current monitoring.
                        While the output current deviates too much from the
                        set current, the channel LED will flash.
        :type current: bool
        :param current_error: Maximum allowed deviation of output current from
                              set current in percent.
        :type current_error: float
        :return: None
        :raises TypeError: if the types don't match
        :raises ValueError: if usb_timeout or current_error are out of range
        :raises ConnectionError: if communication with ECU fails

        Use ``save_eeprom()`` to save the change to EEPROM. If not saved, the
        change will be reverted during the next reset/power off.
        """
        # Read current configuration
        if 'configuration_monitoring' not in self._hardware['commands']:
            raise ConnectionError('MONITORINGCONFIG is not supported by '
                                  'this hardware')
        command = self._hardware['commands']['configuration_monitoring']()
        command.write_mode = False
        command.send(self._interface)

        # Set new values
        if usb is not None:
            if not isinstance(usb, bool):
                raise TypeError("'usb' must either be True or False")
            command.usb_monitoring = usb
        if usb_timeout is not None:
            if not isinstance(usb_timeout, (int, float)):
                raise TypeError("'usb_timeout' must be a float")
            if not 0 <= usb_timeout <= 65.534:
                raise ValueError("'usb_timeout' out of range")
            command.usb_monitoring_timeout = int(usb_timeout * 1000)
        if current is not None:
            if not isinstance(current, bool):
                raise TypeError("'current' must either be True or False")
            command.current_monitoring = current
        if current_error is not None:
            if not isinstance(current_error, (int, float)):
                raise TypeError("'current_error' must be a float")
            if not 0 <= current_error <= 655.34:
                raise ValueError("'current_error' out of range")
            command.current_monitoring_error = int(current_error * 100)
        command.write_mode = True
        command.send(self._interface)

    def get_configuration_ccsource(self) -> Dict[str, Union[bool, int]]:
        """
        Get the configuration for constant current sources. Not all firmware
        versions return all the keys.

        :return: Dictionary with the following keys:

                 * ``closed_loop_control`` (*bool*): Is closed loop control of
                   output current enabled?
                 * ``feedback_multiplier`` (*int*): Multiplier for
                   the error feedback loop.
                 * ``sample_delay`` (*int*): Clock cycles between
                   starts of ADC conversions.
                 * ``sample_delay_adc`` (*int*): Clock cycles between turning
                   on output channels and starting the ADC conversion.
                 * ``pwm_switchover`` (*bool*): Enable switching to PWM mode
                   for output current if setpoint is lower than threshold.
                 * ``pwm_switchover_threshold`` (*int*): Threshold current
                   in **mA** below which the output switches to PWM mode.
                 * ``always_measure_resistance`` (*bool*): Enable measurement
                   of resistance for disabled channels after reset.
        :rtype: dict
        :raises ConnectionError: if communication with ECU fails
        """
        # Get CC source configuration version
        if 'configuration_ccsource' not in self._hardware['commands']:
            raise ConnectionError('CCSOURCECONFIG is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['configuration_ccsource']()
        command.write_mode = False
        command.send(self._interface)

        ret = {}
        ret['closed_loop_control'] = command.closed_loop_control
        ret['feedback_multiplier'] = command.closed_loop_control_multiplier
        if hasattr(command, 'sample_delay'):
            ret['sample_delay'] = command.sample_delay
        if hasattr(command, 'sample_delay_adc'):
            ret['sample_delay_adc'] = command.sample_delay_adc
        if hasattr(command, 'pwm_switchover'):
            ret['pwm_switchover'] = command.pwm_switchover
        if hasattr(command, 'pwm_switchover_threshold'):
            ret['pwm_switchover_threshold'] = command.pwm_switchover_threshold
        if hasattr(command, 'always_measure_resistance'):
            ret['always_measure_resistance'] = (command.
                                                always_measure_resistance)
        return ret

    def set_configuration_ccsource(self, closed_loop_control: Optional[bool] = None,
                                   feedback_multiplier: Optional[int] = None,
                                   sample_delay: Optional[int] = None,
                                   sample_delay_adc: Optional[int] = None,
                                   pwm_switchover: Optional[bool] = None,
                                   pwm_switchover_threshold: Optional[float] = None,
                                   always_measure_resistance: Optional[bool] = None) -> None:
        """
        Set the configuration for constant current sources. Not all firmware
        versions support every setting.

        :param closed_loop_control: Enable close loop control of output
                                    current on all channels. This compares the
                                    measured output current with the setpoint
                                    and adjusts the DAC value accordingly.
        :type closed_loop_control: bool
        :param feedback_multiplier: If closed loop control is performed, the
                                    error in output current gets multiplied
                                    with this value and added to the previous
                                    DAC value.
        :type feedback_multiplier: int
        :param sample_delay: Clock cycles between starts of ADC conversions.
                             This clock is running at 6 MHz, so a value of
                             12000 results in a sampling rate of 500 Hz.
        :type sample_delay: int
        :param sample_delay_adc: Clock cycles between turning on output
                                 channels and starting the ADC conversion.
                                 This gives the analog voltages time to settle.
                                 This clock is running at 6 MHz, so a value of
                                 850 results in a delay of 142 us.
        :type sample_delay_adc: int
        :param pwm_switchover: Enable switching to PWM mode for output current
                               if setpoint is lower than threshold. The PWM
                               frequency is determind by the ``sample_delay``
                               and the minimal achievable on time is determind
                               by ``sample_delay_adc`` and the conversion time
                               of the ADC.
        :type pwm_switchover: bool
        :param pwm_switchover_threshold: Threshold current in **mA** below
                                         which the output switches to PWM mode.
        :type pwm_switchover_threshold: float
        :param always_measure_resistance: Enable measurement of resistance for
                                          disabled channels after reset. This
                                          means the output will be turned on
                                          during the settling time and ADC
                                          conversion with the current set in
                                          ``pwm_switchover_threshold``. The
                                          resulting output current depends on
                                          ``pwm_switchover_threshold``,
                                          ``sample_delay_adc`` and
                                          ``sample_delay``.
        :type always_measure_resistance: bool
        :return: None
        :raises TypeError: if the types don't match
        :raises ValueError: if feedback_multiplier is out of range
        :raises ConnectionError: if communication with ECU fails

        Use ``save_eeprom()`` to save the change to EEPROM. If not saved, the
        change will be reverted during the next reset/power off.
        """
        # Read current configuration
        if 'configuration_ccsource' not in self._hardware['commands']:
            raise ConnectionError('CCSOURCECONFIG is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['configuration_ccsource']()
        command.write_mode = False
        command.send(self._interface)

        # Set new values
        if closed_loop_control is not None:
            if not isinstance(closed_loop_control, bool):
                raise TypeError("'closed_loop_control' must either be "
                                "True or False")
            command.closed_loop_control = closed_loop_control
        if feedback_multiplier is not None:
            if not isinstance(feedback_multiplier, int):
                raise TypeError("'feedback_multiplier' must be an int")
            if not 0 <= feedback_multiplier <= 65534:
                raise ValueError("'feedback_multiplier' out of range")
            command.closed_loop_control_multiplier = feedback_multiplier
        if sample_delay is not None:
            if not isinstance(sample_delay, int):
                raise TypeError("'sample_delay' must be an int")
            if not 0 <= sample_delay <= 65534:
                raise ValueError("'sample_delay' out of range")
            command.sample_delay = sample_delay
        if sample_delay_adc is not None:
            if not isinstance(sample_delay_adc, int):
                raise TypeError("'sample_delay_adc' must be an int")
            if not 0 <= sample_delay_adc <= 65534:
                raise ValueError("'sample_delay_adc' out of range")
            command.sample_delay_adc = sample_delay_adc
        if pwm_switchover is not None:
            if not isinstance(pwm_switchover, bool):
                raise TypeError("'pwm_switchover' must either be "
                                "True or False")
            command.pwm_switchover = pwm_switchover
        if pwm_switchover_threshold is not None:
            self._check_current(pwm_switchover_threshold)
            command.pwm_switchover_threshold = pwm_switchover_threshold
        if always_measure_resistance is not None:
            if not isinstance(always_measure_resistance, bool):
                raise TypeError("'always_measure_resistance' must either "
                                "be True or False")
            command.always_measure_resistance = always_measure_resistance
        command.write_mode = True
        command.send(self._interface)

    def get_configuration_adc(self) -> Dict[str, int]:
        """
        Get the configuration for the analog-to-digital converter.

        :return: Dictionary with the following keys:

                 * ``current_tracking_time`` (*int*): Tracking time in ADC
                   clock cycles for current measurments.
                 * ``current_accumulate`` (*int*): Number of samples to
                   average for current measurments.
                 * ``voltage_tracking_time`` (*int*): Tracking time in ADC
                   clock cycles for voltage measurments.
                 * ``voltage_accumulate`` (*int*): Number of samples to
                   average for voltage measurments.
        :rtype: dict
        :raises ConnectionError: if communication with ECU fails
        """
        if 'configuration_adc' not in self._hardware['commands']:
            raise ConnectionError('ADCCONFIG is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['configuration_adc']()
        command.write_mode = False
        command.send(self._interface)

        ret = {}
        ret['current_tracking_time'] = command.current_tracking_time
        ret['current_accumulate'] = command.current_accumulate
        ret['voltage_tracking_time'] = command.voltage_tracking_time
        ret['voltage_accumulate'] = command.voltage_accumulate
        return ret

    def set_configuration_adc(self,
                              current_tracking_time: Optional[int] = None,
                              current_accumulate: Optional[int] = None,
                              voltage_tracking_time: Optional[int] = None,
                              voltage_accumulate: Optional[int] = None) -> None:
        """
        Set the configuration for the analog-to-digital converter.

        :param current_tracking_time: Time in ADC clock cycles
                                      for sampling the current channel inputs.
                                      This time is needed for the input
                                      capacitor to reach the input voltage.
                                      Maximum value is 63 clock cycles.
        :type current_tracking_time: int
        :param current_accumulate: Number of samples to average current
                                   channel inputs. Possible values are 1, 4,
                                   8, 16, 32.
        :type current_accumulate: int
        :param voltage_tracking_time: Time in ADC clock cycles for sampling
                                      the voltage channel inputs. This time is
                                      needed for the input capacitor to reach
                                      the input voltage. Maximum value is 63
                                      clock cycles.
        :type voltage_tracking_time: int
        :param voltage_accumulate: Number of samples to average voltage
                                   channel inputs. Possible values are 1, 4,
                                   8, 16, 36.
        :type voltage_accumulate: int
        :return: None
        :raises TypeError: if the types don't match
        :raises ValueError: if a value is out of range
        :raises ConnectionError: if communication with ECU fails

        Use ``save_eeprom()`` to save the change to EEPROM. If not saved, the
        change will be reverted during the next reset/power off.
        """
        # Read current configuration
        if 'configuration_adc' not in self._hardware['commands']:
            raise ConnectionError('ADCCONFIG is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['configuration_adc']()
        command.write_mode = False
        command.send(self._interface)

        # Set new values
        if current_tracking_time is not None:
            if not isinstance(current_tracking_time, int):
                raise TypeError("'current_tracking_time' must be an int")
            if not 0 <= current_tracking_time <= 63:
                raise ValueError("'current_tracking_time' out of range")
            command.current_tracking_time = current_tracking_time
        if current_accumulate is not None:
            if not isinstance(current_accumulate, int):
                raise TypeError("'current_accumulate' must be an int")
            if current_accumulate not in [1, 2, 4, 8, 16, 32]:
                raise ValueError("'current_accumulate' out of range")
            command.current_accumulate = current_accumulate
        if voltage_tracking_time is not None:
            if not isinstance(voltage_tracking_time, int):
                raise TypeError("'voltage_tracking_time' must be an int")
            if not 0 <= voltage_tracking_time <= 63:
                raise ValueError("'voltage_tracking_time' out of range")
            command.voltage_tracking_time = voltage_tracking_time
        if voltage_accumulate is not None:
            if not isinstance(voltage_accumulate, int):
                raise TypeError("'voltage_accumulate' must be an int")
            if voltage_accumulate not in [1, 2, 4, 8, 16, 32]:
                raise ValueError("'voltage_accumulate' out of range")
            command.voltage_accumulate = voltage_accumulate
        command.write_mode = True
        command.send(self._interface)

    def get_configuration_pushbutton(self) -> Dict[str, bool]:
        """
        Get the configuration for the push button mode.

        :return: Dictionary with the following keys:

                 * ``toggle_mode`` (*bool*): Is toggle mode enabled?
        :rtype: dict
        :raises ConnectionError: if communication with ECU fails
        """
        if 'configuration_pushbutton' not in self._hardware['commands']:
            raise ConnectionError('PUSHBUTTONCONFIG is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['configuration_pushbutton']()
        command.write_mode = False
        command.send(self._interface)

        ret = {}
        ret['toggle_mode'] = command.toggle_mode
        return ret

    def set_configuration_pushbutton(self, toggle_mode: Optional[bool] = None) -> None:
        """
        Set the configuration for the push button mode.

        :param manual_mode: If true, channels will stay on after push button
                            was pressed and released in manual mode. If false,
                            channels in manual mode will be disabled as soon as
                            the push button is released.
        :type toggle_mode: bool
        :return: None
        :raises TypeError: if the types don't match
        :raises ConnectionError: if communication with ECU fails

        Use ``save_eeprom()`` to save the change to EEPROM. If not saved, the
        change will be reverted during the next reset/power off.
        """
        # Read current configuration
        if 'configuration_pushbutton' not in self._hardware['commands']:
            raise ConnectionError('PUSHBUTTONCONFIG is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['configuration_pushbutton']()
        command.write_mode = False
        command.send(self._interface)

        # Set new values
        if toggle_mode is not None:
            if not isinstance(toggle_mode, bool):
                raise TypeError("'toggle_mode' must either be True or False")
            command.toggle_mode = toggle_mode
        command.write_mode = True
        command.send(self._interface)

    def get_configuration_i2c(self) -> Dict[str, int]:
        """
        Get the configuration for the I2C interface.

        :return: Dictionary with the following keys:

                 * ``address`` (*int*): I2C address of the ECU.
        :rtype: dict
        :raises ConnectionError: if communication with ECU fails
        """
        if 'configuration_i2c' not in self._hardware['commands']:
            raise ConnectionError('I2CCONFIG is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['configuration_i2c']()
        command.write_mode = False
        command.send(self._interface)

        ret = {}
        ret['address'] = command.address
        return ret

    def set_configuration_i2c(self, address: Optional[int] = None) -> None:
        """
        Set the configuration for the I2C interface.

        :param address: I2C slave address the ECU listens on. Possible values
                        are in range 0 - 126.
        :type address: int
        :return: None
        :raises TypeError: if the types don't match
        :raises ValueError: if address is out of range
        :raises ConnectionError: if communication with ECU fails

        Use ``save_eeprom()`` to save the change to EEPROM. If not saved, the
        change will be reverted during the next reset/power off.
        """
        # Read current configuration
        if 'configuration_i2c' not in self._hardware['commands']:
            raise ConnectionError('I2CCONFIG is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['configuration_i2c']()
        command.write_mode = False
        command.write_mode = False
        command.send(self._interface)

        # Set new values
        if address is not None:
            if not isinstance(address, int):
                raise TypeError("'address' must be an int")
            if not 0 <= address <= 126:
                raise ValueError("'address' out of range")
            command.address = address
        command.write_mode = True
        command.send(self._interface)

    def configuration_to_file(self, filename: pathlib.Path) -> None:
        """
        Writes the complete configuration to the specified output file in JSON
        format. This file can be used with ``configuration_from_file()`` to
        upload the configuration later to an ECU.

        :param filename: the file name or file path the configuration is
                         written to
        :type filename: str
        :return: None
        :raises ValueError: if filename can't be written to
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_file(filename, read=False)

        # Get all configuration and combine into one dict
        configuration: Dict[str, Any] = {}
        configuration['ecu'] = {'uuid': self.uuid,
                                'firmware': self.firmware_name,
                                'version': self.firmware_version}
        configuration['mode'] = self.get_configuration_mode()
        # JSON can't serialize bytearray
        # Replace bytearray with str representation
        statemachine = self.get_configuration_statemachine()
        configuration['statemachine'] = {'byte_stream': statemachine['byte_stream'].hex()}
        configuration['monitoring'] = self.get_configuration_monitoring()
        configuration['ccsource'] = self.get_configuration_ccsource()
        configuration['adc'] = self.get_configuration_adc()
        configuration['pushbutton'] = self.get_configuration_pushbutton()
        configuration['i2c'] = self.get_configuration_i2c()

        # Write configuration to file as JSON
        with open(filename, 'w') as file:
            json.dump(configuration, file, sort_keys=True, indent=4)

    def configuration_from_file(self, filename: pathlib.Path) -> None:
        """
        Write the configuration from the JSON file to the ECU. A file
        containing the current configuration can be generated with
        ``configuration_to_file()``. Entries in the file that are unkwown,
        will be ignored.

        :param filename: the file name or file path the configuration is
                         written to
        :type filename: str
        :return: None
        :raises ValueError: if filename can't be read or a value is out of
                            range
        :raises TypeError: if a type doesn't match
        :raises JSONDecodeError: if JSON can't be decoded
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_file(filename, read=True)

        # Read configuration from file
        with open(filename) as file:
            conf = json.load(file)

        # Write configuration to ECU
        self.set_configuration_mode(**conf['mode'])
        # JSON can't serialize bytearray
        # Replace str representation with bytearray
        statemachine = conf['statemachine']['byte_stream']
        conf['statemachine']['byte_stream'] = bytearray.fromhex(statemachine)
        self.set_configuration_statemachine(**conf['statemachine'])
        self.set_configuration_monitoring(**conf['monitoring'])
        self.set_configuration_ccsource(**conf['ccsource'])
        self.set_configuration_adc(**conf['adc'])
        self.set_configuration_pushbutton(**conf['pushbutton'])
        self.set_configuration_i2c(**conf['i2c'])

    def get_calibration_dac(self, channel: int) -> Dict[str, int]:
        """
        Get the calibration for a specific DAC channel.

        :param channel: Selects the channel to read the calibration data for.
        :type channel: int
        :return: Dictionary with the following keys:

                 * ``multiplier`` (*int*): Multiplier for calculating DAC value
                   from set output current.
                 * ``offset`` (*int*): Offset for calculating DAC value from
                   set output current.
        :rtype: dict
        :raises ValueError: if channel is out of range
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)
        if 'calibration_dac' not in self._hardware['commands']:
            raise ConnectionError('DACCAL is not supported by this hardware')
        command = self._hardware['commands']['calibration_dac']()
        command.write_mode = False
        command.channel = channel
        command.send(self._interface)

        ret = {}
        ret['multiplier'] = command.multiplier
        ret['offset'] = command.offset
        return ret

    def set_calibration_dac(self, channel: int, multiplier: Optional[int] = None,
                            offset: Optional[int] = None) -> None:
        """
        Set the calibration for a specific DAC channel.

        This is used to calculate the DAC output voltage from the set
        output current. The following formula is used
        (output_current in 0.1mA, so 1000 equals 100mA):

        DAC_VALUE = (output_current * multiplier / 2^10 + offset) / 2^6

        :param channel: Selects the channel to set the calibration data
                        for.
        :type channel: int
        :param multiplier: Multiplier from above formula.
        :type multiplier: int
        :param offset: Offset from above formula.
        :type offset: int
        :return: None
        :raises ValueError: if a value is out of range
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)
        # Read current configuration
        if 'calibration_dac' not in self._hardware['commands']:
            raise ConnectionError('DACCAL is not supported by this hardware')
        command = self._hardware['commands']['calibration_dac']()
        command.write_mode = False
        command.channel = channel
        command.send(self._interface)

        # Set new values
        if multiplier is not None:
            if not isinstance(multiplier, int):
                raise TypeError("'multiplier' must be an int")
            if not 0 <= multiplier <= 65534:
                raise ValueError("'multiplier' out of range")
            command.multiplier = multiplier
        if offset is not None:
            if not isinstance(offset, int):
                raise TypeError("'offset' must be an int")
            if not 0 <= offset <= 65534:
                raise ValueError("'offset' out of range")
            command.offset = offset
        command.write_mode = True
        command.send(self._interface)

    def get_calibration_adc_current(self, channel: int) -> Dict[str, int]:
        """
        Get the calibration for a specific ADC current measurment channel.

        :param channel: Selects the channel to read the calibration data for.
        :type channel: int
        :return: Dictionary with the following keys:

                 * ``multiplier`` (*int*): Multiplier for calculating output
                   current from ADC value.
                 * ``offset`` (*int*): Offset for calculating output current
                   from ADC value.
        :rtype: dict
        :raises ValueError: if channel is out of range
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)
        if 'calibration_adc_current' not in self._hardware['commands']:
            raise ConnectionError('ADCCURRENTCAL is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['calibration_adc_current']()
        command.write_mode = False
        command.channel = channel
        command.send(self._interface)

        ret = {}
        ret['multiplier'] = command.multiplier
        ret['offset'] = command.offset
        return ret

    def set_calibration_adc_current(self, channel: int, multiplier: Optional[int] = None,
                                    offset: Optional[int] = None) -> None:
        """
        Set the calibration for a specific ADC current measurment channel.

        This is used to calculate the output current from the measured ADC
        value. The following formula is used
        (output_current in 0.1mA, so 1000 equals 100mA):

        output_current = (ADC_VALUE * multiplier / 2^9 + offset - 32768) / 2^5

        :param channel: Selects the channel to set the calibration data
                        for.
        :type channel: int
        :param multiplier: Multiplier from above formula.
        :type multiplier: int
        :param offset: Offset from above formula.
        :type offset: int
        :return: None
        :raises ValueError: if a value is out of range
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)
        # Read current configuration
        if 'calibration_adc_current' not in self._hardware['commands']:
            raise ConnectionError('ADCCURRENTCAL is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['calibration_adc_current']()
        command.write_mode = False
        command.channel = channel
        command.send(self._interface)

        # Set new values
        if multiplier is not None:
            if not isinstance(multiplier, int):
                raise TypeError("'multiplier' must be an int")
            if not 0 <= multiplier <= 65534:
                raise ValueError("'multiplier' out of range")
            command.multiplier = multiplier
        if offset is not None:
            if not isinstance(offset, int):
                raise TypeError("'offset' must be an int")
            if not 0 <= offset <= 65534:
                raise ValueError("'offset' out of range")
            command.offset = offset
        command.write_mode = True
        command.send(self._interface)

    def get_calibration_adc_input_current(self) -> Dict[str, int]:
        """
        Get the calibration for ADC USB current measurment.

        :return: Dictionary with the following keys:

                 * ``multiplier`` (*int*): Multiplier for calculating USB
                   current from ADC value.
                 * ``offset`` (*int*): Offset for calculating USB current
                   from ADC value.
        :rtype: dict
        :raises ConnectionError: if communication with ECU fails
        """
        if 'calibration_adc_input_current' not in self._hardware['commands']:
            raise ConnectionError('ADCINPUTCURRENTCAL is not supported by '
                                  'this hardware')
        command = self._hardware['commands']['calibration_adc_input_current']()
        command.write_mode = False
        command.send(self._interface)

        ret = {}
        ret['multiplier'] = command.multiplier
        ret['offset'] = command.offset
        return ret

    def set_calibration_adc_input_current(self, multiplier: Optional[int] = None,
                                          offset: Optional[int] = None) -> None:
        """
        Set the calibration for ADC USB current measurment.

        This is used to calculate the USB current from the measured ADC
        value. The following formula is used
        (usb_current in 0.1mA, so 1000 equals 100mA):

        usb_current = (ADC_VALUE * multiplier / 2^9 + offset - 32768) / 2^5

        :param multiplier: Multiplier from above formula.
        :type multiplier: int
        :param offset: Offset from above formula.
        :type offset: int
        :return: None
        :raises ValueError: if a value is out of range
        :raises ConnectionError: if communication with ECU fails
        """
        # Read current configuration
        if 'calibration_adc_input_current' not in self._hardware['commands']:
            raise ConnectionError('ADCINPUTCURRENTCAL is not supported by '
                                  'this hardware')
        command = self._hardware['commands']['calibration_adc_input_current']()
        command.write_mode = False
        command.send(self._interface)

        # Set new values
        if multiplier is not None:
            if not isinstance(multiplier, int):
                raise TypeError("'multiplier' must be an int")
            if not 0 <= multiplier <= 65534:
                raise ValueError("'multiplier' out of range")
            command.multiplier = multiplier
        if offset is not None:
            if not isinstance(offset, int):
                raise TypeError("'offset' must be an int")
            if not 0 <= offset <= 65534:
                raise ValueError("'offset' out of range")
            command.offset = offset
        command.write_mode = True
        command.send(self._interface)

    def get_calibration_adc_voltage(self, channel: int) -> Dict[str, int]:
        """
        Get the calibration for a specific ADC voltage measurment channel.

        :param channel: Selects the channel to read the calibration data for.
        :type channel: int
        :return: Dictionary with the following keys:

                 * ``multiplier_p`` (*int*): Multiplier for calculating
                   high side output voltage from ADC value.
                 * ``offset_p`` (*int*): Offset for calculating high side
                   output voltage from ADC value.
                 * ``multiplier_n`` (*int*): Multiplier for calculating
                   low side output voltage from ADC value.
                 * ``offset_n`` (*int*): Offset for calculating low side
                   output voltage from ADC value.
        :rtype: dict
        :raises ValueError: if channel is out of range
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)
        if 'calibration_adc_voltage' not in self._hardware['commands']:
            raise ConnectionError('ADCVOLTAGECAL is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['calibration_adc_voltage']()
        command.write_mode = False
        command.channel = channel
        command.send(self._interface)

        ret = {}
        ret['multiplier_p'] = command.voltage_p_multiplier
        ret['offset_p'] = command.voltage_p_offset
        ret['multiplier_n'] = command.voltage_n_multiplier
        ret['offset_n'] = command.voltage_n_offset
        return ret

    def set_calibration_adc_voltage(self, channel: int,
                                    multiplier_p: Optional[int] = None,
                                    offset_p: Optional[int] = None,
                                    multiplier_n: Optional[int] = None,
                                    offset_n: Optional[int] = None) -> None:
        """
        Set the calibration for a specific ADC voltage measurment channel.

        This is used to calculate the output voltage from the measured ADC
        value. Both, the high and the low side of the output channel are
        measured. The voltage across the output is the difference of the
        high side and the low side voltage. The following formulas are used
        (output_voltage in 1mV, so 1000 equals 1V):

        output_voltage_high_side = (ADC_VALUE * multiplier_p / 2^9 + offset_p -
        32768) / 2^5

        output_voltage_low_side = (ADC_VALUE * multiplier_n / 2^9 + offset_n -
        32768) / 2^5

        :param channel: Selects the channel to set the calibration data
                        for.
        :type channel: int
        :param multiplier_p: Multiplier for high side from above formula.
        :type multiplier_p: int
        :param offset_p: Offset for high side from above formula.
        :type offset_p: int
        :param multiplier_n: Multiplier for low side from above formula.
        :type multiplier_n: int
        :param offset_n: Offset for low side from above formula.
        :type offset_n: int
        :return: None
        :raises ValueError: if a value is out of range
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)
        # Read current configuration
        if 'calibration_adc_voltage' not in self._hardware['commands']:
            raise ConnectionError('ADCVOLTAGECAL is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['calibration_adc_voltage']()
        command.write_mode = False
        command.channel = channel
        command.send(self._interface)

        # Set new values
        if multiplier_p is not None:
            if not isinstance(multiplier_p, int):
                raise TypeError("'multiplier_p' must be an int")
            if not 0 <= multiplier_p <= 65534:
                raise ValueError("'multiplier_p' out of range")
            command.voltage_p_multiplier = multiplier_p
        if offset_p is not None:
            if not isinstance(offset_p, int):
                raise TypeError("'offset_p' must be an int")
            if not 0 <= offset_p <= 65534:
                raise ValueError("'offset_p' out of range")
            command.voltage_p_offset = offset_p
        if multiplier_n is not None:
            if not isinstance(multiplier_n, int):
                raise TypeError("'multiplier_n' must be an int")
            if not 0 <= multiplier_n <= 65534:
                raise ValueError("'multiplier_n' out of range")
            command.voltage_n_multiplier = multiplier_n
        if offset_n is not None:
            if not isinstance(offset_n, int):
                raise TypeError("'offset_n' must be an int")
            if not 0 <= offset_n <= 65534:
                raise ValueError("'offset_n' out of range")
            command.voltage_n_offset = offset_n
        command.write_mode = True
        command.send(self._interface)

    def calibration_to_file(self, filename: pathlib.Path) -> None:
        """
        Writes the complete calibration to the specified output file in JSON
        format. This file can be used with ``calibration_from_file()`` to
        upload the calibration later to an ECU.

        :param filename: the file name or file path the calibration is
                         written to
        :type filename: str
        :return: None
        :raises ValueError: if filename can't be written to
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_file(filename, read=False)

        # Get all calibration and combine into one dict
        calibration: Dict[str, Any] = {}
        calibration['ecu'] = {'uuid': self.uuid,
                              'firmware': self.firmware_name,
                              'version': self.firmware_version}
        if 'calibration_adc_input_current' in self._hardware['commands']:
            calibration['adc_usb'] = self.get_calibration_adc_input_current()
        # Iterate over all channels
        channels_cal: List[Dict[str, Any]] = []
        for channel in range(1, self.channels + 1):
            cal: Dict[str, Any] = {}
            cal['channel_number'] = channel
            if 'calibration_dac' in self._hardware['commands']:
                cal['dac'] = self.get_calibration_dac(channel)
            if 'calibration_adc_current' in self._hardware['commands']:
                cal['adc_current'] = self.get_calibration_adc_current(channel)
            if 'calibration_adc_voltage' in self._hardware['commands']:
                cal['adc_voltage'] = self.get_calibration_adc_voltage(channel)
            channels_cal.append(cal)
        calibration['channels'] = channels_cal

        # Write configuration to file as JSON
        with open(filename, 'w') as file:
            json.dump(calibration, file, sort_keys=True, indent=4)

    def calibration_from_file(self, filename: pathlib.Path) -> None:
        """
        Write the calibration from the JSON file to the ECU. A file
        containing the current calibration can be generated with
        ``calibration_to_file()``. Entries in the file that are unkwown,
        will be ignored.

        :param filename: the file name or file path the configuration is
                         written to
        :type filename: str
        :return: None
        :raises ValueError: if filename can't be read or a value is out of
                            range
        :raises TypeError: if a type doesn't match
        :raises JSONDecodeError: if JSON can't be decoded
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_file(filename, read=True)

        # Read configuration from file
        with open(filename) as file:
            cal = json.load(file)

        # Unlock the calibration values
        self.unlock()

        # Write calibration to ECU
        self.set_calibration_adc_input_current(**cal['adc_usb'])
        for channel_cal in cal['channels']:
            channel = channel_cal['channel_number']
            self._check_channel(channel)
            self.set_calibration_dac(channel, **channel_cal['dac'])
            self.set_calibration_adc_current(channel,
                                             **channel_cal['adc_current'])
            self.set_calibration_adc_voltage(channel,
                                             **channel_cal['adc_voltage'])

    def send_raw_command(self, command_id: int, write_mode: bool = False,
                         data_bytes: Optional[bytes] = bytes()) -> bytes:
        """
        Send a command to the ECU with arbitrary command ID and data.

        :param command_id: command ID to send, in range 0 - 255
        :type command_id: int
        :param write_mode: send command in write mode?
        :type write_mode: bool
        :param data_bytes: command data to send
        :type data_bytes: list(int), bytes or bytearray
        :return: response data
        :rtype: bytes
        :raises ConnectionError: if communication with ECU fails
        """
        if not isinstance(command_id, int):
            raise TypeError('command_id must be an integer')
        if not 0 <= command_id <= 255:
            raise ValueError('channel must be in range of 0 - 255')

        class RawCommand(Command):
            NAME = 'raw transfer'
            BYTE_ID = command_id
            READABLE = True
            WRITEABLE = True
            LENGTH_READ = len(data_bytes) if data_bytes is not None else 0
            LENGTH_WRITE = len(data_bytes) if data_bytes is not None else 0
            RESPONSE_LENGTH_READ = -1
            RESPONSE_LENGTH_WRITE = -1

            _data: bytes = bytes()

            @property
            def data(self) -> bytes:
                return self._data

            @data.setter
            def data(self, value: bytes) -> None:
                self._data = value

            def parse(self, message):
                self.response_data = message[3:-2]

        command = RawCommand()
        command.write_mode = bool(write_mode)
        if data_bytes is not None:
            command.data = data_bytes
        command.send(self._interface)
        return command.response_data
