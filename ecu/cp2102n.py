# The MIT License (MIT)
# Copyright 2021-2022 memetis GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import array
import logging
import platform
from typing import Optional

import serial.tools.list_ports

if platform.system() == 'Windows':
    import pywintypes
    import win32file
else:
    import usb
    from libusb_package import get_libusb1_backend

log = logging.getLogger(__name__)

STOPBITS_ONE = 0x00
STOPBITS_ONE_POINT_FIVE = 0x01
STOPBITS_TWO = 0x02
PARITY_NONE = 0x00
PARITY_ODD = 0x01
PARITY_EVEN = 0x02
PARITY_MARK = 0x03
PARITY_SPACE = 0x04
FIVEBITS = 0x05
SIXBITS = 0x06
SEVENBITS = 0x07
EIGHTBITS = 0x08

READ_BYTE_MAX = 64

CTRL_IFC_ENABLE_WRITE = 0x00
CTRL_BAUDRATE_READ = 0x1D
CTRL_BAUDRATE_WRITE = 0x1E
CTRL_BAUDRATE_LENGTH = 4
CTRL_SET_LINE_CTL_READ = 0x04
CTRL_SET_LINE_CTL_WRITE = 0x03
CTRL_SET_LINE_CTL_LENGTH = 2
CTRL_RECEIVE_TIMEOUT_READ = 0x18
CTRL_RECEIVE_TIMEOUT_WRITE = 0x17
CTRL_RECEIVE_TIMEOUT_LENGTH = 2
CTRL_PURGE = 0x12
CTRL_GET_COMM_STATUS = 0x10
CTRL_GET_COMM_STATUS_LENGTH = 19
CTRL_VENDORSPECIFIC_READ = 0xFF
CTRL_VENDORSPECIFIC_DEVICE_PART_NUMBER = 0x370B
CTRL_VENDORSPECIFIC_DEVICE_PART_NUMBER_LENGTH = 1
CTRL_VENDORSPECIFIC_FIRMWARE_VERSION = 0x10
CTRL_VENDORSPECIFIC_FIRMWARE_VERSION_LENGTH = 3


class CP2102N():
    def __init__(self, usb_device, timeout=None):
        self._usb_device = usb_device
        self._usb_device.set_configuration()
        if timeout is not None:
            self._usb_device.default_timeout = int(timeout * 1000)

        self._set_ifc_enable(True)

        self._bytesize = EIGHTBITS
        self._parity = PARITY_NONE
        self._stopbits = STOPBITS_ONE

    @staticmethod
    def _get_usb_device_from_serial_port(serial_port_name: str):
        for port in serial.tools.list_ports.comports():
            if port.device == serial_port_name:
                try:
                    usb_device = usb.core.find(
                        idVendor=port.vid,
                        idProduct=port.pid,
                        serial_number=port.serial_number,
                        backend=get_libusb1_backend(),
                    )
                    if usb_device is not None:
                        return usb_device
                except Exception as ex:
                    raise ConnectionError(
                        'could not get USB device from serial port name'
                    ) from ex
        raise ConnectionError('could not get USB device from serial port name')

    @staticmethod
    def from_serial_port_name(serial_port_name):
        return CP2102N(CP2102N._get_usb_device_from_serial_port(serial_port_name))

    @staticmethod
    def set_receive_timeout(serial_port, receive_timeout: int):
        receive_timeout = int(receive_timeout) & 0xFFFF

        if platform.system() == 'Windows':
            serial_port.close()
            handle = win32file.CreateFileW(
                f'\\\\.\\{serial_port.name}',
                win32file.GENERIC_READ | win32file.GENERIC_WRITE,
                0,
                pywintypes.SECURITY_ATTRIBUTES(),
                win32file.OPEN_EXISTING,
                0,
            )
            win32file.DeviceIoControl(
                handle,
                0x222024,
                bytes([
                    0x40,
                    CTRL_RECEIVE_TIMEOUT_WRITE,
                    (receive_timeout & 0xFF),
                    (receive_timeout >> 8 & 0xFF),
                    0x00,
                    0x00,
                    0x00,
                    0x00,
                ]),
                8,
            )
            win32file.CloseHandle(handle)
            serial_port.open()
        else:
            usb_device = CP2102N._get_usb_device_from_serial_port(serial_port.name)
            usb_device.ctrl_transfer(
                0x40,
                CTRL_RECEIVE_TIMEOUT_WRITE,
                receive_timeout,
                0,
                None,
            )

        log.debug('set receive timeout to %d us', receive_timeout)

    def _set_ifc_enable(self, value):
        self._usb_device.ctrl_transfer(0x41, CTRL_IFC_ENABLE_WRITE,
                                       bool(value), 0, None)

    def _set_line_ctl(self):
        value = (self._bytesize << 8) | (self._parity << 4) | self._stopbits
        self._usb_device.ctrl_transfer(0x41, CTRL_SET_LINE_CTL_WRITE, value, 0,
                                       None)

    def _get_line_ctl(self):
        value = self._usb_device.ctrl_transfer(0xC1, CTRL_SET_LINE_CTL_READ, 0,
                                               0, 2)
        self._bytesize = value[1]
        self._parity = (value[0] >> 4) & 0x0F
        self._stopbits = value[0] & 0x0F

    @property
    def name(self) -> str:
        return f'CP2102N on USB {self.bus:02d}:{self.address:02d}'

    @property
    def vendor_id(self) -> int:
        return self._usb_device.idVendor

    @property
    def product_id(self) -> int:
        return self._usb_device.idProduct

    @property
    def bus(self):
        return self._usb_device.bus

    @property
    def address(self):
        return self._usb_device.address

    @property
    def manufacturer(self):
        return self._usb_device.manufacturer

    @property
    def product(self):
        return self._usb_device.product

    @property
    def serial_number(self):
        return self._usb_device.serial_number

    @property
    def device_part_number(self):
        result = self._usb_device.ctrl_transfer(
            0xC0,
            CTRL_VENDORSPECIFIC_READ,
            CTRL_VENDORSPECIFIC_DEVICE_PART_NUMBER,
            0,
            CTRL_VENDORSPECIFIC_DEVICE_PART_NUMBER_LENGTH
        )
        value = result[0]
        log.debug('read device part number %d from %s', value,
                  self.serial_number)
        return value

    @property
    def firmware_version(self):
        result = self._usb_device.ctrl_transfer(
            0xC0,
            CTRL_VENDORSPECIFIC_READ,
            CTRL_VENDORSPECIFIC_FIRMWARE_VERSION,
            0,
            CTRL_VENDORSPECIFIC_FIRMWARE_VERSION_LENGTH
        )
        value = f'{result[0]}.{result[1]}.{result[2]}'
        log.debug('read firmware version %s from %s', value,
                  self.serial_number)
        return value

    @property
    def baudrate(self):
        result = self._usb_device.ctrl_transfer(0xC1, CTRL_BAUDRATE_READ, 0, 0,
                                                CTRL_BAUDRATE_LENGTH)
        value = (
            result[0] | (result[1] << 8)
            | (result[2] << 16) | (result[3] << 24)
        )
        log.debug('read baudrate %d from %s', value,
                  self.serial_number)
        return value

    @baudrate.setter
    def baudrate(self, value):
        self._usb_device.ctrl_transfer(
            0x41,
            CTRL_BAUDRATE_WRITE,
            0,
            0,
            int(value).to_bytes(4, byteorder='little')
        )
        log.debug('set baudrate to %d on %s', value,
                  self.serial_number)

    @property
    def bytesize(self):
        self._get_line_ctl()
        return self._bytesize

    @bytesize.setter
    def bytesize(self, value):
        self._bytesize = value
        self._set_line_ctl()

    @property
    def parity(self):
        self._get_line_ctl()
        return self._parity

    @parity.setter
    def parity(self, value):
        self._parity = value
        self._set_line_ctl()

    @property
    def stopbits(self):
        self._get_line_ctl()
        return self._stopbits

    @stopbits.setter
    def stopbits(self, value):
        self._stopbits = value
        self._set_line_ctl()

    @property
    def receive_timeout(self):
        result = self._usb_device.ctrl_transfer(
            0xC0,
            CTRL_RECEIVE_TIMEOUT_READ,
            0,
            0,
            CTRL_RECEIVE_TIMEOUT_LENGTH
        )
        value = result[0] | (result[1] << 8)
        log.debug('read receive timeout %d us from %s', value,
                  self.serial_number)
        return value

    @receive_timeout.setter
    def receive_timeout(self, value):
        value = int(value) & 0xFFFF
        self._usb_device.ctrl_transfer(0x40, CTRL_RECEIVE_TIMEOUT_WRITE, value,
                                       0, None)
        log.debug('set receive timeout to %d us on %s', value,
                  self.serial_number)

    def open(self):
        pass

    def close(self):
        pass

    def read(self, bytes_to_read: Optional[int] = None):
        read_data = array.array('b', bytes(READ_BYTE_MAX))
        number_of_bytes = self._usb_device.read(0x82, read_data)
        return read_data[:number_of_bytes]

    def write(self, data):
        self._usb_device.write(0x02, data)

    def _get_bytes_in_buffers(self):
        result = self._usb_device.ctrl_transfer(
            0xC1,
            CTRL_GET_COMM_STATUS,
            0,
            0,
            CTRL_GET_COMM_STATUS_LENGTH
        )
        bytes_receive_buffer = (
            result[8] | (result[9] << 8)
            | (result[10] << 16) | (result[11] << 24)
        )
        bytes_transmit_buffer = (
            result[12] | (result[13] << 8)
            | (result[14] << 16) | (result[15] << 24)
        )
        return bytes_transmit_buffer, bytes_receive_buffer

    def flush(self):
        while self._get_bytes_in_buffers()[0] != 0:
            pass

    @property
    def in_waiting(self):
        return self._get_bytes_in_buffers()[1]

    def reset_input_buffer(self):
        value = 0x0A
        self._usb_device.ctrl_transfer(0x41, CTRL_PURGE, value,
                                       0, None)
        log.debug('reset input buffer on %s', self.serial_number)

    def reset_output_buffer(self):
        value = 0x05
        self._usb_device.ctrl_transfer(0x41, CTRL_PURGE, value,
                                       0, None)
        log.debug('reset output buffer on %s', self.serial_number)
