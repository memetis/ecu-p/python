# The MIT License (MIT)
# Copyright 2021-2022 memetis GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging
from typing import Any, Dict, List, Tuple, Union

import serial
import serial.tools.list_ports
from packaging.version import Version

from ecu.command import CommandDeviceID, CommandFirmwareVersion
from ecu.communication import (CommunicationBootloader, CommunicationI2C,
                               CommunicationSerial)
from ecu.cp2102n import CP2102N
from ecu.hardware import BOOTLOADER, ECUHardware, convert_custom_hardware
from ecu.smbus import SMBus

log = logging.getLogger(__name__)


class ECU():
    """
    Connect to an ECU.

    :param interface: the communication interface to use. If interface is
                      of type `Serial` or str, communication via USB is
                      used. If interface is of type 'SMBus', communication
                      via I2C is used.
    :type interface: `Serial`, str or `SMBus`
    :param retry: how many times a command should be resent if a
                  transmission fails.
    :type retry: int
    :param timeout: how long to wait for a response to commands in seconds
    :type timeout: float
    :param receive_timeout: delay between receiving ECU-P response and
                            transmitting it over USB.
    :type receive_timeout: int
    :param i2c_address: the address to use for I2C communication.
    :type i2c_address: int
    :param custom_hardware: definitions of additional custom hardware variants
    :type custom_hardware: list of dicts
    :param accept_unknown_hardware: connect to unknown hardware variants
                                    with a basic set of available commands
    :type accept_unknown_hardware: bool
    :raises ConnectionError: if communication with ECU fails
    """
    def __init__(self, interface: Any, retry: int = 2, timeout: float = 0.05,
                 receive_timeout: int = 75, i2c_address: int = 0x14,
                 custom_hardware: Union[List[Dict[str, Any]], Dict[str, Any]] = [],
                 accept_unknown_hardware: bool = False) -> None:
        if not isinstance(retry, int):
            raise TypeError("'retry' needs to be an int")
        if retry < 0:
            raise ValueError("'retry' may not be negative")
        if not isinstance(timeout, (int, float)):
            raise TypeError("'timeout' needs to be a float")
        if timeout <= 0:
            raise ValueError("'timeout' may not be negative or zero")
        if not isinstance(custom_hardware, list):
            custom_hardware = [custom_hardware]

        self._interface: Any = None
        self._hardware: Dict[str, Any] = {}
        self._product_id: int = 0
        self._firmwarename: str = ''
        self._firmwareversion: str = ''
        self._firmwareversion_tuple: tuple[int, int] = (0, 0)
        self._uuid: str = ''
        self._uuidshort: str = ''
        self._bootloader_mode: bool = False
        self._accept_unknown_hardware: bool = accept_unknown_hardware

        # Try to connect to ECU via interface:
        if isinstance(interface, str):
            self._open_interface_serial(interface, timeout)
        if (hasattr(interface, 'baudrate') and hasattr(interface, 'write')
                and callable(interface.write) and hasattr(interface, 'read')
                and callable(interface.read)):
            self._open_interface_serial(interface, timeout)
        if (hasattr(interface, 'write_quick')
                and callable(interface.write_quick)):
            self._test_interface_i2c(interface, i2c_address, timeout)
        if self._interface is None:
            raise ValueError("no working interface found")

        self._custom_hardware = convert_custom_hardware(custom_hardware)
        self._get_hardware_type()

        # Configure CP2102N receive timeout
        if isinstance(self._interface, CommunicationSerial):
            self._configure_cp2102n(receive_timeout)

        self._load_identifier()

        # Set retry count
        self._interface.retry_count = retry

    def __str__(self) -> str:
        return "{} (v{}) @ {} ({})".format(self._firmwarename,
                                           self._firmwareversion,
                                           self._interface,
                                           self._uuidshort)

    def _open_interface_serial(self, interface: Union[str, Any],
                               timeout: float) -> None:
        # Check if serial port can be opened
        if isinstance(interface, str):
            try:
                serial_port = serial.Serial(interface,
                                            baudrate=1000000,
                                            timeout=timeout,
                                            write_timeout=timeout)
            except serial.serialutil.SerialException as exc:
                raise ConnectionError("serial port '{}' cannot be opened"
                                      .format(interface)) from exc

            log.debug("opened serial port '%s'", interface)
        else:
            serial_port = interface

        self._interface = CommunicationSerial(serial_port)
        self._interface.timeout = timeout

    def _configure_cp2102n(self, receive_timeout: int) -> None:
        try:
            CP2102N.set_receive_timeout(self._interface.serial_port,
                                        receive_timeout)
            log.debug(f'successfully set receive timeout to '
                      f'{receive_timeout} us')
        except Exception:
            log.debug('exception while setting receive timeout', exc_info=True)
            log.warning(
                "could not configure read timeout for "
                f"'{self._interface.interface_name}', "
                "communication will be slower"
            )

    def _test_interface_i2c(self, interface: Any, i2c_address: int,
                            timeout: float) -> None:
        if not isinstance(i2c_address, int):
            raise TypeError("'i2c_address' must be an integer")
        if not 0 <= i2c_address <= 126:
            raise ValueError("'i2c_address' must be in range 0 to 126")

        # Test if device is available on bus
        try:
            interface.write_quick(i2c_address)
        except IOError:
            log.error("'can not find device with address 0x%02X", i2c_address)
            raise
        log.debug("found device 0x%02X", i2c_address)

        self._interface = CommunicationI2C(interface, i2c_address)
        self._interface.timeout = timeout

    def _get_hardware_type(self) -> None:
        # Read device identificaton
        try:
            command_deviceid = CommandDeviceID()
            command_deviceid.send(self._interface)
            command_firmwareversion = CommandFirmwareVersion()
            command_firmwareversion.send(self._interface)

            # If sending commands succeeds, device is not in bootloader mode
            self._bootloader_mode = False
        except ConnectionError as exc:
            # Check if bootloader mode is active
            if self._check_bootloader():
                self._bootloader_mode = True
                self._hardware = BOOTLOADER
                log.warning("ECU on '%s' is in bootloader mode",
                            self._interface)
                return
            else:
                raise ConnectionError("identification commands failed on '{}'"
                                      .format(self._interface)) from exc

        success = False
        all_variants = (
            self._custom_hardware
            + [item.value for item in ECUHardware]
        )
        for hardware in all_variants:
            # Compare microcontroller DEVICEID and DERIVID and product ID
            match = self._check_ids(
                hardware,
                command_deviceid.device_id,
                command_deviceid.deriv_id,
                command_deviceid.hardware_id
            )

            # Check if firmware is in valid range
            match &= self._check_firmware_version(
                hardware,
                command_firmwareversion.firmware_version
            )

            # Everything matches, so stop searching further
            if match:
                self._hardware = hardware
                success = True
                break

        # Raise an exception if hardware could not be identified
        if not success:
            log_function = log.error
            if self._accept_unknown_hardware:
                log_function = log.warning
            log_function(
                "could not identify device on '%s' with product ID 0x%02X",
                self._interface,
                command_deviceid.hardware_id
            )
            if self._accept_unknown_hardware:
                self._hardware = ECUHardware.ECU_FALLBACK.value
            else:
                raise ConnectionError("unknown hardware on interface '{}'"
                                      .format(self._interface))

    def _check_bootloader(self) -> bool:
        # Bootloader is only availble on serial connection
        if not isinstance(self._interface, CommunicationSerial):
            return False

        bootloader = CommunicationBootloader(self._interface.serial_port)

        # Test all DEVIDs and DERIVIDs
        for family in BOOTLOADER['identification']:
            devid = family['deviceid']
            for derivid in family['derivid']:
                result = bootloader.check_device(devid, derivid)
                if result == 'match':
                    log.debug("found bootloader on '%s'",
                              self._interface.interface_name)
                    self._firmwarename = 'Bootloader EFM8'
                    self._firmwareversion = ('0x{:02X}, 0x{:02X}'
                                             .format(devid, derivid))
                    self._uuid = 'bl@{}'.format(self._interface)
                    self._uuidshort = 'bl@{}'.format(self._interface)
                    return True
                elif result == 'no bootloader':
                    # if there is no response from bootloader, skip further checks
                    return False

        return False

    def _check_ids(self, hardware: Dict[str, Any], deviceid: int, derivid: int,
                   productid: int) -> bool:
        if 'microcontroller' not in hardware:
            return False
        if deviceid not in hardware['microcontroller']['deviceid']:
            return False
        if derivid not in hardware['microcontroller']['derivid']:
            return False
        if productid != hardware['product_id']:
            return False
        return True

    def _check_firmware_version(self, hardware: Dict[str, Any],
                                firmware: str) -> bool:
        if 'firmware' not in hardware:
            return False

        version_min = hardware['firmware']['version_min']
        version_max = hardware['firmware']['version_max']
        version = Version(firmware)
        if len(version_min) > 0:
            if not version >= Version(version_min):
                return False
        if len(version_max) > 0:
            if not version <= Version(version_max):
                return False
        return True

    def _load_identifier(self) -> None:
        if 'device_id' in self._hardware['commands']:
            command = self._hardware['commands']['device_id']()
            command.send(self._interface)
            self._product_id = command.hardware_id
        if 'firmware_name' in self._hardware['commands']:
            command = self._hardware['commands']['firmware_name']()
            command.send(self._interface)
            self._firmwarename = command.firmware_name

        if 'firmware_version' in self._hardware['commands']:
            command = self._hardware['commands']['firmware_version']()
            command.send(self._interface)
            self._firmwareversion = command.firmware_version
            split_version = self._firmwareversion.split('.')
            if len(split_version) == 2:
                self._firmwareversion_tuple = (int(split_version[0]), int(split_version[1]))

        if 'device_uuid' in self._hardware['commands']:
            command = self._hardware['commands']['device_uuid']()
            command.send(self._interface)
            self._uuid = command.uuid
            self._uuidshort = command.uuid[0:8]

    def _check_channel(self, channel: int) -> None:
        if not isinstance(channel, int):
            raise TypeError('channel must be an integer')
        if not 1 <= channel <= self.channels:
            raise ValueError('channel must be in range of 1-{}'
                             .format(self.channels))

    def _check_current(self, current: float) -> None:
        if not isinstance(current, (int, float)):
            raise TypeError('current needs to be an int or float')
        if not self.current_min <= current <= self.current_max:
            raise ValueError('current not within limits of {} - {} mA'
                             .format(self.current_min,
                                     self.current_max))

    @property
    def uuid(self) -> str:
        """
        The UUID (serial number) of this ECU. The UUID can also be found
        printed on the actual hardware.

        :getter: returns the UUID
        :type: str
        """
        return self._uuidshort

    @property
    def uuid_short(self) -> str:
        """
        Same as ``uuid``, legacy name for the 8 character long UUID.

        :getter: returns the UUID
        :type: str
        """
        return self._uuidshort

    @property
    def product_id(self) -> int:
        """
        Product ID of this ECU.

        :getter: returns the product ID
        :type: int
        """
        return self._product_id

    @property
    def firmware_name(self) -> str:
        """
        The name of the firmware.

        :getter: returns the firmware name
        :type: str
        """
        return self._firmwarename

    @property
    def firmware_version(self) -> str:
        """
        The version of the firmware.

        :getter: returns the firmware version
        :type: str
        """
        return self._firmwareversion

    @property
    def firmware_version_tuple(self) -> Tuple[int, int]:
        """
        The version of the firmware as tuple (major, minor).

        :getter: returns the firmware version tuple
        :type: tuple(int, int)
        """
        return self._firmwareversion_tuple

    @property
    def serial_port(self) -> str:
        """
        Name of the serial port this ECU is connected to. Empty if connected
        over I2C.

        :getter: returns the serial port
        :type: str
        """
        if (hasattr(self._interface, 'serial_port')
                and self._interface.serial_port is not None):
            return self._interface.serial_port.name
        return ''

    @property
    def bootloader_active(self) -> bool:
        """
        Is the device in bootloader mode?

        :getter: returns if the bootloader is active
        :type: bool
        """
        return self._bootloader_mode

    @property
    def channels(self) -> int:
        """
        The number of output current channels.

        :getter: returns the number of channels
        :type: int
        """
        return self._hardware['channel']['count']

    @property
    def current_min(self) -> float:
        """
        The minimum allowed output current.

        :getter: returns the minimum current
        :type: int
        """
        return self._hardware['channel']['current_min']

    @property
    def current_max(self) -> float:
        """
        The maximum allowed output current.

        :getter: returns the maximum current
        :type: int
        """
        return self._hardware['channel']['current_max']

    def reset(self) -> None:
        """
        Reset ECU. This is equal to turning the power off and on again.

        :return: None
        :raises ConnectionError: if communication with ECU fails
        """
        if 'reset' not in self._hardware['commands']:
            raise ConnectionError('RESET is not supported by this hardware')
        command = self._hardware['commands']['reset']()
        command.send(self._interface)
        log.info('reset ECU %s', self.uuid_short)

    def enable(self, channel: int) -> None:
        """
        Enable a specific output current channel.

        :param channel: channel number to enable
        :type channel: int
        :return: None
        :raises ConnectionError: if communication with ECU fails

        If the ECU is in **manual mode**, the channel will stay enabled until
        manually disabled again.

        If manual mode is not active, enableing a channel, triggers actions
        that are configured by the **state machines**.
        """
        self.set_enabled(channel, True)

    def disable(self, channel: int) -> None:
        """
        Disable a specific output current channel.

        :param channel: channel number to disable
        :type channel: int
        :return: None
        :raises ConnectionError: if communication with ECU fails

        If the ECU is in **manual mode**, the channel will stay disabled until
        manually enabled again.

        If manual mode is not active, disableing a channel, triggers actions
        that are configured by the **state machines**.
        """
        self.set_enabled(channel, False)

    def enable_all(self) -> None:
        """
        Enable all output current channels.

        :return: None
        :raises ConnectionError: if communication with ECU fails
        """
        for channel in range(1, self.channels + 1):
            self.set_enabled(channel, True)

    def disable_all(self) -> None:
        """
        Disable all output current channels.

        :return: None
        :raises ConnectionError: if communication with ECU fails
        """
        for channel in range(1, self.channels + 1):
            self.set_enabled(channel, False)

    def toggle(self, channel: int) -> None:
        """
        Toggle a specific output current channel.
        If it is enabled, it will be disabled and vice versa.

        :param channel: channel number to toggle
        :type channel: int
        :return: None
        :raises ConnectionError: if communication with ECU fails
        """
        self.set_enabled(channel, not self.is_enabled(channel))

    def set_enabled(self, channel: int, enabled: bool) -> None:
        """
        Set a specific output current channel to either enabled or disabled.

        :param channel: channel number to set
        :type channel: int
        :param enabled: value to set the channel to
        :type enabled: bool
        :return: None
        :raises ConnectionError: if communication with ECU fails

        If the ECU is in **manual mode**, the channel will stay
        enabled/disabled until changed again.

        If manual mode is not active, setting a channel to enabled/disabled,
        triggers actions that are configured by the **state machines**.
        """
        self._check_channel(channel)
        if not isinstance(enabled, bool):
            raise TypeError('enabled must be a bool')

        if 'enable' not in self._hardware['commands']:
            raise ConnectionError('ENABLE is not supported by this hardware')
        command = self._hardware['commands']['enable']()
        command.write_mode = True
        command.channel = channel
        command.enabled = enabled
        command.send(self._interface)
        log.info('set channel %d to %s on ECU %s', channel,
                 'enabled' if enabled else 'disabled', self.uuid_short)

    def is_enabled(self, channel: int) -> bool:
        """
        Get wether a specific output current channel is enabled or disabled.

        :param channel: channel number to get status from
        :type channel: int
        :return: status of the specific channel
        :rtype: bool
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)
        if 'enable' not in self._hardware['commands']:
            raise ConnectionError('ENABLE is not supported by this hardware')
        command = self._hardware['commands']['enable']()
        command.write_mode = False
        command.channel = channel
        command.send(self._interface)
        log.info('channel %d is %s on ECU %s', channel,
                 'enabled' if command.enabled else 'disabled', self.uuid_short)
        return command.enabled

    def get_setpoint(self, channel: int) -> float:
        """
        Get the current a specific output channel is set to.

        :param channel: channel number to get current for
        :type channel: int
        :return: current setpoint in **mA** of the specific channel
        :rtype: float
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)

        if 'setpoint' not in self._hardware['commands']:
            raise ConnectionError('SETPOINT is not supported by this hardware')
        command = self._hardware['commands']['setpoint']()
        command.write_mode = False
        command.channel = channel
        command.send(self._interface)
        log.info('channel %d setpoint is %.1f mA on ECU %s', channel,
                 command.current, self.uuid_short)
        return command.current

    def set_setpoint(self, channel: int, current: float) -> None:
        """
        Set the current for a specific output channel. This is only possible
        in **manual mode**.

        :param channel: channel number to set current for
        :type channel: int
        :param current: current in **mA** to set the output channel to
        :type current: float
        :return: None
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)
        self._check_current(current)

        if 'setpoint' not in self._hardware['commands']:
            raise ConnectionError('SETPOINT is not supported by this hardware')
        command = self._hardware['commands']['setpoint']()
        command.write_mode = True
        command.channel = channel
        command.current = current
        command.send(self._interface)
        log.info('channel %d setpoint set to %.1f mA on ECU %s', channel,
                 current, self.uuid_short)

    def get_current(self, channel: int) -> float:
        """
        Get the output current of a specific channel
        that is currently flowing.

        :param channel: channel number to get current for
        :type channel: int
        :return: output current in **mA** of the specific channel
        :rtype: float
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)

        if 'current' not in self._hardware['commands']:
            raise ConnectionError('PROCESSVALUE is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['current']()
        command.channel = channel
        command.send(self._interface)
        log.info('channel %d process value is %.1f mA on ECU %s', channel,
                 command.current, self.uuid_short)
        return command.current

    def get_voltage(self, channel: int) -> float:
        """
        Get the output voltage on the specific channel.

        :param channel: channel number to get voltage for
        :type channel: int
        :return: output voltage in **V** of the specific channel
        :rtype: float
        :raises ConnectionError: if communication with ECU fails
        """
        voltage_p, voltage_n = self.get_voltage_raw(channel)
        voltage = voltage_p - voltage_n
        voltage = voltage if voltage > 0.01 else 0
        return voltage

    def get_voltage_raw(self, channel: int) -> Tuple[float, float]:
        """
        Get the high and low side voltage of the specific output channel

        :param channel: channel number to get voltage for
        :type channel: int
        :return: high and low side voltage in **V** of the specific channel
        :rtype: tuple(float, float)
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)

        if 'voltage' not in self._hardware['commands']:
            raise ConnectionError('VOLTAGE is not supported by this hardware')
        command = self._hardware['commands']['voltage']()
        command.channel = channel
        command.send(self._interface)
        log.info('channel %d voltage is high side %.3f V, '
                 'low side %.3f V on ECU %s', channel, command.voltage_p,
                 command.voltage_n, self.uuid_short)
        return (command.voltage_p, command.voltage_n)

    def get_resistance(self, channel: int) -> float:
        """
        Get the resistance value connected to a specicif channel.

        :param channel: channel number to get voltage for
        :type channel: int
        :return: resistance in **Ohm** on the specific channel
        :rtype: float
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)

        if 'resistance' not in self._hardware['commands']:
            raise ConnectionError('RESISTANCE is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['resistance']()
        command.channel = channel
        command.send(self._interface)
        resistance = command.resistance
        log.info('channel %d resistance is %.3f Ohm on ECU %s', channel,
                 resistance, self.uuid_short)
        return resistance

    def get_channel_info(self, channel: int) -> Dict[str, Union[bool, float]]:
        """
        Get all changing values for this channel in one transaction.

        :param channel: channel number to get info for
        :type channel: int
        :return: Dictionary with the following keys:

                 * ``enabled`` (*bool*): True if the channel is enabled.
                 * ``setpoint`` (*float*): Setpoint in **mA**.
                 * ``current`` (*float*): Momentary output current in **mA**.
                 * ``voltage`` (*float*): Momentary output voltage in **V**.
                 * ``voltage_p`` (*float*): Momentary high sideoutput voltage
                   in **V**.
                 * ``voltage_n`` (*float*): Momentary log sideoutput voltage
                   in **V**.
                 * ``resistance`` (*float*): Momentary resistance on output in
                   **Ω**.
        :rtype: dict
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)

        if 'channel_info' not in self._hardware['commands']:
            raise ConnectionError('CHANNELINFO is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['channel_info']()
        command.channel = channel
        command.send(self._interface)

        ret = {}
        ret['enabled'] = command.enabled
        ret['setpoint'] = command.setpoint
        ret['current'] = command.current
        voltage = command.voltage_p - command.voltage_n
        voltage = voltage if voltage > 0.01 else 0
        ret['voltage'] = voltage
        ret['voltage_p'] = command.voltage_p
        ret['voltage_n'] = command.voltage_n
        ret['resistance'] = command.resistance
        return ret

    @property
    def input_current(self) -> float:
        """
        The current that is currently drawn from the power input.

        :getter: returns current in **mA**
        :type: float
        :raises ConnectionError: if communication with ECU fails
        """
        if 'input_current' not in self._hardware['commands']:
            raise ConnectionError('INPUTCURRENT is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['input_current']()
        command.send(self._interface)
        log.info('input current is %.1f mA on ECU %s', command.current,
                 self.uuid_short)
        return command.current

    @property
    def input_current_max(self) -> float:
        """
        The maximum allowed current that can be drawn from the power input.

        On ECUs powered from USB, this depends on the device
        (computer/wall plug) the ECU is connected to
        (value is set by the USB host).

        :getter: returns maximum allowed current in **mA**
        :type: float
        :raises ConnectionError: if communication with ECU fails
        """
        if 'input_current_max' not in self._hardware['commands']:
            raise ConnectionError('INPUTCURRENTMAX is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['input_current_max']()
        command.send(self._interface)
        log.info('max input current is %.0f mA on ECU %s', command.current,
                 self.uuid_short)
        return command.current

    @property
    def manual_mode(self) -> bool:
        """
        The mode the ECU is currently in.

        The output current can only be set if **manual mode** is active.
        In this mode it is possible to overheat devices connected to the
        output channels. **Be careful** what output current you set and for
        how long you enable outputs.

        If not in **manual mode** the
        actions when enabled/disabled and the output current are controled
        by the **state machine** factory configuration.

        :getter: returns True if the ECU is in **manual mode**, False if not
        :setter: set the manual mode to either True or False
        :type: bool
        :raises ConnectionError: if communication with ECU fails
        """
        if 'mode' not in self._hardware['commands']:
            raise ConnectionError('MODE is not supported by this hardware')
        command = self._hardware['commands']['mode']()
        command.write_mode = False
        command.send(self._interface)
        log.info('mode is %s on ECU %s',
                 'manual' if command.manual_mode else 'automatic',
                 self.uuid_short)
        return command.manual_mode

    @manual_mode.setter
    def manual_mode(self, manual_mode: bool) -> None:
        if not isinstance(manual_mode, bool):
            raise TypeError('mode_manual must be a bool')

        if 'mode' not in self._hardware['commands']:
            raise ConnectionError('MODDE is not supported by this hardware')
        command = self._hardware['commands']['mode']()
        command.write_mode = True
        command.manual_mode = manual_mode
        command.send(self._interface)
        log.info('set mode to %s on ECU %s',
                 'manual' if manual_mode else 'automatic', self.uuid_short)

    @property
    def always_measure_resistance(self) -> bool:
        """
        Should the ECU measure the resistance of disabled output
        channels?

        This means the output will be briefly be turned on for the
        measurement. The resulting output current depends on the configuration
        of the CC source.

        :getter: returns True if the ECU is measuring the resistance of disabled
                 channels, False if not
        :setter: set to measure resistance of disabled output channels
        :type: bool
        :raises ConnectionError: if communication with ECU fails
        """
        if 'measure_resistance' not in self._hardware['commands']:
            raise ConnectionError('MEASURERESISTANCE is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['measure_resistance']()
        command.write_mode = False
        command.send(self._interface)
        log.info(
            'always measure resistance is %s on ECU %s',
            'enabled' if command.always_measure_resistance else 'disabeld',
            self.uuid_short
        )
        return command.always_measure_resistance

    @always_measure_resistance.setter
    def always_measure_resistance(self, always_measure_resistance: bool) -> None:
        if not isinstance(always_measure_resistance, bool):
            raise TypeError('always_measure_resistance must be a bool')

        if 'measure_resistance' not in self._hardware['commands']:
            raise ConnectionError('MEASURERESISTANCE is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['measure_resistance']()
        command.write_mode = True
        command.always_measure_resistance = always_measure_resistance
        command.send(self._interface)
        log.info('set always measure resistance to %s on ECU %s',
                 'enabled' if always_measure_resistance else 'disabeld',
                 self.uuid_short)

    def get_digitaloutput(self, channel: int) -> int:
        """
        Get the value of a specific digital output channel.

        :param channel: channel number to get digital output value for
        :type channel: int
        :return: value of the specific channel
        :rtype: int
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)

        if 'digital_output' not in self._hardware['commands']:
            raise ConnectionError('DIGITALOUTPUT is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['digital_output']()
        command.write_mode = False
        command.channel = channel
        command.send(self._interface)
        log.info('channel %d digital output is 0x%04X on ECU %s', channel,
                 command.value, self.uuid_short)
        return command.value

    def set_digitaloutput(self, channel: int, value: int) -> None:
        """
        Set the value for a specific digital output channel. This is only
        possible in **manual mode**.

        :param channel: channel number to set the digital output for
        :type channel: int
        :param value: value (0 - 65535) to set the dgital output channel to
        :type current: int
        :return: None
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)
        if not isinstance(value, int):
            raise TypeError('value must be an int')
        if not 0 <= value <= 65535:
            raise ValueError('value must be in range 0 to 65535')

        if 'digital_output' not in self._hardware['commands']:
            raise ConnectionError('DIGITALOUTPUT is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['digital_output']()
        command.write_mode = True
        command.channel = channel
        command.value = value
        command.send(self._interface)
        log.info('channel %d digital output set to 0x%04X on ECU %s',
                 channel, value, self.uuid_short)

    @property
    def voltagesource(self) -> float:
        """
        The voltage in V of the variable voltage source.

        If the voltage is set to 0, the voltage source will be disabled.

        :getter: returns the output voltage of the voltage source in V
        :setter: set new output voltage for the voltage source in V
        :type: float
        :raises ConnectionError: if communication with ECU fails
        """
        if 'voltage_source' not in self._hardware['commands']:
            raise ConnectionError('VOLTAGESOURCE is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['voltage_source']()
        command.write_mode = False
        command.send(self._interface)
        log.info('voltage source is set to %.1fV on ECU %s', command.voltage,
                 self.uuid_short)
        return command.voltage

    @voltagesource.setter
    def voltagesource(self, voltagesource: float) -> None:
        if not (voltagesource == 0 or 1.4 <= voltagesource <= 20.5):
            raise ValueError('value must be 0 to disable voltage source or in '
                             'range 1.4V to 20.5V')

        if 'voltage_source' not in self._hardware['commands']:
            raise ConnectionError('VOLTAGESOURCE is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['voltage_source']()
        command.write_mode = True
        command.voltage = voltagesource
        command.send(self._interface)
        log.info('set voltage source to %.1fV on ECU %s', voltagesource,
                 self.uuid_short)

    def get_analoginput(self, channel: int) -> float:
        """
        Get the voltage of a specific analog input channel.

        :param channel: channel number to read
        :type channel: int
        :return: voltage in **V** of the specific channel
        :rtype: float
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)

        if 'analog_input' not in self._hardware['commands']:
            raise ConnectionError('ANALOGINPUT is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['analog_input']()
        command.channel = channel
        command.send(self._interface)
        analog_input = command.analog_input
        log.info('analog input %d voltage is %.3f V on ECU %s', channel,
                 analog_input, self.uuid_short)
        return analog_input

    def get_digitalinput(self, channel: int) -> int:
        """
        Get the state of all associated digital inputs of a channel.

        :param channel: channel number to read
        :type channel: int
        :return: state of the digital inputs
        :rtype: int
        :raises ConnectionError: if communication with ECU fails
        """
        self._check_channel(channel)

        if 'digital_input' not in self._hardware['commands']:
            raise ConnectionError('DIGITALINPUT is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['digital_input']()
        command.channel = channel
        command.send(self._interface)
        values = command.values
        log.info('digital inputs on channel %d are %04X on ECU %s', channel,
                 values, self.uuid_short)
        return values

    @property
    def i2c_speed(self) -> int:
        """
        I2C transfer speed in kbit/s.

        :getter: returns the transfer speed in kbit/s
        :setter: set new transfer speed in kbit/s
        :type: int
        :raises ConnectionError: if communication with ECU fails
        """
        if 'i2ccontroller_speed' not in self._hardware['commands']:
            raise ConnectionError('I2CCONTROLLERSPEED is not supported by '
                                  'this hardware')
        command = self._hardware['commands']['i2ccontroller_speed']()
        command.write_mode = False
        command.send(self._interface)
        log.info('I2C controller speed is set to %.0fkbit/s on ECU %s',
                 command.speed, self.uuid_short)
        return command.speed

    @i2c_speed.setter
    def i2c_speed(self, speed: int) -> None:
        if speed <= 0:
            raise ValueError('value must greater than 0')

        if 'i2ccontroller_speed' not in self._hardware['commands']:
            raise ConnectionError('I2CCONTROLLERSPEED is not supported by '
                                  'this hardware')
        command = self._hardware['commands']['i2ccontroller_speed']()
        command.write_mode = True
        command.speed = speed
        command.send(self._interface)
        log.info('set I2C controller speed to %.0fkbit/s on ECU %s', speed,
                 self.uuid_short)

    def i2c_transfer(self, address: int, write_data: bytes = bytes(),
                     read_length: int = 0) -> bytes:
        """
        Write bytes (if any) and then read back bytes (if any) from an
        I2C peripheral.

        :param address: I2C peripheral address
        :type address: int
        :param write_data: bytes to be written to I2C peripheral
        :type write_data: bytes or bytearray
        :param read_length: number of bytes to be read from the I2C peripheral
        :type read_length: int
        :return: read bytes from I2C peripheral
        :rtype: bytes
        :raises ConnectionError: if communication with ECU fails
        """
        if not isinstance(address, int):
            raise TypeError("'address' must be an int")
        if not 0 <= address <= 127:
            raise ValueError("'address' must be in range 0 - 127")
        if not isinstance(write_data, (bytes, bytearray)):
            raise TypeError("'write_data' must be bytes or bytearray")
        if not isinstance(read_length, int):
            raise TypeError("'read_length' must be an int")
        if read_length < 0:
            raise ValueError("'read_length' must be positive")

        if 'i2ccontroller' not in self._hardware['commands']:
            raise ConnectionError('I2CCONTROLLER is not supported by this '
                                  'hardware')
        command = self._hardware['commands']['i2ccontroller']()
        command.address = address
        command.write_data = write_data
        command.read_length = read_length
        command.send(self._interface)
        read_bytes = bytes(command.read_data)
        log.info('wrote %d bytes to peripheral 0x%X and read back '
                 '%d bytes on ECU %s', len(write_data), address,
                 len(read_bytes), self.uuid_short)
        return read_bytes

    def i2c_get_smbus(self) -> SMBus:
        """
        Get a SMBus compatible object to be used for I2C communication.

        :return: SMBus equivilant object for I2C communication
        :rtype: SMBus
        :raises ConnectionError: if communication with ECU fails
        """
        if 'i2ccontroller' not in self._hardware['commands']:
            raise ConnectionError('I2CCONTROLLER is not supported by this '
                                  'hardware')

        return SMBus(self)
