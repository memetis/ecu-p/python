# The MIT License (MIT)
# Copyright 2021-2022 memetis GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import binascii
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from ecu.communication import Communication


class Command():
    NAME: str = ''
    BYTE_ID: int = 0x00
    READABLE: bool = False
    WRITEABLE: bool = False
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 0
    RESPONSE_LENGTH_READ: int = 0
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False

    @property
    def data(self) -> bytes:
        raise NotImplementedError()

    def parse(self, message: bytes) -> None:
        raise NotImplementedError()

    def generate_message(self) -> bytes:
        data = self.data

        # Check if data length matches, skip this check if length is < 0
        if self.write_mode:
            if self.LENGTH_WRITE >= 0 and len(data) != self.LENGTH_WRITE:
                raise ValueError("data length does not match required length")
        else:
            if self.LENGTH_READ >= 0 and len(data) != self.LENGTH_READ:
                raise ValueError("data length does not match required length")

        message = bytearray()
        # Byte count including CRC checksum
        message.append(5 + len(data))
        # Command as byte
        message.append(self.BYTE_ID)
        # Read or write modde
        message.append(ord('!') if self.write_mode else ord('?'))
        # Command data
        message += data

        crc16 = binascii.crc_hqx(message, 0)
        message.append(crc16 & 0xFF)
        message.append((crc16 >> 8) & 0xFF)

        return message

    def send(self, interface: 'Communication') -> 'Command':
        interface.send(self)
        return self

    def response_data_length_match(self, data_length: int) -> bool:
        expected_length = self.RESPONSE_LENGTH_READ
        if self.write_mode:
            expected_length = self.RESPONSE_LENGTH_WRITE

        # If expected length is negative, skip the length check
        if expected_length < 0 or data_length == expected_length:
            return True
        return False


class CommandDeviceID(Command):
    NAME: str = 'DEVICEID'
    BYTE_ID: int = 0x01
    READABLE: bool = True
    WRITEABLE: bool = False
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 0
    RESPONSE_LENGTH_READ: int = 4
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    device_id: int = 0
    deriv_id: int = 0
    rev_id: int = 0
    hardware_id: int = 0

    @property
    def data(self) -> bytes:
        return bytearray()

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.device_id = message[3]
            self.deriv_id = message[4]
            self.rev_id = message[5]
            self.hardware_id = message[6]


class CommandFirmwareName(Command):
    NAME: str = 'FIRMWARENAME'
    BYTE_ID: int = 0x02
    READABLE: bool = True
    WRITEABLE: bool = False
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 0
    RESPONSE_LENGTH_READ: int = -1
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    firmware_name: str = ''

    @property
    def data(self) -> bytes:
        return bytearray()

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.firmware_name = message[3:-2].decode('ascii')


class CommandFirmwareVersion(Command):
    NAME: str = 'FIRMWAREVERSION'
    BYTE_ID: int = 0x03
    READABLE: bool = True
    WRITEABLE: bool = False
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 0
    RESPONSE_LENGTH_READ: int = -1
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    firmware_version: str = ''

    @property
    def data(self) -> bytes:
        return bytearray()

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.firmware_version = message[3:-2].decode('ascii')


class CommandDeviceUUID(Command):
    NAME: str = 'DEVICEUUID'
    BYTE_ID: int = 0x04
    READABLE: bool = True
    WRITEABLE: bool = False
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 0
    RESPONSE_LENGTH_READ: int = -1
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    uuid: str = ''

    @property
    def data(self) -> bytes:
        return bytearray()

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.uuid = ''.join('{:02X}'.format(x) for x in message[3:-2])


class CommandEnterBootloader(Command):
    NAME: str = 'ENTERBOOTLOADER'
    BYTE_ID: int = 0x05
    READABLE: bool = False
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 0
    RESPONSE_LENGTH_READ: int = 0
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = True

    @property
    def data(self) -> bytes:
        return bytearray()

    def parse(self, message: bytes) -> None:
        pass


class CommandReset(Command):
    NAME: str = 'RESET'
    BYTE_ID: int = 0x06
    READABLE: bool = False
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 0
    RESPONSE_LENGTH_READ: int = 0
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = True

    @property
    def data(self) -> bytes:
        return bytearray()

    def parse(self, message: bytes) -> None:
        pass


class CommandEnable(Command):
    NAME: str = 'ENABLE'
    BYTE_ID: int = 0x07
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 1
    LENGTH_WRITE: int = 2
    RESPONSE_LENGTH_READ: int = 1
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    channel: int = 0
    enabled: bool = False

    @property
    def data(self) -> bytes:
        data = bytearray()
        data.append(self.channel)
        if self.write_mode:
            data.append(0x01 if self.enabled else 0x00)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.enabled = bool(message[3])


class CommandSetpoint(Command):
    NAME: str = 'SETPOINT'
    BYTE_ID: int = 0x08
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 1
    LENGTH_WRITE: int = 3
    RESPONSE_LENGTH_READ: int = 2
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    channel: int = 0
    _current: int = 0  # in 0.1 mA

    @property
    def current(self):
        return self._current / 10

    @current.setter
    def current(self, current):
        self._current = int(current * 10)

    @property
    def data(self) -> bytes:
        data = bytearray()
        data.append(self.channel)
        if self.write_mode:
            data.append(self._current & 0xFF)
            data.append((self._current >> 8) & 0xFF)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self._current = message[4] << 8 | message[3]


class CommandProcessValue(Command):
    NAME: str = 'PROCESSVALUE'
    BYTE_ID: int = 0x09
    READABLE: bool = True
    WRITEABLE: bool = False
    LENGTH_READ: int = 1
    LENGTH_WRITE: int = 0
    RESPONSE_LENGTH_READ: int = 2
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    channel: int = 0
    current: float = 0.0  # in mA

    @property
    def data(self) -> bytes:
        data = bytearray()
        data.append(self.channel)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.current = (message[4] << 8 | message[3]) / 10


class CommandVoltage(Command):
    NAME: str = 'VOLTAGE'
    BYTE_ID: int = 0x0A
    READABLE: bool = True
    WRITEABLE: bool = False
    LENGTH_READ: int = 1
    LENGTH_WRITE: int = 0
    RESPONSE_LENGTH_READ: int = 4
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    channel: int = 0
    voltage_p: float = 0.0  # in V
    voltage_n: float = 0.0  # in V

    @property
    def data(self) -> bytes:
        data = bytearray()
        data.append(self.channel)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.voltage_p = (message[4] << 8 | message[3]) / 1000
            self.voltage_n = (message[6] << 8 | message[5]) / 1000


class CommandResistance(Command):
    NAME: str = 'RESISTANCE'
    BYTE_ID: int = 0x0B
    READABLE: bool = True
    WRITEABLE: bool = False
    LENGTH_READ: int = 1
    LENGTH_WRITE: int = 0
    RESPONSE_LENGTH_READ: int = 2
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    channel: int = 0
    resistance: float = 0.0  # in Ohm

    @property
    def data(self) -> bytes:
        data = bytearray()
        data.append(self.channel)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.resistance = (message[4] << 8 | message[3]) / 1000


class CommandInputCurrent(Command):
    NAME: str = 'INPUTCURRENT'
    BYTE_ID: int = 0x0C
    READABLE: bool = True
    WRITEABLE: bool = False
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 0
    RESPONSE_LENGTH_READ: int = 2
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    current: float = 0.0  # in mA

    @property
    def data(self) -> bytes:
        return bytearray()

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.current = (message[4] << 8 | message[3]) / 10


class CommandInputMaxCurrent(Command):
    NAME: str = 'INPUTCURRENTMAX'
    BYTE_ID: int = 0x0D
    READABLE: bool = True
    WRITEABLE: bool = False
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 0
    RESPONSE_LENGTH_READ: int = 2
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    current: float = 0.0  # in mA

    @property
    def data(self) -> bytes:
        return bytearray()

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.current = (message[4] << 8 | message[3]) / 10


class CommandMode(Command):
    NAME: str = 'MODE'
    BYTE_ID: int = 0x0E
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 1
    RESPONSE_LENGTH_READ: int = 1
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    manual_mode: bool = False

    @property
    def data(self) -> bytes:
        data = bytearray()
        if self.write_mode:
            data.append(self.manual_mode)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.manual_mode = bool(message[3])


class CommandModeConfiguration(Command):
    NAME: str = 'MODECONFIGURATION'
    BYTE_ID: int = 0x0F
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 3
    RESPONSE_LENGTH_READ: int = 3
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    manual_mode: bool = False
    _current: int = 0  # in 0.1 mA

    @property
    def current(self):
        return self._current / 10

    @current.setter
    def current(self, current):
        self._current = int(current * 10)

    @property
    def data(self) -> bytes:
        data = bytearray()
        if self.write_mode:
            data.append(self.manual_mode)
            data.append(self._current & 0xFF)
            data.append((self._current >> 8) & 0xFF)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.manual_mode = bool(message[3])
            self._current = message[5] << 8 | message[4]


class CommandStateMachineConfiguration(Command):
    NAME: str = 'STATEMACHINECONFIGURATION'
    BYTE_ID: int = 0x10
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 2
    LENGTH_WRITE: int = -1
    RESPONSE_LENGTH_READ: int = -1
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    address: int = 0
    byte_data: bytes = bytearray()

    @property
    def data(self) -> bytes:
        data = bytearray()
        data.append(self.address & 0xFF)
        data.append((self.address >> 8) & 0xFF)
        if self.write_mode:
            data += self.byte_data
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.byte_data = message[3:-2]


class CommandMonitoringConfiguration(Command):
    NAME: str = 'MONITORINGCONFIGURATION'
    BYTE_ID: int = 0x11
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 6
    RESPONSE_LENGTH_READ: int = 6
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    usb_monitoring: bool = False
    usb_monitoring_timeout: int = 0
    current_monitoring: bool = False
    current_monitoring_error: int = 0

    @property
    def data(self) -> bytes:
        data = bytearray()
        if self.write_mode:
            data.append(0x01 if self.usb_monitoring else 0x00)
            data.append(self.usb_monitoring_timeout & 0xFF)
            data.append((self.usb_monitoring_timeout >> 8) & 0xFF)
            data.append(0x01 if self.current_monitoring else 0x00)
            data.append(self.current_monitoring_error & 0xFF)
            data.append((self.current_monitoring_error >> 8) & 0xFF)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.usb_monitoring = bool(message[3])
            self.usb_monitoring_timeout = (message[5] << 8) | message[4]
            self.current_monitoring = bool(message[6])
            self.current_monitoring_error = (message[8] << 8) | message[7]


class CommandCCSourceConfiguration(Command):
    NAME: str = 'CCSOURCECONFIGURATION'
    BYTE_ID: int = 0x12
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 3
    RESPONSE_LENGTH_READ: int = 3
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    closed_loop_control: bool = False
    closed_loop_control_multiplier: int = 0

    @property
    def data(self) -> bytes:
        data = bytearray()
        if self.write_mode:
            data.append(0x01 if self.closed_loop_control else 0x00)
            data.append(self.closed_loop_control_multiplier & 0xFF)
            data.append((self.closed_loop_control_multiplier >> 8) & 0xFF)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.closed_loop_control = bool(message[3])
            self.closed_loop_control_multiplier = ((message[5] << 8)
                                                   | message[4])


class CommandCCSourceConfigurationV2(Command):
    NAME: str = 'CCSOURCECONFIGURATION'
    BYTE_ID: int = 0x12
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 11
    RESPONSE_LENGTH_READ: int = 11
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    closed_loop_control: bool = False
    closed_loop_control_multiplier: int = 0
    sample_delay: int = 0
    sample_delay_adc: int = 0
    pwm_switchover: bool = False
    _pwm_switchover_threshold: int = 0  # in 0.1 mA
    always_measure_resistance: bool = False

    @property
    def pwm_switchover_threshold(self):
        return self._pwm_switchover_threshold / 10

    @pwm_switchover_threshold.setter
    def pwm_switchover_threshold(self, pwm_switchover_threshold):
        self._pwm_switchover_threshold = int(pwm_switchover_threshold * 10)

    @property
    def data(self) -> bytes:
        data = bytearray()
        if self.write_mode:
            data.append(0x01 if self.closed_loop_control else 0x00)
            data.append(self.closed_loop_control_multiplier & 0xFF)
            data.append((self.closed_loop_control_multiplier >> 8) & 0xFF)
            data.append(self.sample_delay & 0xFF)
            data.append((self.sample_delay >> 8) & 0xFF)
            data.append(self.sample_delay_adc & 0xFF)
            data.append((self.sample_delay_adc >> 8) & 0xFF)
            data.append(0x01 if self.pwm_switchover else 0x00)
            data.append(self._pwm_switchover_threshold & 0xFF)
            data.append((self._pwm_switchover_threshold >> 8) & 0xFF)
            data.append(0x01 if self.always_measure_resistance else 0x00)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.closed_loop_control = bool(message[3])
            self.closed_loop_control_multiplier = ((message[5] << 8)
                                                   | message[4])
            self.sample_delay = ((message[7] << 8) | message[6])
            self.sample_delay_adc = ((message[9] << 8) | message[8])
            self.pwm_switchover = bool(message[10])
            self._pwm_switchover_threshold = ((message[12] << 8) | message[11])
            self.always_measure_resistance = bool(message[13])


class CommandDACCalibration(Command):
    NAME: str = 'DACCALIBRATION'
    BYTE_ID: int = 0x13
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 1
    LENGTH_WRITE: int = 5
    RESPONSE_LENGTH_READ: int = 4
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    channel: int = 0
    multiplier: int = 0
    offset: int = 0

    @property
    def data(self) -> bytes:
        data = bytearray()
        data.append(self.channel & 0xFF)
        if self.write_mode:
            data.append(self.multiplier & 0xFF)
            data.append((self.multiplier >> 8) & 0xFF)
            data.append(self.offset & 0xFF)
            data.append((self.offset >> 8) & 0xFF)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.multiplier = (message[4] << 8 | message[3])
            self.offset = (message[6] << 8 | message[5])


class CommandADCConfiguration(Command):
    NAME: str = 'ADCCONFIGURATION'
    BYTE_ID: int = 0x14
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 4
    RESPONSE_LENGTH_READ: int = 4
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    current_tracking_time: int = 0
    current_accumulate: int = 0
    voltage_tracking_time: int = 0
    voltage_accumulate: int = 0

    @property
    def data(self) -> bytes:
        data = bytearray()
        if self.write_mode:
            data.append(self.current_tracking_time)
            data.append(self.current_accumulate)
            data.append(self.voltage_tracking_time)
            data.append(self.voltage_accumulate)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.current_tracking_time = message[3]
            self.current_accumulate = message[4]
            self.voltage_tracking_time = message[5]
            self.voltage_accumulate = message[6]


class CommandADCCurrentCalibration(Command):
    NAME: str = 'ADCCURRENTCALIBRATION'
    BYTE_ID: int = 0x15
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 1
    LENGTH_WRITE: int = 5
    RESPONSE_LENGTH_READ: int = 4
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    channel: int = 0
    multiplier: int = 0
    offset: int = 0

    @property
    def data(self) -> bytes:
        data = bytearray()
        data.append(self.channel & 0xFF)
        if self.write_mode:
            data.append(self.multiplier & 0xFF)
            data.append((self.multiplier >> 8) & 0xFF)
            data.append(self.offset & 0xFF)
            data.append((self.offset >> 8) & 0xFF)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.multiplier = (message[4] << 8 | message[3])
            self.offset = (message[6] << 8 | message[5])


class CommandADCUSBCurrentCalibration(Command):
    NAME: str = 'ADCINPUTCURRENTCALIBRATION'
    BYTE_ID: int = 0x16
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 4
    RESPONSE_LENGTH_READ: int = 4
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    multiplier: int = 0
    offset: int = 0

    @property
    def data(self) -> bytes:
        data = bytearray()
        if self.write_mode:
            data.append(self.multiplier & 0xFF)
            data.append((self.multiplier >> 8) & 0xFF)
            data.append(self.offset & 0xFF)
            data.append((self.offset >> 8) & 0xFF)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.multiplier = (message[4] << 8 | message[3])
            self.offset = (message[6] << 8 | message[5])


class CommandADCVoltageCalibration(Command):
    NAME: str = 'ADCCVOLTAGECALIBRATION'
    BYTE_ID: int = 0x17
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 1
    LENGTH_WRITE: int = 9
    RESPONSE_LENGTH_READ: int = 8
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    channel: int = 0
    voltage_p_multiplier: int = 0
    voltage_p_offset: int = 0
    voltage_n_multiplier: int = 0
    voltage_n_offset: int = 0

    @property
    def data(self) -> bytes:
        data = bytearray()
        data.append(self.channel & 0xFF)
        if self.write_mode:
            data.append(self.voltage_p_multiplier & 0xFF)
            data.append((self.voltage_p_multiplier >> 8) & 0xFF)
            data.append(self.voltage_p_offset & 0xFF)
            data.append((self.voltage_p_offset >> 8) & 0xFF)
            data.append(self.voltage_n_multiplier & 0xFF)
            data.append((self.voltage_n_multiplier >> 8) & 0xFF)
            data.append(self.voltage_n_offset & 0xFF)
            data.append((self.voltage_n_offset >> 8) & 0xFF)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.voltage_p_multiplier = (message[4] << 8 | message[3])
            self.voltage_p_offset = (message[6] << 8 | message[5])
            self.voltage_n_multiplier = (message[8] << 8 | message[7])
            self.voltage_n_offset = (message[10] << 8 | message[9])


class CommandPushButtonConfiguration(Command):
    NAME: str = 'PUSHBUTTONCONFIGURATION'
    BYTE_ID: int = 0x18
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 1
    RESPONSE_LENGTH_READ: int = 1
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    toggle_mode: bool = False

    @property
    def data(self) -> bytes:
        data = bytearray()
        if self.write_mode:
            data.append(0x01 if self.toggle_mode else 0x00)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.toggle_mode = bool(message[3])


class CommandI2CConfiguration(Command):
    NAME: str = 'I2CCONFIGURATION'
    BYTE_ID: int = 0x19
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 1
    RESPONSE_LENGTH_READ: int = 1
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    address: int = 0

    @property
    def data(self) -> bytes:
        data = bytearray()
        if self.write_mode:
            data.append(self.address)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.address = message[3]


class CommandUnlock(Command):
    NAME: str = 'UNLOCK'
    BYTE_ID: int = 0x1A
    READABLE: bool = False
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 2
    RESPONSE_LENGTH_READ: int = 0
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = True
    key1: int = 0
    key2: int = 0

    @property
    def data(self) -> bytes:
        data = bytearray()
        data.append(self.key1)
        data.append(self.key2)
        return data

    def parse(self, message: bytes) -> None:
        pass


class CommandSaveToEEPROM(Command):
    NAME: str = 'SAVETOEEPROM'
    BYTE_ID: int = 0x1B
    READABLE: bool = False
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 0
    RESPONSE_LENGTH_READ: int = 0
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = True

    @property
    def data(self) -> bytes:
        return bytearray()

    def parse(self, message: bytes) -> None:
        pass


class CommandMeasureResistance(Command):
    NAME: str = 'MEASURERESISTANCE'
    BYTE_ID: int = 0x1C
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 1
    RESPONSE_LENGTH_READ: int = 1
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    always_measure_resistance: bool = False

    @property
    def data(self) -> bytes:
        data = bytearray()
        if self.write_mode:
            data.append(self.always_measure_resistance)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.always_measure_resistance = bool(message[3])


class CommandChannelInfo(Command):
    NAME: str = 'CHANNELINFO'
    BYTE_ID: int = 0x1D
    READABLE: bool = True
    WRITEABLE: bool = False
    LENGTH_READ: int = 1
    LENGTH_WRITE: int = 0
    RESPONSE_LENGTH_READ: int = 11
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    channel: int = 0
    enabled: bool = False
    setpoint: float = 0.0  # in mA
    current: float = 0.0  # in V
    voltage_p: float = 0.0  # in V
    voltage_n: float = 0.0  # in V
    resistance: float = 0.0  # in Ohm

    @property
    def data(self) -> bytes:
        data = bytearray()
        data.append(self.channel)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.enabled = bool(message[3])
            self.setpoint = (message[5] << 8 | message[4]) / 10
            self.current = (message[7] << 8 | message[6]) / 10
            self.voltage_p = (message[9] << 8 | message[8]) / 1000
            self.voltage_n = (message[11] << 8 | message[10]) / 1000
            self.resistance = (message[13] << 8 | message[12]) / 1000


class CommandDigitalOutput(Command):
    NAME: str = 'DIGITALOUTPUT'
    BYTE_ID: int = 0x1E
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 1
    LENGTH_WRITE: int = 3
    RESPONSE_LENGTH_READ: int = 2
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    channel: int = 0
    value: int = 0

    @property
    def data(self) -> bytes:
        data = bytearray()
        data.append(self.channel)
        if self.write_mode:
            data.append(self.value & 0xFF)
            data.append((self.value >> 8) & 0x0FF)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.value = message[4] << 8 | message[3]


class CommandVoltageSource(Command):
    NAME: str = 'VOLTAGESOURCE'
    BYTE_ID: int = 0x1F
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 2
    RESPONSE_LENGTH_READ: int = 2
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    _voltage: int = 0  # in mA

    @property
    def voltage(self):
        return self._voltage / 1000

    @voltage.setter
    def voltage(self, voltage):
        self._voltage = int(voltage * 1000)

    @property
    def data(self) -> bytes:
        data = bytearray()
        if self.write_mode:
            data.append(self._voltage & 0xFF)
            data.append((self._voltage >> 8) & 0x0FF)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self._voltage = message[4] << 8 | message[3]


class CommandAnalogInput(Command):
    NAME: str = 'ANALOGINPUT'
    BYTE_ID: int = 0x20
    READABLE: bool = True
    WRITEABLE: bool = False
    LENGTH_READ: int = 1
    LENGTH_WRITE: int = 0
    RESPONSE_LENGTH_READ: int = 2
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    channel: int = 0
    analog_input: float = 0.0  # in V

    @property
    def data(self) -> bytes:
        data = bytearray()
        data.append(self.channel)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.analog_input = (message[4] << 8 | message[3]) / 1000


class CommandI2CController(Command):
    NAME: str = 'I2CCONTROLLER'
    BYTE_ID: int = 0x21
    READABLE: bool = False
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = -1
    RESPONSE_LENGTH_READ: int = 0
    RESPONSE_LENGTH_WRITE: int = -1

    write_mode: bool = True
    address = 0
    read_length: int = 0
    write_data: bytes = bytearray()
    read_data: bytes = bytearray()

    @property
    def data(self) -> bytes:
        data = bytearray()
        data.append(self.address & 0xFF)
        data.append(len(self.write_data) & 0xFF)
        data.append(self.read_length & 0xFF)
        data += self.write_data
        return data

    def parse(self, message: bytes) -> None:
        self.read_data = message[6:-2]


class CommandI2CControllerSpeed(Command):
    NAME: str = 'I2CCONTROLLERSPEED'
    BYTE_ID: int = 0x22
    READABLE: bool = True
    WRITEABLE: bool = True
    LENGTH_READ: int = 0
    LENGTH_WRITE: int = 2
    RESPONSE_LENGTH_READ: int = 2
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    _speed: int = 0

    @property
    def speed(self):
        return self._speed

    @speed.setter
    def speed(self, speed):
        self._speed = int(speed)

    @property
    def data(self) -> bytes:
        data = bytearray()
        if self.write_mode:
            data.append(self._speed & 0xFF)
            data.append((self._speed >> 8) & 0xFF)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self._speed = (message[4] << 8 | message[3])


class CommandDigitalInput(Command):
    NAME: str = 'DIGITALINPUT'
    BYTE_ID: int = 0x23
    READABLE: bool = True
    WRITEABLE: bool = False
    LENGTH_READ: int = 1
    LENGTH_WRITE: int = 0
    RESPONSE_LENGTH_READ: int = 2
    RESPONSE_LENGTH_WRITE: int = 0

    write_mode: bool = False
    channel: int = 0
    values: int = 0

    @property
    def data(self) -> bytes:
        data = bytearray()
        data.append(self.channel)
        return data

    def parse(self, message: bytes) -> None:
        if not self.write_mode:
            self.values = (message[4] << 8 | message[3])
