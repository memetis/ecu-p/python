# The MIT License (MIT)
# Copyright 2021-2022 memetis GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import binascii
import logging
import time
from dataclasses import dataclass
from typing import Dict, Literal, Union

import intelhex
import serial

from ecu.command import Command
from ecu.cp2102n import CP2102N

log = logging.getLogger(__name__)


@dataclass
class ErrorCode():
    name: str = ''
    description: str = ''


ECU_ERROR_CODES: Dict[int, ErrorCode] = {
    0x01: ErrorCode(
        name='CHECKSUM',
        description='checksum of transmitted command was wrong',
    ),
    0x02: ErrorCode(
        name='UNKNOWN_COMMAND',
        description='transmitted command was unkown',
    ),
    0x03: ErrorCode(
        name='WRONG_MODE',
        description='set mode on command was neither read nor write',
    ),
    0x04: ErrorCode(
        name='READ_ONLY',
        description='command is read-only',
    ),
    0x05: ErrorCode(
        name='WRITE_ONLY',
        description='command is write-only',
    ),
    0x06: ErrorCode(
        name='WRONG_DATA_LENGTH',
        description='transmitted data length does not match commands requierements',
    ),
    0x07: ErrorCode(
        name='WRONG_CHANNEL',
        description='wrong channel in command selected',
    ),
    0x08: ErrorCode(
        name='CALIBRATION_LOCKED',
        description='calibration values need to be unlocked first',
    ),
    0x09: ErrorCode(
        name='AUTOMATIC_MODE',
        description='automatic mode is active',
    ),
    0x0A: ErrorCode(
        name='STATEMACHINE_WRONG',
        description='statemachine data is wrong',
    ),
    0x0B: ErrorCode(
        name='OUT_OF_RANGE',
        description='data is out of range',
    ),
    0x0C: ErrorCode(
        name='I2C_TRANSFER_FAILED',
        description='I2C transaction failed',
    ),
}


class Communication():
    retry_count: int = 0
    timeout: float = 0.05  # in s

    def __str__(self) -> str:
        return self.interface_name

    @property
    def interface_name(self) -> str:
        raise NotImplementedError()

    def transfer(self, message: bytes) -> None:
        raise NotImplementedError()

    def receive(self) -> bytes:
        raise NotImplementedError()

    def reset(self) -> None:
        raise NotImplementedError()

    def _retry(function):
        def wrapper(self, *args, **kwargs):
            for i in range(0, self.retry_count + 1):
                try:
                    function(self, *args, **kwargs)
                    break
                except Exception as error:
                    if i == self.retry_count:
                        raise
                    log.exception(error)
                    log.warning("retrying, %d retries left",
                                self.retry_count - i)
                    # Wait at least 25ms for next communication to be
                    # considered as a new command
                    time.sleep(0.03)
                    # Reset communication buffers
                    self.reset()
        return wrapper

    def _check_response_length(self, response: bytes) -> None:
        # Check if any bytes were received
        if len(response) == 0:
            raise ConnectionError('ECU did not send any response ')

        # Check if at least 5 bytes were received
        if len(response) < 5:
            raise ConnectionError('ECU response does not match the '
                                  'communication protocol')

        # Check if length is correct
        if len(response) != response[0]:
            raise ConnectionError(
                'received response has {} bytes but {} bytes were expected'
                .format(len(response), response[0])
            )

    def _check_checksum(self, response: bytes) -> None:
        # Check if checksum matches
        crc16_calculated = binascii.crc_hqx(response[:-2], 0)
        crc16_received = response[-1] << 8 | response[-2]
        if crc16_calculated != crc16_received:
            raise ConnectionError('checksum mismatch, got {} but expected {}'
                                  .format(crc16_received, crc16_calculated))

    def _check_error_code(self, response: bytes) -> None:
        # Check if response contains error code
        if response[2] == ord('-'):
            # Check for specific message length
            if len(response) != 6:
                raise ConnectionError('response length does not match '
                                      'error code format')
            # Evaluate error code
            error_code = response[3]
            if error_code in ECU_ERROR_CODES:
                error = ECU_ERROR_CODES[error_code]
                raise ConnectionError(f'ERROR {error.name}: {error.description}')
            else:
                raise ConnectionError('response contains an unknown error code')

    @_retry  # type: ignore[arg-type]
    def send(self, command: Command) -> None:
        # Send command
        start_time = time.perf_counter()
        message = command.generate_message()
        start_time_send = time.perf_counter()
        self.transfer(message)
        duration = (time.perf_counter() - start_time_send) * 1000
        log.debug("sent command %s '%s' in %.3f ms", command.NAME,
                  message.hex(), duration)

        # Get response
        start_time_read = time.perf_counter()
        response = self.receive()
        duration = (time.perf_counter() - start_time_read) * 1000
        log.debug("got response '%s' in %.3f ms", response.hex(), duration)

        # Check for errors
        self._check_response_length(response)
        self._check_checksum(response)
        self._check_error_code(response)

        # Check for non-error response
        if response[2] == ord('+'):
            # Check if data length matches expectation
            # if response_data_length is negative, skip this check
            if not command.response_data_length_match(len(response) - 5):
                raise ConnectionError('response data length is wrong')
        else:
            raise ConnectionError('response format does not match')

        # Parse response data
        command.parse(response)

        duration = (time.perf_counter() - start_time) * 1000
        log.debug('successfully sent command %s in %.3f ms', command.NAME,
                  duration)


class CommunicationSerial(Communication):
    def __init__(self, serial_port: Union[serial.Serial, CP2102N]) -> None:
        self._serial_port = serial_port
        self.reset()

    def _receive_pyserial(self) -> bytearray:
        # this will be replaced with value of first received byte
        expected_length = 255
        response = bytearray()
        start_time = time.perf_counter()
        while (len(response) < expected_length
               and time.perf_counter() - start_time < self.timeout):
            # A variable length of data should be received,
            # get the length from the first received byte
            if len(response) == 0:
                response += self._serial_port.read(1)
            else:
                expected_length = response[0]
                missing_bytes = expected_length - len(response)
                response += self._serial_port.read(missing_bytes)

        return response

    def _receive_cp2102n(self) -> bytearray:
        # TODO
        expected_length = 255  # this will be replaced with first received byte
        response = bytearray()
        start_time = time.perf_counter()
        while (len(response) < expected_length
               and time.perf_counter() - start_time < self.timeout):
            response += self._serial_port.read()
            # A variable length of data should be received,
            # get the length from the first received byte
            if len(response) > 1:
                expected_length = response[0]

        return response

    @property
    def interface_name(self) -> str:
        if self._serial_port.name is not None:
            return self._serial_port.name
        else:
            return 'unkown serial port'

    def transfer(self, message: bytes) -> None:
        self._serial_port.write(message)

    def receive(self) -> bytes:
        if isinstance(self._serial_port, CP2102N):
            return self._receive_cp2102n()
        else:
            return self._receive_pyserial()

    def reset(self) -> None:
        try:
            self._serial_port.reset_input_buffer()
            self._serial_port.reset_output_buffer()
        except Exception as error:
            log.exception(error)

    @property
    def serial_port(self) -> Union[serial.Serial, CP2102N]:
        return self._serial_port


class CommunicationI2C(Communication):
    """
    SMBus methods used: write_quick(), write_byte(), write_i2c_block_data(),
                        read_byte(), read_i2c_block_data()
    """
    def __init__(self, i2c_bus, i2c_address: int) -> None:
        if not isinstance(i2c_address, int):
            raise TypeError("'i2c_address' needs to be an int")
        if not 0 <= i2c_address <= 126:
            raise ValueError("'i2c_address' out of range")

        self._bus = i2c_bus
        self._address = i2c_address

    @property
    def interface_name(self) -> str:
        return f'I2C address 0x{self._address:02X}'

    def transfer(self, message: bytes) -> None:
        if len(message) > 1:
            self._bus.write_i2c_block_data(self._address,
                                           message[0],
                                           list(message[1:]))
        elif len(message) == 1:
            self._bus.write_byte(self._address, message[0])
        else:
            self._bus.write_quick(self._address)

    def receive(self) -> bytes:
        data_length = self._bus.read_byte(self._address)
        if not 5 <= data_length <= 32:
            return bytes()
        data = self._bus.read_i2c_block_data(self._address, 255, data_length)
        return bytes(data)

    def reset(self) -> None:
        pass


BOOTLOADER_COMMAND_CODES: Dict[str, int] = {
    'IDENTIFY': 0x30,
    'SETUP': 0x31,
    'ERASE': 0x32,
    'WRITE': 0x33,
    'VERIFY': 0x34,
    'RESET': 0x36,
}

BOOTLOADER_RESPONSE_CODES: Dict[int, str] = {
    0x40: 'ACK',
    0x41: 'RANGE_ERROR',
    0x42: 'BAD_ID',
    0x43: 'CRC_ERROR',
}


class CommunicationBootloader():
    timeout = 0.05

    def __init__(self, serial_port: Union[serial.Serial, CP2102N]) -> None:
        self._serial_port = serial_port

    def _receive(self, count: int) -> bytes:
        if self._serial_port is None:
            return bytearray()
        elif isinstance(self._serial_port, CP2102N):
            return self._receive_cp2102n(count)
        else:
            return self._receive_pyserial(count)

    def _receive_pyserial(self, count: int) -> bytes:
        response = bytearray()
        start_time = time.perf_counter()
        while (len(response) < count
               and time.perf_counter() - start_time < self.timeout):
            response += self._serial_port.read(self._serial_port.in_waiting)

        # Calculate transfer speed
        elapsed_time = time.perf_counter() - start_time
        speed = len(response) * 8 / elapsed_time / 1000
        log.debug('Read %d bytes in %.3fs: %.2fkbit/s',
                  len(response), elapsed_time, speed)
        return response

    def _receive_cp2102n(self, count: int) -> bytes:
        # TODO
        return bytearray()

    def send_autobaud_training(self) -> None:
        # Send auto-baud traning to bootloader
        self._serial_port.write(bytearray([0xFF, 0xFF]))

    def send_command(self, command: str, data: bytes) -> None:
        if command not in BOOTLOADER_COMMAND_CODES:
            raise ValueError("unkown command '{}'".format(command))
        if not isinstance(data, (bytes, bytearray)):
            raise TypeError('data must be of type bytes or bytearray')
        if not 2 <= len(data) <= 130:
            raise ValueError('data length must be in range of 2-130')

        # Create message
        message = bytearray()
        message.append(ord('$'))
        message.append(len(data) + 1)
        message.append(BOOTLOADER_COMMAND_CODES[command])
        message += data

        # Send message and get response
        self._serial_port.write(message)
        log.debug("sent command %s '%s'", command, message.hex())
        responsedata = self._receive(1)
        log.debug("got response '%s'", responsedata.hex())

        # Evaluate response
        if len(responsedata) != 1:
            raise ConnectionError('{} failed: bootloader response has wrong '
                                  'length'.format(command))
        if responsedata[0] not in BOOTLOADER_RESPONSE_CODES:
            raise ConnectionError('{} failed: unknown response from bootloader'
                                  .format(command))
        if BOOTLOADER_RESPONSE_CODES[responsedata[0]] != 'ACK':
            raise ConnectionError('{} failed: error code {}'.format(
                command, BOOTLOADER_RESPONSE_CODES[responsedata[0]]
            ))

        log.debug('successfully sent command %s', command)

    def reset(self) -> bool:
        self.send_autobaud_training()
        self.send_command('RESET', b'\xFF\xFF')
        return True

    def check_device(self, deviceid: int,
                     derivid: int) -> Literal['no bootloader', 'no match', 'match']:
        if not isinstance(deviceid, int):
            raise TypeError('deviceid must be an integer')
        if not isinstance(derivid, int):
            raise TypeError('derivid must be an integer')
        if not 0 <= deviceid <= 255:
            raise ValueError('deviceid must be in range of 0-255')
        if not 0 <= derivid <= 255:
            raise ValueError('derivid must be in range of 0-255')

        try:
            # Enable flash access
            self.send_autobaud_training()
            self.send_command('SETUP', b'\xA5\xF1\x00')

            self.send_command('IDENTIFY', bytearray([deviceid, derivid]))
        except ConnectionError as exc:
            log.debug('failed to identifiy bootloader by DEVICEID=%d DERIVID=%d',
                      deviceid, derivid)
            if 'length' in str(exc):
                return 'no bootloader'
            return 'no match'

        return 'match'

    def _erase_page(self, page_number: int) -> None:
        startaddress = page_number * 512
        data = bytearray([(startaddress >> 8) & 0xFF, startaddress & 0xFF])
        self.send_command('ERASE', data)
        log.debug('erased 0x%04X - 0x%04X %d bytes',
                  startaddress, startaddress + 512 - 1, 512)

    def _write_flash(self, start_address: int, data: bytes) -> None:
        if not isinstance(start_address, int):
            raise TypeError('start_address needs to be an integer')
        if not 0 <= start_address <= 65535:
            raise ValueError('start_address needs to be in range 0-255')
        if not 1 <= len(data) <= 128:
            raise ValueError('data length is wrong')

        data = (bytearray([(start_address >> 8) & 0xFF, start_address & 0xFF])
                + data)
        self.send_command('WRITE', data)
        log.debug('wrote 0x%04X - 0x%04X %d bytes', start_address,
                  start_address + len(data) - 1, len(data))

    def _verify_flash(self, start_address: int, data: bytes) -> bool:
        if not isinstance(start_address, int):
            raise TypeError('startAddress needs to be an integer')
        if not 0 <= start_address <= 65535:
            raise ValueError('startAddress need to be in range 0-255')
        if not 1 <= len(data) <= 128:
            raise ValueError('data length is wrong')

        endaddress = start_address + len(data) - 1
        crc16 = binascii.crc_hqx(data, 0)
        message = bytearray()
        message.append((start_address >> 8) & 0xFF)
        message.append(start_address & 0xFF)
        message.append((endaddress >> 8) & 0xFF)
        message.append(endaddress & 0xFF)
        message.append((crc16 >> 8) & 0xFF)
        message.append(crc16 & 0xFF)

        try:
            self.send_command('VERIFY', message)
        except ConnectionError:
            log.error('failed to verify flash on address %d', start_address)
            return False
        log.debug('verified 0x%04X - 0x%04X %d bytes', start_address,
                  endaddress, len(data))
        return True

    def _erase_firmware_pages(self, intelhex: intelhex.IntelHex) -> bool:
        lastflashpage = int(intelhex.maxaddr() / 512)
        for page in range(lastflashpage + 1):
            # Always erase first page to ensure
            # bootloader is active after reset
            if page == 0:
                self._erase_page(page)
                continue

            # Check if page is to be written by hex file
            pagestartaddress = page * 512
            pageendaddress = pagestartaddress + 512 - 1
            for address in intelhex.addresses():
                if pagestartaddress <= address <= pageendaddress:
                    self._erase_page(page)
                    break

        return True

    def _write_firmware_pages(self, intelhex: intelhex.IntelHex) -> None:
        bytezero = None
        for startaddress, endaddress in intelhex.segments():
            # Get data in 128 byte blocks
            for addr in range(startaddress, endaddress, 128):
                # Copy data
                data = bytearray(intelhex.tobinarray(start=addr, size=128))

                # Check if flash address 0 is to be written
                # and replace with 0xFF
                if addr == 0:
                    bytezero = data[0]
                    data[0] = 0xFF

                # Write data
                self._write_flash(addr, data)

                # Verify data
                if not self._verify_flash(addr, data):
                    log.error('failed to verify flash')
                    return
        log.debug('wrote data except first byte')

        # Write flash address 0
        if bytezero is not None:
            try:
                self._write_flash(0, bytes([bytezero]))
            except ConnectionError:
                log.error('failed to write flash')
                # Enable bootloader on reset
                self._erase_page(0)
                return

            # Verify data
            if not self._verify_flash(0, bytearray([bytezero])):
                # Enable bootloader on reset
                self._erase_page(0)
                log.error('failed to verify flash')

    def write_firmware(self, intelhex: intelhex.IntelHex) -> None:
        # Enable flash access
        self.send_autobaud_training()
        self.send_command('SETUP', b'\xA5\xF1\x00')

        # Erase flash pages
        self._erase_firmware_pages(intelhex)
        log.info('erased flash')

        # Write flash pages
        self._write_firmware_pages(intelhex)
        log.info('successfully wrote firmware')
