# The MIT License (MIT)
# Copyright 2021-2022 memetis GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from copy import deepcopy
from enum import Enum
from typing import Any, Dict, List

from ecu.command import (CommandADCConfiguration, CommandADCCurrentCalibration,
                         CommandADCUSBCurrentCalibration,
                         CommandADCVoltageCalibration, CommandAnalogInput,
                         CommandCCSourceConfiguration,
                         CommandCCSourceConfigurationV2, CommandChannelInfo,
                         CommandDACCalibration, CommandDeviceID,
                         CommandDeviceUUID, CommandDigitalOutput,
                         CommandEnable, CommandEnterBootloader,
                         CommandFirmwareName, CommandFirmwareVersion,
                         CommandI2CConfiguration, CommandI2CController,
                         CommandI2CControllerSpeed, CommandInputCurrent,
                         CommandInputMaxCurrent, CommandMeasureResistance,
                         CommandMode, CommandModeConfiguration,
                         CommandMonitoringConfiguration, CommandProcessValue,
                         CommandPushButtonConfiguration, CommandReset,
                         CommandResistance, CommandSaveToEEPROM,
                         CommandSetpoint, CommandStateMachineConfiguration,
                         CommandUnlock, CommandVoltage, CommandVoltageSource)

BOOTLOADER = {
    'identification': [
        {'deviceid': 0x34, 'derivid': [0x41, 0x45]},  # EFM8LB12
        {'deviceid': 0x30, 'derivid': [0x02, 0x18]},  # EFM8BB1
    ],
    'firmware': {},
    'commands': {},
}


class ECUHardware(Enum):
    """
    Enum of all kwnown hardware this Python module can interact with
    """
    ECU_2I15_10 = {
        'product_id': 0xE7,
        'microcontroller': {'deviceid': [0x34], 'derivid': [0x45]},
        'channel': {'count': 2, 'current_min': 25, 'current_max': 1400},
        'firmware': {'version_min': '', 'version_max': '1.2',
                     'unlock': [0x34, 0xBE], 'bytestream_start': 0x77FF,
                     'bytestream_length': 512},
        'commands': {
            'device_id': CommandDeviceID,
            'firmware_name': CommandFirmwareName,
            'firmware_version': CommandFirmwareVersion,
            'device_uuid': CommandDeviceUUID,
            'reset': CommandReset,
            'mode': CommandMode,
            'input_current': CommandInputCurrent,
            'input_current_max': CommandInputMaxCurrent,
            'enable': CommandEnable,
            'setpoint': CommandSetpoint,
            'current': CommandProcessValue,
            'voltage': CommandVoltage,
            'resistance': CommandResistance,
            'enter_bootloader': CommandEnterBootloader,
            'save_to_eeprom': CommandSaveToEEPROM,
            'configuration_mode': CommandModeConfiguration,
            'configuration_state_machine': CommandStateMachineConfiguration,
            'configuration_monitoring': CommandMonitoringConfiguration,
            'configuration_ccsource': CommandCCSourceConfiguration,
            'configuration_adc': CommandADCConfiguration,
            'configuration_pushbutton': CommandPushButtonConfiguration,
            'configuration_i2c': CommandI2CConfiguration,
            'unlock': CommandUnlock,
            'calibration_dac': CommandDACCalibration,
            'calibration_adc_current': CommandADCCurrentCalibration,
            'calibration_adc_input_current': CommandADCUSBCurrentCalibration,
            'calibration_adc_voltage': CommandADCVoltageCalibration,
        },
    }
    ECU_2I15_11 = {
        'product_id': 0xE7,
        'microcontroller': {'deviceid': [0x34], 'derivid': [0x41, 0x45]},
        'channel': {'count': 2, 'current_min': 25, 'current_max': 1400},
        'firmware': {'version_min': '1.3', 'version_max': '',
                     'unlock': [0x34, 0xBE], 'bytestream_start': 0x77FF,
                     'bytestream_length': 512},
        'commands': {
            'device_id': CommandDeviceID,
            'firmware_name': CommandFirmwareName,
            'firmware_version': CommandFirmwareVersion,
            'device_uuid': CommandDeviceUUID,
            'reset': CommandReset,
            'mode': CommandMode,
            'input_current': CommandInputCurrent,
            'input_current_max': CommandInputMaxCurrent,
            'measure_resistance': CommandMeasureResistance,
            'enable': CommandEnable,
            'setpoint': CommandSetpoint,
            'current': CommandProcessValue,
            'voltage': CommandVoltage,
            'resistance': CommandResistance,
            'channel_info': CommandChannelInfo,
            'enter_bootloader': CommandEnterBootloader,
            'save_to_eeprom': CommandSaveToEEPROM,
            'configuration_mode': CommandModeConfiguration,
            'configuration_state_machine': CommandStateMachineConfiguration,
            'configuration_monitoring': CommandMonitoringConfiguration,
            'configuration_ccsource': CommandCCSourceConfigurationV2,
            'configuration_adc': CommandADCConfiguration,
            'configuration_pushbutton': CommandPushButtonConfiguration,
            'configuration_i2c': CommandI2CConfiguration,
            'unlock': CommandUnlock,
            'calibration_dac': CommandDACCalibration,
            'calibration_adc_current': CommandADCCurrentCalibration,
            'calibration_adc_input_current': CommandADCUSBCurrentCalibration,
            'calibration_adc_voltage': CommandADCVoltageCalibration,
        },
    }
    ECU_P2 = {
        'product_id': 0xE8,
        'microcontroller': {'deviceid': [0x34], 'derivid': [0x41, 0x45]},
        'channel': {'count': 2, 'current_min': 25, 'current_max': 1400},
        'firmware': {'version_min': '', 'version_max': '',
                     'unlock': [0x34, 0xBE], 'bytestream_start': 0x77FF,
                     'bytestream_length': 512},
        'commands': {
            'device_id': CommandDeviceID,
            'firmware_name': CommandFirmwareName,
            'firmware_version': CommandFirmwareVersion,
            'device_uuid': CommandDeviceUUID,
            'reset': CommandReset,
            'mode': CommandMode,
            'input_current': CommandInputCurrent,
            'input_current_max': CommandInputMaxCurrent,
            'measure_resistance': CommandMeasureResistance,
            'enable': CommandEnable,
            'setpoint': CommandSetpoint,
            'current': CommandProcessValue,
            'voltage': CommandVoltage,
            'resistance': CommandResistance,
            'channel_info': CommandChannelInfo,
            'enter_bootloader': CommandEnterBootloader,
            'save_to_eeprom': CommandSaveToEEPROM,
            'configuration_mode': CommandModeConfiguration,
            'configuration_state_machine': CommandStateMachineConfiguration,
            'configuration_monitoring': CommandMonitoringConfiguration,
            'configuration_ccsource': CommandCCSourceConfigurationV2,
            'configuration_adc': CommandADCConfiguration,
            'configuration_pushbutton': CommandPushButtonConfiguration,
            'configuration_i2c': CommandI2CConfiguration,
            'unlock': CommandUnlock,
            'calibration_dac': CommandDACCalibration,
            'calibration_adc_current': CommandADCCurrentCalibration,
            'calibration_adc_input_current': CommandADCUSBCurrentCalibration,
            'calibration_adc_voltage': CommandADCVoltageCalibration,
        },
    }
    ECU_PCON_MP6QUAD = {
        'product_id': 0xA1,
        'microcontroller': {'deviceid': [0x30], 'derivid': [0x02, 0x18]},
        'channel': {'count': 4, 'current_min': 0, 'current_max': 0},
        'firmware': {'version_min': '', 'version_max': '',
                     'unlock': [0, 0], 'bytestream_start': 0,
                     'bytestream_length': 0},
        'commands': {
            'device_id': CommandDeviceID,
            'firmware_name': CommandFirmwareName,
            'firmware_version': CommandFirmwareVersion,
            'device_uuid': CommandDeviceUUID,
            'reset': CommandReset,
            'i2ccontroller': CommandI2CController,
            'i2ccontroller_speed': CommandI2CControllerSpeed,
            'enter_bootloader': CommandEnterBootloader,
        },
    }
    ECU_PCON_MP6SINGLE = {
        'product_id': 0xA9,
        'microcontroller': {'deviceid': [0x30], 'derivid': [0x02, 0x18]},
        'channel': {'count': 1, 'current_min': 0, 'current_max': 0},
        'firmware': {'version_min': '', 'version_max': '',
                     'unlock': [0, 0], 'bytestream_start': 0,
                     'bytestream_length': 0},
        'commands': {
            'device_id': CommandDeviceID,
            'firmware_name': CommandFirmwareName,
            'firmware_version': CommandFirmwareVersion,
            'device_uuid': CommandDeviceUUID,
            'reset': CommandReset,
            'i2ccontroller': CommandI2CController,
            'i2ccontroller_speed': CommandI2CControllerSpeed,
            'enter_bootloader': CommandEnterBootloader,
        },
    }
    ECU_PCON_ABP2LAN = {
        'product_id': 0xB1,
        'microcontroller': {'deviceid': [0x30], 'derivid': [0x02, 0x18]},
        'channel': {'count': 0, 'current_min': 0, 'current_max': 0},
        'firmware': {'version_min': '', 'version_max': '',
                     'unlock': [0, 0], 'bytestream_start': 0,
                     'bytestream_length': 0},
        'commands': {
            'device_id': CommandDeviceID,
            'firmware_name': CommandFirmwareName,
            'firmware_version': CommandFirmwareVersion,
            'device_uuid': CommandDeviceUUID,
            'reset': CommandReset,
            'i2ccontroller': CommandI2CController,
            'i2ccontroller_speed': CommandI2CControllerSpeed,
            'enter_bootloader': CommandEnterBootloader,
        },
    }
    ECU_PCON_SLF3 = {
        'product_id': 0xB9,
        'microcontroller': {'deviceid': [0x30], 'derivid': [0x02, 0x18]},
        'channel': {'count': 0, 'current_min': 0, 'current_max': 0},
        'firmware': {'version_min': '', 'version_max': '',
                     'unlock': [0, 0], 'bytestream_start': 0,
                     'bytestream_length': 0},
        'commands': {
            'device_id': CommandDeviceID,
            'firmware_name': CommandFirmwareName,
            'firmware_version': CommandFirmwareVersion,
            'device_uuid': CommandDeviceUUID,
            'reset': CommandReset,
            'i2ccontroller': CommandI2CController,
            'i2ccontroller_speed': CommandI2CControllerSpeed,
            'enter_bootloader': CommandEnterBootloader,
        },
    }
    ECU_P8_10 = {
        'product_id': 0x35,
        'microcontroller': {'deviceid': [0x34], 'derivid': [0x41, 0x45]},
        'channel': {'count': 8, 'current_min': 25, 'current_max': 1400},
        'firmware': {'version_min': '', 'version_max': '',
                     'unlock': [0x34, 0xBE], 'bytestream_start': 0x77FF,
                     'bytestream_length': 512},
        'commands': {
            'device_id': CommandDeviceID,
            'firmware_name': CommandFirmwareName,
            'firmware_version': CommandFirmwareVersion,
            'device_uuid': CommandDeviceUUID,
            'reset': CommandReset,
            'mode': CommandMode,
            'input_current': CommandInputCurrent,
            'input_current_max': CommandInputMaxCurrent,
            'measure_resistance': CommandMeasureResistance,
            'voltage_source': CommandVoltageSource,
            'enable': CommandEnable,
            'setpoint': CommandSetpoint,
            'current': CommandProcessValue,
            'voltage': CommandVoltage,
            'resistance': CommandResistance,
            'channel_info': CommandChannelInfo,
            'digital_output': CommandDigitalOutput,
            'analog_input': CommandAnalogInput,
            'enter_bootloader': CommandEnterBootloader,
        },
    }
    ECU_P8_11 = {
        'product_id': 0x36,
        'microcontroller': {'deviceid': [0x34], 'derivid': [0x41, 0x45]},
        'channel': {'count': 8, 'current_min': 25, 'current_max': 1400},
        'firmware': {'version_min': '', 'version_max': '',
                     'unlock': [0x34, 0xBE], 'bytestream_start': 0x77FF,
                     'bytestream_length': 512},
        'commands': {
            'device_id': CommandDeviceID,
            'firmware_name': CommandFirmwareName,
            'firmware_version': CommandFirmwareVersion,
            'device_uuid': CommandDeviceUUID,
            'reset': CommandReset,
            'mode': CommandMode,
            'input_current': CommandInputCurrent,
            'input_current_max': CommandInputMaxCurrent,
            'measure_resistance': CommandMeasureResistance,
            'voltage_source': CommandVoltageSource,
            'enable': CommandEnable,
            'setpoint': CommandSetpoint,
            'current': CommandProcessValue,
            'voltage': CommandVoltage,
            'resistance': CommandResistance,
            'channel_info': CommandChannelInfo,
            'digital_output': CommandDigitalOutput,
            'analog_input': CommandAnalogInput,
            'enter_bootloader': CommandEnterBootloader,
        },
    }
    ECU_MEMBLOCK_10 = {
        'product_id': 0x86,
        'microcontroller': {'deviceid': [0x34], 'derivid': [0x41, 0x45]},
        'channel': {'count': 1, 'current_min': 50, 'current_max': 1400},
        'firmware': {'version_min': '', 'version_max': '',
                     'unlock': [0x36, 0xA1], 'bytestream_start': 0x77FF,
                     'bytestream_length': 512},
        'commands': {
            'device_id': CommandDeviceID,
            'firmware_name': CommandFirmwareName,
            'firmware_version': CommandFirmwareVersion,
            'device_uuid': CommandDeviceUUID,
            'reset': CommandReset,
            'mode': CommandMode,
            'input_current': CommandInputCurrent,
            'input_current_max': CommandInputMaxCurrent,
            'measure_resistance': CommandMeasureResistance,
            'enable': CommandEnable,
            'setpoint': CommandSetpoint,
            'current': CommandProcessValue,
            'voltage': CommandVoltage,
            'resistance': CommandResistance,
            'enter_bootloader': CommandEnterBootloader,
            'save_to_eeprom': CommandSaveToEEPROM,
            'configuration_mode': CommandModeConfiguration,
            'configuration_state_machine': CommandStateMachineConfiguration,
            'configuration_monitoring': CommandMonitoringConfiguration,
            'configuration_ccsource': CommandCCSourceConfiguration,
            'configuration_adc': CommandADCConfiguration,
            'configuration_pushbutton': CommandPushButtonConfiguration,
            'configuration_i2c': CommandI2CConfiguration,
            'unlock': CommandUnlock,
            'calibration_dac': CommandDACCalibration,
            'calibration_adc_current': CommandADCCurrentCalibration,
            'calibration_adc_voltage': CommandADCVoltageCalibration,
        },
    }
    ECU_MEMBLOCK_11 = {
        'product_id': 0x87,
        'microcontroller': {'deviceid': [0x34], 'derivid': [0x41, 0x45]},
        'channel': {'count': 1, 'current_min': 50, 'current_max': 1400},
        'firmware': {'version_min': '', 'version_max': '',
                     'unlock': [0x36, 0xA1], 'bytestream_start': 0x77FF,
                     'bytestream_length': 512},
        'commands': {
            'device_id': CommandDeviceID,
            'firmware_name': CommandFirmwareName,
            'firmware_version': CommandFirmwareVersion,
            'device_uuid': CommandDeviceUUID,
            'reset': CommandReset,
            'mode': CommandMode,
            'input_current': CommandInputCurrent,
            'input_current_max': CommandInputMaxCurrent,
            'enable': CommandEnable,
            'setpoint': CommandSetpoint,
            'current': CommandProcessValue,
            'voltage': CommandVoltage,
            'resistance': CommandResistance,
            'channel_info': CommandChannelInfo,
            'enter_bootloader': CommandEnterBootloader,
            'save_to_eeprom': CommandSaveToEEPROM,
            'configuration_mode': CommandModeConfiguration,
            'configuration_state_machine': CommandStateMachineConfiguration,
            'configuration_monitoring': CommandMonitoringConfiguration,
            'configuration_ccsource': CommandCCSourceConfiguration,
            'configuration_adc': CommandADCConfiguration,
            'configuration_pushbutton': CommandPushButtonConfiguration,
            'configuration_i2c': CommandI2CConfiguration,
            'unlock': CommandUnlock,
            'calibration_dac': CommandDACCalibration,
            'calibration_adc_current': CommandADCCurrentCalibration,
            'calibration_adc_voltage': CommandADCVoltageCalibration,
        },
    }
    ECU_FALLBACK = {
        'product_id': 0x00,
        'microcontroller': {
            'deviceid': [0x30, 0x34],
            'derivid': [0x02, 0x18, 0x41, 0x45],
        },
        'channel': {'count': 0, 'current_min': 0, 'current_max': 0},
        'firmware': {'version_min': '', 'version_max': '',
                     'unlock': [0, 0], 'bytestream_start': 0,
                     'bytestream_length': 0},
        'commands': {
            'device_id': CommandDeviceID,
            'firmware_name': CommandFirmwareName,
            'firmware_version': CommandFirmwareVersion,
            'device_uuid': CommandDeviceUUID,
            'reset': CommandReset,
            'i2ccontroller': CommandI2CController,
            'i2ccontroller_speed': CommandI2CControllerSpeed,
            'enter_bootloader': CommandEnterBootloader,
        },
    }


def convert_custom_hardware(custom_hardware: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    parsed_hardawre = deepcopy(custom_hardware)
    for item in parsed_hardawre:
        for command_name, command_string in item['commands'].items():
            item['commands'][command_name] = eval(command_string)
    return parsed_hardawre
