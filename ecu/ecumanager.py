# The MIT License (MIT)
# Copyright 2021-2022 memetis GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging
import platform
import re
from typing import Any, Callable, Dict, List

import serial
import serial.tools.list_ports

from ecu.ecu import ECU

log = logging.getLogger(__name__)

FILTER_SERIAL_DARWIN: List[str] = [
    r'/dev/cu\.usbserial.*',
]


class ECUManager():
    """
    Scan for ECUs connected with USB or I2C.

    If not disabled all USB ports will be check for connected ECUs when an
    instance is created. Scanning for I2C connections must be manually
    triggered.

    :param scan_usb: scan for USB connected ECUs
    :type scan_usb: bool
    :param custom_hardware: definitions of additional custom hardware variants
    :type custom_hardware: list of dicts
    :param exclude_serial: serial ports to exclude when scanning for ECUs
    :type exclude_serial: list of str
    :param skip_open_ports: try to skip testing already open serial ports
    :type skip_open_ports: bool
    :param test_os_ports: also test serial ports always present on different OSes
    :type skip_os_ports: bool
    :param accept_unknown_hardware: connect to unknown hardware variants
                                    with a basic set of available commands
    :type accept_unknown_hardware: bool
    """
    def __init__(self, scan_usb: bool = True,
                 custom_hardware: List[Dict[str, Any]] = [],
                 exclude_serial: List[str] = [],
                 skip_open_ports: bool = True, test_os_ports: bool = False,
                 accept_unknown_hardware: bool = False) -> None:
        self._custom_hardware = custom_hardware

        if not isinstance(exclude_serial, list):
            exclude_serial = [exclude_serial]

        self._exclude_serial = exclude_serial
        self._test_os_ports = test_os_ports
        self._filter_serial_darwin = re.compile(
            "(?:" + ")|(?:".join(FILTER_SERIAL_DARWIN) + ")"
        )
        self._accept_unknown_hardware = accept_unknown_hardware
        self._ecus_usb: Dict[str, ECU] = {}
        self._ecus_i2c: Dict[str, ECU] = {}

        if scan_usb:
            self.scan_usb(skip_open_ports=skip_open_ports)

    def _has_methods(self, item: Any, methods: List[str]) -> bool:
        """
        Check if the supplied item has all the specified methods

        :param item: Item to check
        :type item: object
        :param methods: Method names as str to check
        :type methods: list
        :return: True if all methods are callable, False otherwise
        :rtype: bool
        """
        for method in methods:
            if not hasattr(item, method):
                log.error("method '%s()' is missing", method)
                return False
            if not callable(getattr(item, method)):
                log.error("method '%s()' is not callable", method)
                return False
        return True

    def _scan_i2c_addresses(self, write_quick_function: Callable[[int], None]) -> List[int]:
        """
        Find all addresses of I2C devices connected to the bus. This uses
        write_quick() to test if a device is responding.

        :param bus: I2C interface to scan
        :type bus: any I2C interface that implements write_quick().
        :return: Addresses on the I2C bus
        :rtype: list
        """
        all_i2c_devices: List[int] = []
        for address in range(128):
            try:
                write_quick_function(address)
                all_i2c_devices.append(address)
            except Exception:
                pass
        return all_i2c_devices

    def scan_usb(self, exclude_serial: List[str] = [],
                 skip_open_ports: bool = True) -> None:
        """
        Scan for ECUs connected with USB. This will remove disconnected units
        from the list of available ECUs.

        :param exclude_serial: serial ports to exclude when scanning for ECUs
        :type exclude_serial: list of str
        :return: None
        """
        if not isinstance(exclude_serial, list):
            exclude_serial = [exclude_serial]

        # Reset all ECUs connected on USB
        self._ecus_usb = {}

        # Get all connected serial ports if they are not to be excluded
        serialports: List[str] = []
        test_interface = serial.Serial()
        for port in serial.tools.list_ports.comports():
            # Skip ports in exclude list
            if port.device in (self._exclude_serial + exclude_serial):
                continue

            # Skip already open ports if enabled
            test_interface.port = port.name
            if skip_open_ports and test_interface.is_open:
                continue

            # Skip ports not matching filter list on macOS
            # except if configured to also test OS ports
            if (not self._test_os_ports and platform.system() == 'Darwin'
                    and self._filter_serial_darwin.match(port.device) is None):
                continue

            serialports.append(port.device)
        log.debug('serial ports to test: %s', serialports)

        # Test if ECU is connected on a serial port
        for port_to_test in serialports:
            log.debug("testing serial port '%s'", port_to_test)
            try:
                ecu = ECU(
                    port_to_test,
                    custom_hardware=self._custom_hardware,
                    accept_unknown_hardware=self._accept_unknown_hardware,
                )
                self._ecus_usb[ecu.uuid] = ecu
                log.info('Found %s', ecu)
            except Exception as exc:
                log.debug(exc, exc_info=True)
                log.info("No ECU found on '%s'", port_to_test)

        # Sort all found ECUs by their UUID
        self._ecus_usb = dict(sorted(self._ecus_usb.items()))

    def scan_i2c(self, i2c_interfaces: Any) -> None:
        """
        Scan for ECUs connected with I2C. This will remove disconnected units
        from the list of available ECUs. A list of I2C interfaces is given that
        will be scanned for available ECUs.

        These interfaces must have the following callable methods
        accepting the same parameters as the
        `smbus2 <https://pypi.org/project/smbus2/>`_ library:
        * write_quick()
        * write_byte()
        * write_i2c_block_data()
        * read_byte()
        * read_i2c_block_data()

        :param i2c_interfaces: I2C interfaces to scan
        :type i2c_interfaces: list
        :return: None
        :raises TypeError: if I2C interface does not implement all required
                           methods
        """
        # Convert to list
        if not isinstance(i2c_interfaces, list):
            i2c_interfaces = [i2c_interfaces]

        # Check if I2C interfaces have required methods
        methods_required = [
            'write_quick',
            'write_byte',
            'write_i2c_block_data',
            'read_byte',
            'read_i2c_block_data',
        ]
        for bus in i2c_interfaces:
            if not self._has_methods(bus, methods_required):
                raise TypeError("I2C interface '{}' does not implement "
                                "the required methods: {}"
                                .format(bus, ', '.join(methods_required)))

        # Reset all ECUs connected on I2C
        self._ecus_i2c = {}

        # Check all supplied I2C interfaces
        for bus in i2c_interfaces:
            # Find all devices on bus
            all_i2c_devices = self._scan_i2c_addresses(bus.write_quick)

            # Warn if no devices found on I2C bus
            log.debug("I2C device addresses on bus '%s' to test: %s", bus,
                      all_i2c_devices)
            if len(all_i2c_devices) == 0:
                log.warning("no I2C devices found on '%s'", bus)
                continue

            # Test all I2C devices on bus
            for address in all_i2c_devices:
                try:
                    ecu = ECU(
                        bus,
                        i2c_address=int(address),
                        custom_hardware=self._custom_hardware,
                        accept_unknown_hardware=self._accept_unknown_hardware,
                    )
                    self._ecus_i2c[ecu.uuid] = ecu
                    log.info("Found %s", ecu)
                except Exception as exc:
                    log.debug(exc, exc_info=True)
                    log.info("No ECU found on '%s' 0x%X", bus, address)

        # Sort all found ECUs by their UUID
        self._ecus_i2c = dict(sorted(self._ecus_i2c.items()))

    @property
    def ecus(self) -> List[ECU]:
        """
        All ECUs connected with USB and I2C.

        :getter: returns all found ECUs
        :type: list
        """
        return self.get_all()

    def get_all(self) -> List[ECU]:
        """
        All ECUs connected with USB and I2C.

        :return: connected ECUs
        :rtype: list
        """
        return list(self._ecus_usb.values()) + list(self._ecus_i2c.values())

    @property
    def uuids(self) -> List[str]:
        """
        UUIDs of all connected ECUs.

        :getter: returns list of UUIDs of all found ECUs
        :type: list
        """
        return list(self._ecus_usb.keys()) + list(self._ecus_i2c.keys())

    def get_by_uuid(self, uuid: str) -> ECU:
        """
        Get a connected ECU by it's UUID.

        :param uuid: UUID of ECU to get
        :type uuid: str
        :return: ECU
        :rtype: ECU
        :raises ValueError: if ECU with UUID is not connected.
        """
        if uuid in self._ecus_usb.keys():
            return self._ecus_usb[uuid]
        if uuid in self._ecus_i2c.keys():
            return self._ecus_i2c[uuid]
        raise ValueError('ECU {} is not connected'.format(uuid))
