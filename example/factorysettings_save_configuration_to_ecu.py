import logging
import pathlib
import sys

from ecu import ECUFactorySettings, ECUManager

# Configure logging
log = logging.getLogger()
log.setLevel(logging.WARNING)
logHandler = logging.StreamHandler()
logFormat = logging.Formatter('%(asctime)s %(levelname)s - '
                              '%(name)s: %(message)s')
logHandler.setFormatter(logFormat)
log.addHandler(logHandler)

# List all devices
manager = ECUManager()
print('---------------------------------------------'
      '---------------------------------------------')
for ecu in manager.get_all():
    print(ecu)
print('---------------------------------------------'
      '---------------------------------------------')

for ecu in manager.get_all():
    ecu = ECUFactorySettings.from_ecu(ecu)
    ecu.configuration_from_file(pathlib.Path(sys.argv[1]))
    ecu.save_eeprom()

print('Done!')
