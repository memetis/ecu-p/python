import logging

from ecu import ECUManager

# Configure logging
log = logging.getLogger()
log.setLevel(logging.INFO)
logHandler = logging.StreamHandler()
logFormat = logging.Formatter(('%(asctime)s %(levelname)s - '
                               '%(name)s: %(message)s'))
logHandler.setFormatter(logFormat)
log.addHandler(logHandler)

# List all devices
manager = ECUManager()
print('---------------------------------------------'
      '---------------------------------------------')
for ecu in manager.get_all():
    print(ecu)
print('---------------------------------------------'
      '---------------------------------------------')
