import logging

from smbus2 import SMBus

from ecu import ECU, ECUManager

# Configure logging
log = logging.getLogger()
log.setLevel(logging.INFO)
logHandler = logging.StreamHandler()
logFormat = logging.Formatter('%(asctime)s %(levelname)s - '
                              '%(name)s: %(message)s')
logHandler.setFormatter(logFormat)
log.addHandler(logHandler)


##############################################################################
#
# Option 1: Using an already opened I2C interface
#           (default ECU I2C address 0x14)
#
##############################################################################
i2c_interface = SMBus(1)
ecu = ECU(i2c_interface)
print('connected to', ecu)

# Clean up the created objects
del ecu
del i2c_interface


##############################################################################
#
# Option 2: Use ECUManager to find all connected ECUs
#
##############################################################################
i2c_interface = SMBus(1)
manager = ECUManager(scan_usb=False)
manager.scan_i2c(i2c_interface)
print('List of all connected ECUs:')
for ecu in manager.get_all():
    print(ecu)

# Clean up the created objects
del manager


##############################################################################
#
# Option 3: Using an already opened I2C interface with a changed I2C address
#           (ECU configuration for I2C was previously changed)
#
##############################################################################
i2c_interface = SMBus(1)
ecu = ECU(i2c_interface, i2c_address=0x54)
print('connected to', ecu)

# Clean up the created objects
del ecu
del i2c_interface
