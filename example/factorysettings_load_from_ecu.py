import logging
import pathlib

from ecu import ECUFactorySettings, ECUManager

# Configure logging
log = logging.getLogger()
log.setLevel(logging.INFO)
logHandler = logging.StreamHandler()
logFormat = logging.Formatter('%(asctime)s %(levelname)s - '
                              '%(name)s: %(message)s')
logHandler.setFormatter(logFormat)
log.addHandler(logHandler)

# List all devices
manager = ECUManager()
print('---------------------------------------------'
      '---------------------------------------------')
for ecu in manager.get_all():
    print(ecu)
print('---------------------------------------------'
      '---------------------------------------------')

for ecu in manager.get_all():
    ecu = ECUFactorySettings.from_ecu(ecu)
    ecu.configuration_to_file(pathlib.Path(f"configuration_{ecu.uuid_short}.json"))
    ecu.calibration_to_file(pathlib.Path(f"calibration_{ecu.uuid_short}.json"))
