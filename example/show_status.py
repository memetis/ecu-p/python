import logging
import signal
import time

from ecu import ECUManager

# Configure logging
log = logging.getLogger()
log.setLevel(logging.DEBUG)
logHandler = logging.StreamHandler()
logFormat = logging.Formatter(('%(asctime)s %(levelname)s - '
                               '%(name)s: %(message)s'))
logHandler.setFormatter(logFormat)
log.addHandler(logHandler)

# List all devices
manager = ECUManager()
print('---------------------------------------------'
      '---------------------------------------------')
for ecu in manager.get_all():
    print(ecu)
print('---------------------------------------------'
      '---------------------------------------------')

# Check if at leaste one ECU was found
if len(manager.get_all()) == 0:
    raise ConnectionError('no ECU was found')


# Catch CTRL-C events and reset ECUs
def shutdown(signal, frame):
    for ecu in manager.get_all():
        ecu.reset()
    exit(0)


signal.signal(signal.SIGINT, shutdown)
print('press CTRL + C to exit!')

# Change log level
log.setLevel(logging.WARNING)

# Show status of first found ECU
ecu = list(manager.get_all())[0]
last_time = time.perf_counter()
while True:
    report_string = '\rECU: {}'.format(ecu.uuid_short)
    for channel in range(1, ecu.channels + 1):
        enabled = ecu.is_enabled(channel)
        setpoint = ecu.get_setpoint(channel)
        processvalue = ecu.get_current(channel)
        voltage = ecu.get_voltage(channel)
        resistance = ecu.get_resistance(channel)

        status = ' EN' if enabled else 'DIS'
        report_string += ('   CH{}:{} '
                          'SP {:6.1f}mA '
                          'PV {:6.1f}mA '
                          'V {:4.2f}V '
                          'R {:6.3f}Ohm').format(channel,
                                                 status,
                                                 setpoint,
                                                 processvalue,
                                                 voltage,
                                                 resistance)
    now = time.perf_counter()
    sample_rate = 1 / (now - last_time)
    last_time = now
    report_string += '   {:.1f}SPS'.format(sample_rate)
    print(report_string, end='', flush=True)
