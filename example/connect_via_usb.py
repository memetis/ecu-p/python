import logging

import serial

from ecu import ECU, ECUManager

# Configure logging
log = logging.getLogger()
log.setLevel(logging.WARNING)
logHandler = logging.StreamHandler()
logFormat = logging.Formatter('%(asctime)s %(levelname)s - '
                              '%(name)s: %(message)s')
logHandler.setFormatter(logFormat)
log.addHandler(logHandler)


##############################################################################
#
# Serial port name to use
#
##############################################################################
# on macOS: look for the virtual COM port by running 'ls -l /dev/cu.*'
serial_name = '/dev/cu.SLAB_USBtoUART'

# on Linux: look for the virtual COM port by running 'ls -l /dev/ttyUSB*'
# serial_name = '/dev/ttyUSB0'

# on Windows: check in Device Manager for the COM port name
# serial_name = 'COM0'


##############################################################################
#
# Option 1: Let the ECU handle opening the serial port
#
##############################################################################
ecu = ECU(serial_name)
print('connected to', ecu)

# Clean up the created objects
del ecu


##############################################################################
#
# Option 2: Using an already opened serial port
#
##############################################################################
serial_port = serial.Serial(serial_name,
                            baudrate=1000000,
                            timeout=0.2,
                            write_timeout=0.2)
ecu = ECU(serial_port)
print('connected to', ecu)

# Clean up the created objects
del ecu
del serial_port


##############################################################################
#
# Option 3: Use ECUManager to find all connected ECUs
#
##############################################################################
manager = ECUManager()
print('List of all connected ECUs:')
for ecu in manager.get_all():
    print(ecu)

# Clean up the created objects
del manager
